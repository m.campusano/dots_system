import copy

from dots.ast import Parameter, ActionDefinition, Mission, Operation, Constraint, Coordination, ActionCall, Argument
from dots.core.program import Program

class Slicer:

    def slice(self, original_program):
        self.__new_program = self.__build_base_program(original_program)
        init_scope = {'__replaced': {}}
        self.__new_program.operation.mission.node = self.__slice_node(original_program.operation.mission.node, original_program.runtime, init_scope)

        return self.__new_program

    def __build_base_program(self, original_program):
        new_program = Program(self.__build_base_operation(original_program.operation), {})
        new_program.__id__ = original_program.__id__
        new_program.original_operation = original_program.original_operation
        new_program.arguments = copy.deepcopy(original_program.arguments)

        # update take_off_position of flying drones
        for drone_id, position in original_program.runtime.drones_position.items():
            drone = new_program.get_drone(drone_id)
            drone.take_off_position.latitude = position[0]
            drone.take_off_position.longitude = position[1]
        
        return new_program

    def __build_base_operation(self, original_operation):
        new_operation = Operation(self.new_operation_name(original_operation))
        new_operation.action_definitions = copy.deepcopy(original_operation.action_definitions)
        new_operation.parameters = copy.deepcopy(original_operation.parameters)
        new_operation.mission = Mission()
        return new_operation 


    def new_operation_name(self, original_operation):
        return f'sliced_{original_operation.name}'
    
    def __slice_node(self, node, runtime, scope):

        new_scope = copy.deepcopy(scope)
        return_node = None
        if isinstance(node, Constraint):
            return_node = self.__slice_constraint(node, runtime, scope)
        if isinstance(node, Coordination):
            return_node = self.__slice_coordination(node, runtime, scope)
        if isinstance(node, ActionCall):
            return_node = self.__slice_action(node, runtime, scope)

        return return_node

    def __slice_coordination(self, node, runtime, scope):
        new_scope = copy.deepcopy(scope)
        if node.is_using_drones():
            new_scope[node.drone_reference_name] = node.drone_list_reference_name

        new_node = None
        if node.is_sequential():
            new_node = self.__slice_coordination_sequential(node, runtime, new_scope)
        if node.is_prioritized():
            new_node = self.__slice_coordination_prioritized(node, runtime, new_scope)
        if node.is_parallel():
            new_node = self.__slice_coordination_parallel(node, runtime, new_scope)

        if node.is_using_drones():
            self.__replaced_drone(new_node, new_scope)

        return new_node

    def __slice_coordination_sequential(self, node, runtime, scope):
        runtime_node = runtime.get_node_from_ast(node)
        new_sequential = Coordination.sequential()
        
        if len(runtime_node.executing_nodes()) != 0:
            # new_prioritized = Coordination.prioritized()
            # for executing in runtime_node.executing_nodes():
                # new_prioritized.add_node(self.__slice_node(executing.ast_node, runtime, scope))
            # new_sequential.add_node(new_prioritized)
            new_sequential.add_node(self.__slice_node(runtime_node.executing_nodes()[0].ast_node, runtime, scope))
        
        for waiting in runtime_node.waiting_nodes():
            new_sequential.add_node(copy.deepcopy(waiting.ast_node))
        
        return new_sequential

    def __slice_coordination_prioritized(self, node, runtime, scope):
        return self.__slice_common_coordination(Coordination.prioritized(), node, runtime, scope)

    def __slice_coordination_parallel(self, node, runtime, scope):
        return self.__slice_common_coordination(Coordination.parallel(), node, runtime, scope)

    def __slice_common_coordination(self, new_coordination, node, runtime, scope):
        runtime_node = runtime.get_node_from_ast(node)
        new_prioritized = Coordination.prioritized()
        if runtime_node.executing():
            for executing in runtime_node.executing_nodes():
                new_prioritized.add_node(self.__slice_node(executing.ast_node, runtime, scope))
        if len(runtime_node.waiting_nodes()) != 0:
            for waiting in runtime_node.waiting_nodes():
                new_coordination.add_node(copy.deepcopy(waiting.ast_node))
            new_prioritized.add_node(new_coordination)
        return new_prioritized

    def __slice_constraint(self, node, runtime, scope):
        new_constraint = node.copy_no_body()
        runtime_node = runtime.get_node_from_ast(node)
        for executing in runtime_node.executing_nodes():
            new_constraint.add_node(self.__slice_node(executing.ast_node, runtime, scope))
        for waiting in runtime_node.waiting_nodes():
            new_constraint.add_node(copy.deepcopy(waiting.ast_node))
        return new_constraint

    def __slice_action(self, node, runtime, scope):
        if node.action_name.lower() == 'straight':
            return self.__slice_straight(node, runtime, scope)
        elif node.action_name.lower() == 'patrol':
            return self.__slice_patrol(node, runtime, scope)
    
    def __slice_straight(self, node, runtime, scope):
        runtime_node = runtime.get_node_from_ast(node)
        ast_node = copy.deepcopy(runtime_node.ast_node)
        if ast_node.left_argument.reference_name in scope:
            self.__set_drone(ast_node, runtime_node.drones[0], scope)

        return ast_node

    def __slice_patrol(self, node, runtime, scope):
        self.__add_action_definition('Straight')
        runtime_node = runtime.get_node_from_ast(node)
        ast_node = runtime_node.ast_node

        new_sequential = Coordination.sequential()
        current_index = runtime_node.reached_patrol_index + 1
        len_area = len(runtime_node.patrol_area.boundingBox)
        while current_index < len_area:
            straight = ActionCall('Straight')
            straight.add_argument(Argument(f'{ast_node.arguments[0].reference_name}[{current_index%(len_area - 1)}]'))

            if ast_node.left_argument.reference_name in scope:
                self.__set_drone(straight, runtime_node.drones[0], scope)
            else:
                straight.left_argument = copy.deepcopy(ast_node.left_argument)

            new_sequential.add_node(straight)
            current_index += 1

        return new_sequential

    def __add_action_definition(self, action_name):
        if action_name == 'Straight':
            for definition in self.__new_program.operation.action_definitions:
                if definition.name == action_name:
                    return
            straight = ActionDefinition('Straight')
            param = Parameter()
            param.is_array = False
            param.type = "NavigationPoint"
            straight.add_parameter(param)
            self.__new_program.operation.action_definitions.append(straight)

    def __set_drone(self, ast_node, used_drone, scope):
        reference_name = ast_node.left_argument.reference_name
        if(reference_name in scope['__replaced']):
            ast_node.left_argument.reference_name = scope['__replaced'][reference_name]
            return

        parameter = scope[ast_node.left_argument.reference_name]
        drones = self.__new_program.arguments[parameter]
        used_drone_index = 0
        for index, drone in enumerate(drones):
            if drone.uuid == used_drone.uuid:
                used_drone_index = index
        scope['__replaced'][reference_name] = f'{parameter}[{used_drone_index}]'
        ast_node.left_argument.reference_name = scope['__replaced'][reference_name]

    def __replaced_drone(self, ast_node, scope):
        for node in ast_node.nodes:
            if isinstance(node, ActionCall):
                reference_name = node.left_argument.reference_name
                if reference_name in scope['__replaced']:
                    node.left_argument.reference_name = scope['__replaced'][reference_name]

            else:
                self.__replaced_drone(node, scope)