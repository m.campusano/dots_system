

class UtmManager:

    def __init__(self):
        self.operations = []

    def add_operation(self, operation):
        self.operations.append(operation)

    def get_operations_of_program(self, program):
        return list(filter(lambda operation: operation.program.get_id() == program.get_id(), self.operations))

    def get_program(self, program):
        operation = next(filter(lambda operation: operation.program.get_id() == program.get_id(), self.operations))
        return operation.program