from .abstract_node import ActionableNode

from os import linesep
import copy

class Constraint(ActionableNode):

    def to_dots(self, stream, indent = 0):
        super().to_dots(stream, indent)
        stream.write('constraint')
        self._write_specific_constraint(stream)
        self.body_to_dots(stream, indent+1)
        stream.write(linesep)
        super().to_dots(stream, indent)
        stream.write('end')

    def _write_specific_constraint(self, stream):
        pass
