from abc import ABC, abstractmethod


## TODO: fix this - probably better visitor/composite pattern
## probably because the limitation of OOP and a more FP paradigm is better (open types and pattern matching)

class VisitorMission(ABC):
    
    # @abstractmethod
    def visit_action_call(self, element):
        pass

    # @abstractmethod
    def visit_mission(self, element):
        pass

    # @abstractmethod
    def visit_coordination(self, element):
        pass

    # @abstractmethod
    def visit_geofence_constraint(self, element):
        pass

    # @abstractmethod
    def visit_no_fly_zone_constraint(self, element):
        pass

    # @abstractmethod
    def visit_start_time_constraint(self, element):
        pass

    # @abstractmethod
    def visit_end_time_constraint(self, element):
        pass

    ## this method assume element with body, which is not true for every element
    def visit_node_with_body(self, element):
        for node in element.nodes:
            node.accept(self)

class VisitorTemplate(VisitorMission):

    # @abstractmethod
    def visit_loop(self, element):
        pass    

class VisitorDots(VisitorTemplate):

    # @abstractmethod
    def visit_action_definition(self, element):
        pass

    # @abstractmethod
    def visit_argument(self, element):
        pass

    # @abstractmethod
    def visit_parameter(self, element):
        pass

    # @abstractmethod
    def visit_operation(self, element):
        pass