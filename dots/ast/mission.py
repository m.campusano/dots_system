from os import linesep
from .abstract_node import ActionableNode
from .visitor import VisitorMission

##TODO: restriction over missions: it can only has one Coordination node
class Mission(ActionableNode):

    @property
    def node(self):
        return self.nodes[0]
    
    @node.setter
    def node(self, node):
        self.nodes = [node]

    def accept(self, visitor: VisitorMission):
        return visitor.visit_mission(self)

    def to_dots(self, stream, indent = 0):
        super().to_dots(stream, indent)
        stream.write('implementation:')
        if self.has_body():
            self.node.body_to_dots(stream, indent + 1)

    def all_no_fly_zone_references(self):
        return self.node.all_no_fly_zone_references()