from .constraint import Constraint
from .visitor import VisitorMission

class Geofence(Constraint):

    def __init__(self, reference_name):
        super().__init__()
        self.area_reference_name = reference_name

    def accept(self, visitor: VisitorMission):
        return visitor.visit_geofence_constraint(self)

    def _write_specific_constraint(self, stream):
        stream.write(' inside ')
        stream.write(self.area_reference_name)
