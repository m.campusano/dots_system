from .constraint import Constraint
from .visitor import VisitorMission

class EndTime(Constraint):

    def __init__(self, reference_name):
        super().__init__()
        self.time_reference_name = reference_name

    def accept(self, visitor: VisitorMission):
        return visitor.visit_end_time_constraint(self)

    def _write_specific_constraint(self, stream):
        stream.write(' before ')
        stream.write(self.time_reference_name)