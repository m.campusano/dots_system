from io import StringIO
from inflection import underscore

from .abstract_node import DotsNode
from .visitor import VisitorDots

class Parameter(DotsNode):

    count = 0

    def __init__(self):
        self.__name = None
        self.is_array = None
        self.type = None
        self.__fake_name = ''

    @property
    def name(self):
        if self.__name is None:
            if self.__fake_name is None:
                Parameter.count = Parameter.count + 1
                self.__fake_name = "param"+str(Parameter.count)
            return self.__fake_name
        return self.__name

    @name.setter
    def name(self, name):
        self.__name = name

    def to_dots(self, stream, indent = 0):
        super().to_dots(stream, indent)
        if self.__name is not None:
            stream.write(self.__name)
            stream.write(' : ')
        stream.write(underscore(self.type))
        if self.is_array:
            stream.write('[]')

    def accept(self, visitor: VisitorDots):
        return visitor.visit_parameter(self)