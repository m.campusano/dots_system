from .constraint import Constraint
from .visitor import VisitorMission

class NoFlyZone(Constraint):

    def __init__(self, reference_name):
        super().__init__()
        self.area_reference_name = reference_name

    def accept(self, visitor: VisitorMission):
        return visitor.visit_no_fly_zone_constraint(self)

    def _write_specific_constraint(self, stream):
        stream.write(' outside ')
        stream.write(self.area_reference_name)

    def all_no_fly_zone_references(self):
        references = super().all_no_fly_zone_references()
        references.append(self.area_reference_name)
        return references