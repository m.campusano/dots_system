from .abstract_node import DotsNode
from io import StringIO
from .visitor import VisitorDots

class Argument(DotsNode):

    def __init__(self, reference_name):
        self.reference_name = reference_name

    def is_array(self):
        return ('[' in self.reference_name)

    def array_reference(self):
        return self.reference_name.split('[')[0]

    def array_index(self):
        return int(self.reference_name.split('[')[1].split(']')[0])


    def to_dots(self, stream, indent = 0):
        super().to_dots(stream, indent)
        stream.write(self.reference_name)

    def accept(self, visitor: VisitorDots):
        return visitor.visit_argument(self) 