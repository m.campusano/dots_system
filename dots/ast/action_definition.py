from .abstract_node import DotsNode
from io import StringIO
from .visitor import VisitorDots

class ActionDefinition(DotsNode):

    def __init__(self, name):
        self.name = name
        self.parameters = []

    def has_parameters(self):
        return not(len(self.parameters) == 0)

    def add_parameter(self, parameter):
        self.parameters.append(parameter)

    def to_dots(self, stream, indent = 0):
        super().to_dots(stream, indent)
        stream.write('import action ')
        stream.write(self.name)
        if self.has_parameters():
            stream.write('<')
            self.parameters[0].to_dots(stream)
            for parameter in self.parameters[1:]:
                stream.write(',')
                parameter.to_dots(stream)
            stream.write('>')

    def accept(self, visitor: VisitorDots):
        return visitor.visit_action_definition(self) 