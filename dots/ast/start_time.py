from .constraint import Constraint
from .visitor import VisitorMission

class StartTime(Constraint):

    def __init__(self, reference_name):
        super().__init__()
        self.time_reference_name = reference_name

    def accept(self, visitor: VisitorMission):
        return visitor.visit_start_time_constraint(self)

    def _write_specific_constraint(self, stream):
        stream.write(' after ')
        stream.write(self.time_reference_name)
