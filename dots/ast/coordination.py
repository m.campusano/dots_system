from os import linesep
from enum import Enum, auto
from .abstract_node import ActionableNode
from .visitor import VisitorMission

class Coordination(ActionableNode):

    class Type(Enum):
       PARALLEL = auto()
       PRIORITIZED = auto()
       SEQUENTIAL = auto()

    def __init__(self, type):
        super().__init__()
        self.type = type
        self.drone_reference_name = None
        self.drone_list_reference_name = None

    @classmethod
    def parallel(cls):
        return cls(cls.Type.PARALLEL)

    @classmethod
    def prioritized(cls):
        return cls(cls.Type.PRIORITIZED)

    @classmethod
    def sequential(cls):
        return cls(cls.Type.SEQUENTIAL)

    def is_parallel(self):
        return self.type == Coordination.Type.PARALLEL

    def is_sequential(self):
        return self.type == Coordination.Type.SEQUENTIAL

    def is_prioritized(self):
        return self.type == Coordination.Type.PRIORITIZED

    def is_using_drones(self):
        return not (self.drone_list_reference_name is None)

    def accept(self, visitor: VisitorMission):
        return visitor.visit_coordination(self)

    def to_dots(self, stream, indent = 0):
        super().to_dots(stream, indent)
        if self.is_parallel():
            stream.write('parallel')
        elif self.is_prioritized():
            stream.write('prioritized')
        elif self.is_sequential():
            stream.write('sequential')
        if self.is_using_drones():
            stream.write(' using ')
            stream.write(self.drone_reference_name)
            stream.write(' from ')
            stream.write(self.drone_list_reference_name)
        self.body_to_dots(stream, indent + 1)
        stream.write(linesep)
        super().to_dots(stream, indent)
        stream.write('end')
