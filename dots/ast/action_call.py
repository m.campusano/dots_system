from .abstract_node import ActionableNode
from .visitor import VisitorMission
from .coordination import Coordination
from .argument import Argument

class ActionCall(ActionableNode):

    def __init__(self, action_name):
        super().__init__()
        self.action_name = action_name
        self.left_argument = None
        self.arguments = []

    def has_arguments(self):
        return not (len(self.arguments) == 0)

    def accept(self, visitor: VisitorMission):
        return visitor.visit_action_call(self) 

    def add_argument(self, argument):
        self.arguments.append(argument)

    def to_dots(self, stream, indent = 0):
        super().to_dots(stream, indent)
        self.left_argument.to_dots(stream)
        stream.write(' += ')
        stream.write(self.action_name)
        stream.write('(')
        if self.has_arguments():
            self.arguments[0].to_dots(stream)
            for argument in self.arguments[1:]:
                stream.write(',')
                argument.to_dots(stream)
        stream.write(')')

    def accept(self, visitor: VisitorMission):
        return visitor.visit_action_call(self) 