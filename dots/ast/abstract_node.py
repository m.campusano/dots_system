import copy
from io import StringIO
from os import linesep
from abc import ABC, abstractmethod

from .visitor import VisitorDots, VisitorTemplate, VisitorMission

class DotsNode(ABC):

    def to_dots_string(self):
        stream = StringIO()
        self.to_dots(stream)
        return stream.getvalue()

    @abstractmethod
    def to_dots(self, stream, indent = 0):
        for i in range(indent):
            stream.write('\t')

    @abstractmethod
    def accept(self, visitor: VisitorDots):
        pass

class AbstractNode(DotsNode):

    def __init__(self):
       self.nodes = []
       self.parent = None

    def add_node(self, ast_node):
        self.nodes.append(ast_node)

    def has_body(self):
        return not (len(self.nodes) == 0)

    def copy_no_body(self):
        original_body = self.nodes
        original_parent = self.parent
        self.nodes = []
        self.parent = None
        copied = copy.deepcopy(self)
        self.nodes = original_body
        self.parent = original_parent
        return copied

    def to_dots_string(self):
        stream = StringIO()
        self.to_dots(stream)
        return stream.getvalue()

    def body_to_dots(self, stream, indent = 0):
        for node in self.nodes:
            stream.write(linesep)
            node.to_dots(stream, indent)

    def exchange_node(self, old_element, new_element):
        index = self.nodes.index(old_element)
        self.nodes[index] = new_element

    def all_no_fly_zone_references(self):
        no_fly_zones = []
        for node in self.nodes:
            no_fly_zones = no_fly_zones + node.all_no_fly_zone_references()
        return no_fly_zones

    @abstractmethod
    def accept(self, visitor: VisitorTemplate):
        pass

class ActionableNode(AbstractNode):
    @abstractmethod
    def accept(self, visitor: VisitorMission):
        pass