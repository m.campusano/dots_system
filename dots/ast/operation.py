from os import linesep
from .abstract_node import DotsNode
from dots.core.type import Drone, NavigationPoint, Area, Time
from .visitor import VisitorDots

import copy

class Operation(DotsNode):

    def __init__(self, name):
        self.name = name
        self.action_definitions = []
        self.parameters = {}
        self.mission = None
        self.utm = None

    def get_parameters(self):
        return self.parameters.values()

    def get_parameters_as_dictionary(self):
        parameters_dictionary = {}
        for parameter in self.get_parameters():
            parameter_dictionary = globals()[parameter.type].as_dictionary()
            if parameter.is_array:
                parameters_dictionary[parameter.name] = [parameter_dictionary]
            else:
                parameters_dictionary[parameter.name] = parameter_dictionary

        return parameters_dictionary

    def has_mission(self):
        return self.mission is not None

    def to_dots(self, stream, indent = 0):
        super().to_dots(stream, indent)
        stream.write('operation ')
        stream.write(self.name)
        stream.write(':')
        for action_definition in self.action_definitions:
            stream.write(linesep)
            action_definition.to_dots(stream, indent + 1)
        for key, parameter in self.parameters.items():
            stream.write(linesep)
            parameter.to_dots(stream, indent + 1)
        if self.has_mission():
            stream.write(linesep)
            self.mission.to_dots(stream, indent + 1)

    def accept(self, visitor: VisitorDots):
        return visitor.visit_operation(self)