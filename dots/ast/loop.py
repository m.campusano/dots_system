from os import linesep
from enum import Enum, auto
from .abstract_node import AbstractNode
from .coordination import Coordination
from .visitor import VisitorTemplate

class Loop(AbstractNode):

    class Type(Enum):
       PARALLEL = auto()
       PRIORITIZED = auto()
       SEQUENTIAL = auto()

    def __init__(self, type):
        super().__init__()
        self.type = type
        self.location_reference_name = None
        self.location_list_reference_name = None
        self.drone_reference_name = None
        self.drone_list_reference_name = None

    @classmethod
    def from_type(cls, string_type):
        if string_type == 'parallel':
            return cls(cls.Type.PARALLEL)
        elif string_type == 'prioritized':
            return cls(cls.Type.PRIORITIZED)
        elif string_type == 'sequential':
            return cls(cls.Type.SEQUENTIAL)
        return None

    def is_parallel(self):
        return self.type == Loop.Type.PARALLEL

    def is_sequential(self):
        return self.type == Loop.Type.SEQUENTIAL

    def is_prioritized(self):
        return self.type == Loop.Type.PRIORITIZED

    def is_using_drones(self):
        return not (self.drone_list_reference_name is None)

    def accept(self, visitor: VisitorTemplate):
        return visitor.visit_loop(self)

    def to_dots(self, stream, indent = 0):
        super().to_dots(stream, indent)
        if self.is_parallel():
            stream.write('parallel')
        elif self.is_prioritized():
            stream.write('prioritized')
        elif self.is_sequential():
            stream.write('sequential')
        self.body_to_dots(stream, indent + 1)
        stream.write(linesep)
        super().to_dots(stream, indent)
        stream.write('end')