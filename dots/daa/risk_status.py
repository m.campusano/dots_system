from collections import UserList 

class RiskStatus:

    def __init__(self):
        self.code = 0

    def is_risky(self):
        return self.code > 0

class RiskStatusCollection(UserList):

    def is_risky(self):
        for data in self:
            if data.is_risky():
                return True
        return False