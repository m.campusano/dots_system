import enum
import datetime

class Alert:

    def __init__(self):
        self.default_message = ''
        self.type = AlertType.INFORMATIVE
        self.risk_code = 0
        self.created_at = datetime.datetime.now().astimezone()

    def riskier_than(self, another_alert):
        if self.type == another_alert.type:
            return self.risk_code > another_alert.risk_code

        return self.type > another_alert.type

    def __str__(self):
        return f'Alert(code:{self.risk_code}, type:{self.type})'

    @classmethod
    def informative(cls):
        alert = cls()
        alert.type = AlertType.INFORMATIVE
        return alert

    @classmethod
    def preventive(cls):
        alert = cls()
        alert.type = AlertType.PREVENTIVE
        return alert

    @classmethod
    def corrective(cls):
        alert = cls()
        alert.type = AlertType.CORRECTIVE
        return alert

    @classmethod
    def warning(cls):
        alert = cls()
        alert.type = AlertType.WARNING
        return alert


class AlertType(enum.Enum):
    INFORMATIVE = 1
    PREVENTIVE = 2
    CORRECTIVE = 3
    WARNING = 4

    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.value >= other.value
        return NotImplemented
    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.value > other.value
        return NotImplemented
    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.value <= other.value
        return NotImplemented
    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented