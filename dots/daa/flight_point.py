from collections import UserList 

class FlightPoint:
    def __init__(self):
        self.seen = 0
        self.time = 0
        self.type = ''
        self.id = ''
        self.name = ''
        self.latitude = 0.0
        self.longitude = 0.0
        self.altitude = 0
        self.track = 0
        self.speed = 0.0
        self.point_id = 0
        self.hex = ''
        self.flarm_id = ''
        self.ais_id = ''
        self.landed = False
        self.sim = False

    def same_2d_position(self, another_point):
        return self.latitude == another_point.latitude and self.longitude == another_point.longitude

    def is_valid(self):
        return not(self.latitude == 0 or self.longitude == 0)

    def hex_or_multiple_ids(self):
        if self.has_hex():
            return self.hex
        return f'(type:{self.type},flarm_id:{self.flarm_id},ais_id:{self.ais_id})'

    def has_hex(self):
        return len(self.hex.strip()) != 0

    def __eq__(self, other):
        return self.hex_or_multiple_ids() == other.hex_or_multiple_ids()

    def __str__(self):
        return "FlightPoint{%s}(lat:%s, lon:%s, alt: %s, track:%s, speed:%s)" % (self.point_id, self.latitude, self.longitude, self.altitude, self.track, self.speed)

    def __repr__(self):
        return self.__str__()


class FlightPointCollection(UserList):


    def missing_points(self, newest_list):
        flights = self.__class__()
        for old_flight in self:
            if (not old_flight.sim) and (not old_flight in newest_list):
                flights.append(old_flight)

        return flights