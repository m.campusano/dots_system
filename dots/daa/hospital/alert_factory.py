from dots.daa import Alert

class AlertFactory:

    @classmethod
    def from_risk_status(cls, risk_status):
        alert = None
        if risk_status.code == 0:
            alert = Alert.informative()
            alert.default_message = 'Operate normally, there is no aircraft detected nearby'
            alert.risk_code = 0
        elif risk_status.code == 11:
            alert = Alert.preventive()
            alert.default_message = 'Keep the drone landed: an aircraft is inside the Detection Volume'
            alert.risk_code = 11
        elif risk_status.code == 12:
            alert = Alert.corrective()
            alert.default_message = 'Land the drone: an aircraft is inside the Detection Volume'
            alert.risk_code = 12
        elif risk_status.code == 21:
            alert = Alert.preventive()
            alert.default_message = 'Keep the drone landed: an aircraft is inside the Well Clear Volume'
            alert.risk_code = 21
        elif risk_status.code == 22:
            alert = Alert.warning()
            alert.default_message = 'Land the drone now!: an aircraft is inside the Well Clear Volume'
            alert.risk_code = 22
        else:
            alert = Alert.warning()
            alert.default_message = 'This is an unknown situation, please be careful'
            alert.risk_code = -1
        return alert

    @classmethod
    def from_ending_risk_status(cls, risk_status):
        alert = None
        if risk_status.code == 0:
            alert = Alert.informative()
            alert.default_message = 'Operate normally, there is no aircraft detected nearby'
            alert.risk_code = 0
        elif risk_status.code == 11:
            alert = Alert.informative()
            alert.default_message = 'You can continue the operation: the aircraft has exited the Detection Volume'
            alert.risk_code = 11
        elif risk_status.code == 12:
            alert = Alert.informative()
            alert.default_message = 'You can continue the operation: the aircraft has exited the Detection Volume'
            alert.risk_code = 12
        elif risk_status.code == 21:
            alert = Alert.informative()
            alert.default_message = 'You can continue the operation: the aircraft has exited the Well Clear Volume'
            alert.risk_code = 21
        elif risk_status.code == 22:
            alert = Alert.informative()
            alert.default_message = 'You can continue the operation: the aircraft has exited the Well Clear Volume'
            alert.risk_code = 22
        else:
            alert = Alert.warning()
            alert.default_message = 'This is an unknown situation, please be careful'
            alert.risk_code = -1
        return alert