from geopy.distance import geodesic

class USpace:

    EKOH_POINT = (55.3845, 10.36394) # latidude, longitude
    WC_RADIUS = 1160 # Well Clear radious in meters
    WC_ALTIDUDE = 102 # Well Clear altitude in meters
    DV_RADIUS = 3000 # Detection Volume radious in meters
    DV_ALTITUDE = 300 # Detection Volume altitude in meters

    WC_SAFE_ALTITUDE = 500 # We use this altitude because of meassures errors
    DV_SAFE_ALTITUDE = 500

    def is_inside_well_clear_volume(self, point, altitude):
        return altitude <= self.WC_SAFE_ALTITUDE \
                and geodesic(point, self.EKOH_POINT).meters < self.WC_RADIUS

    def is_inside_detection_volume(self, point, altitude):
        return altitude <= self.DV_SAFE_ALTITUDE \
                and geodesic(point, self.EKOH_POINT).meters < self.DV_RADIUS 