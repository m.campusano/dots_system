from dots.daa import RiskStatus
from .u_space import USpace
from dots.utils.decision_tree import DecisionNode, DecisionLeaf

## Decision Tree
class RiskIdentifier:

    def identify(self, operation_status, flight):
        return VolumeNode(operation_status, flight).decide().value()

## Decision Tree nodes and leafs
class VolumeNode(DecisionNode):

    def __init__(self, operation_status, flight):
        super().__init__(["no_detection", "inside_detection", "inside_well_clear"])

        self.u_space = USpace()
        self.operation_status = operation_status
        self.flight = flight

        self.add_child("no_detection", NoDetectionLeaf())
        self.add_child("inside_detection", InsideVolumeNode(self.operation_status))
        self.add_child("inside_well_clear", InsideWellClearNode(self.operation_status))

    def decide_function(self):
        if not self.operation_status.started:
            return 'no_detection'

        point = (self.flight.latitude, self.flight.longitude)
        altitude = self.flight.altitude
        if self.u_space.is_inside_well_clear_volume(point, altitude):
            return 'inside_well_clear'
        elif self.u_space.is_inside_detection_volume(point, altitude):
            return 'inside_detection'
        return "no_detection"

class NoDetectionLeaf(DecisionLeaf):

    def value(self):
        return RiskStatus()

class InsideVolumeNode(DecisionNode):
    def __init__(self, operation_status):
        super().__init__(['landed', 'flying'])
        self.operation_status = operation_status

        self.add_child("landed", InsideVolumeLandedLeaf())
        self.add_child("flying", InsideVolumeFlyingLeaf())

    def decide_function(self):
        if self.operation_status.is_flying:
            return 'flying'
        return 'landed'

class InsideVolumeLandedLeaf(DecisionLeaf):
    def value(self):
        risk = RiskStatus()
        risk.code = 11
        return risk

class InsideVolumeFlyingLeaf(DecisionLeaf):
    def value(self):
        risk = RiskStatus()
        risk.code = 12
        return risk

class InsideWellClearNode(DecisionNode):

    def __init__(self, operation_status):
        super().__init__(['landed', 'flying'])
        self.operation_status = operation_status

        self.add_child("landed", InsideWellClearLandedLeaf())
        self.add_child("flying", InsideWellClearFlyingLeaf())

    def decide_function(self):
        if self.operation_status.is_flying:
            return 'flying'
        return 'landed'

class InsideWellClearLandedLeaf(DecisionLeaf):
    def value(self):
        risk = RiskStatus()
        risk.code = 21
        return risk

class InsideWellClearFlyingLeaf(DecisionLeaf):
    def value(self):
        risk = RiskStatus()
        risk.code = 22
        return risk