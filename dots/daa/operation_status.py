
class OperationStatus:

    def __init__(self, started = False):
        self.started = started
        self.is_flying = False

    def fly(self):
        self.is_flying = True

    def land(self):
        self.is_flying = False

    def start(self):
        self.started = True

    def stop(self):
        self.started = False
        self.land()