from xml.etree import ElementTree
from dots.ast import Operation, Parameter, Mission, ActionDefinition, \
					Coordination, ActionCall, Argument, Loop, \
					Geofence, NoFlyZone, StartTime, EndTime

import distutils.core

class DotsXML:

	
	def parse(self, xmlString):
		root = None
		root = ElementTree.fromstring(xmlString)

		return self.parseOperation(root)

	def parseOperation(self, xmlElement):
		operation = Operation(xmlElement.get("name"))

		operation.action_definitions = self.parseActionDefinitions(xmlElement)

		operation.parameters = self.parseParameters(xmlElement)
		self.parseMission(operation, xmlElement)

		return operation


	def parseParameters(self, xmlElement):
		parameters = {}
		for xmlParameter in xmlElement.findall("parameter"):
			parameter = Parameter()
			if "name" in xmlParameter.attrib:
				parameter.name = xmlParameter.get("name")
			parameter.type = xmlParameter.get("type")
			parameter.is_array = bool(distutils.util.strtobool(xmlParameter.get("array")))

			parameters[parameter.name] = parameter

		return parameters

	def parseMission(self, operation, xmlElement):
		xmlMission = xmlElement.find("mission")
		if xmlMission is None:
			return

		mission = Mission()

		xmlSequence = xmlMission.find("sequential")
		if not (xmlSequence is None):
			mission.node = self.parseNode(xmlSequence)

		operation.mission = mission

	def parseActionDefinitions(self, xmlElement):
		definitions = []
		for xmlActionDefinition in xmlElement.findall("action-definition"):
			action = ActionDefinition(xmlActionDefinition.get("name"))
			action.parameters = list(self.parseParameters(xmlActionDefinition).values())
			definitions.append(action)
		return definitions

	def parseNode(self, xmlElement):
		return getattr(self, "parse"+xmlElement.tag.capitalize())(xmlElement)

	def parseNodes(self, xmlParentElement):
		nodes = []
		for xmlElement in xmlParentElement:
			nodes.append(self.parseNode(xmlElement))
		return nodes

	def parseSequential(self, xmlElement):
		sequential = Coordination.sequential()
		self.parseCoordination(sequential, xmlElement)
		sequential.nodes = self.parseNodes(xmlElement)
		return sequential

	def parseParallel(self, xmlElement):
		parallel = Coordination.parallel()
		self.parseCoordination(parallel, xmlElement)
		parallel.nodes = self.parseNodes(xmlElement)
		return parallel

	def parsePrioritized(self, xmlElement):
		prioritized = Coordination.prioritized()
		self.parseCoordination(prioritized, xmlElement)
		prioritized.nodes = self.parseNodes(xmlElement)
		return prioritized


	def parseCoordination(self, coordination, xmlElement):
		self.parseUsingDrones(coordination, xmlElement)

	def parseAction(self, xmlElement):
		actionCall = ActionCall(xmlElement.get("name"))
		actionCall.left_argument = Argument(xmlElement.find("left").get("reference"))
		actionCall.arguments = self.parseArguments(xmlElement)
		return actionCall

	def parseArguments(self, xmlElement):
		arguments = []
		for xmlArgument in xmlElement.findall("argument"):
			argument = Argument(xmlArgument.get("reference"))
			arguments.append(argument)

		return arguments

	def parseConstraint(self, xmlElement):
		constraintType = xmlElement.get("type")
		typeStringList = list(constraintType)
		typeStringList[0] = typeStringList[0].upper()
		typeCammelCase = "".join(typeStringList)
		constraint = getattr(self, "parseConstraint"+typeCammelCase)(xmlElement)

		constraint.nodes = self.parseNodes(xmlElement)

		return constraint

	def parseConstraintGeofence(self, xmlElement):
		return Geofence(xmlElement.get("variable"))

	def parseConstraintNoFlyZone(self, xmlElement):
		return NoFlyZone(xmlElement.get("variable"))

	def parseConstraintStartTime(self, xmlElement):
		return StartTime(xmlElement.get("variable"))

	def parseConstraintEndTime(self, xmlElement):
		return EndTime(xmlElement.get("variable"))

	def parseFor(self, xmlElement):
		loop = Loop.from_type(xmlElement.get("type"))
		loop.location_list_reference_name = xmlElement.get("location-list")
		loop.location_reference_name = xmlElement.get("location")
		self.parseUsingDrones(loop, xmlElement)
		loop.nodes = self.parseNodes(xmlElement)
		return loop

	def parseUsingDrones(self, ast, xmlElement):
		if "drone-list" in xmlElement.attrib:
			ast.drone_list_reference_name = xmlElement.get("drone-list")
			ast.drone_reference_name = xmlElement.get("drone")
