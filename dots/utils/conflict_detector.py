from pyvisgraph.graph import Graph, Point as VGPoint
from pyvisgraph.visible_vertices import intersect_point

class ConflictDetector:

    def plan_in_no_fly_zones(self, operation_plan, no_fly_zones):
        polygons = []
        for no_fly_zone in no_fly_zones:
            polygons.append(list(map(lambda p: VGPoint(p.latitude, p.longitude), no_fly_zone.boundingBox)))
        graph = Graph(polygons)

        for drone_plan in operation_plan.drone_plans:
            previous_point = None
            for point in drone_plan.path:
                if previous_point is None:
                    previous_point = point
                    continue
                vgPreviousPoint = VGPoint(previous_point.latitude, previous_point.longitude)
                vgPoint = VGPoint(point.latitude, point.longitude)
                for edge in graph.edges:
                    if intersect_point(vgPreviousPoint, vgPoint, edge) is not None:
                        return True
                previous_point = point
        return False
        assert g.point_in_polygon(point_d) != -1

    def concurrent_plans(self, plan, other_plan):
        if other_plan.get_end_time().time < plan.get_start_time().time:
            return False
        if plan.get_end_time().time < other_plan.get_start_time().time:
            return False
        return True