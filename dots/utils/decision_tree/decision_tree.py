
from abc import ABC, abstractmethod

class AbstractNode(ABC):
    
    def is_inner_node(self):
        return False

    def is_leaf(self):
        return False

class DecisionNode(AbstractNode):

    def __init__(self, outcomes):
        self.outcomes = outcomes
        self.transitions = {}
    
    def add_child(self, from_outcome, node):
        self.transitions[from_outcome] = node

    def decide(self):
        node = self
        while node.is_inner_node():
            node = node.transitions[node.decide_function()]

        return node

    def is_inner_node(self):
        return True

    @abstractmethod
    def decide_function(self):
        pass
        

class DecisionLeaf(AbstractNode):

    def is_leaf(self):
        return True

    @abstractmethod
    def value(self):
        pass