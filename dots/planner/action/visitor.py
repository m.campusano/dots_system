from abc import ABC, abstractmethod


class Visitor(ABC):
    
    @abstractmethod
    def visit_straight_action(self, element):
        pass

    @abstractmethod
    def visit_patrol_action(self, element):
        pass

    @abstractmethod
    def visit_sequential_action(self, element):
        pass

    @abstractmethod
    def visit_parallel_action(self, element):
        pass

    @abstractmethod
    def visit_prioritized_action(self, element):
        pass