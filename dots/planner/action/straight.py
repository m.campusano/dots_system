from .abstract_action import AbstractAction
from .visitor import Visitor

class Straight(AbstractAction):

    def __init__(self, ast, modifiers, drones, location):
        super().__init__(modifiers, drones)
        self.navigation_point = location
        self.ast_element = ast

    def accept(self, visitor: Visitor):
        visitor.visit_straight_action(self)