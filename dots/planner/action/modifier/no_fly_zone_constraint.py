from .action_modifier import ActionModifier

class NoFlyZoneConstraint(ActionModifier):
    
    def __init__(self, area):
        self.no_fly_zone = area