from .action_modifier import ActionModifier

class GeofenceConstraint(ActionModifier):
    
    def __init__(self, area):
        self.geofence = area