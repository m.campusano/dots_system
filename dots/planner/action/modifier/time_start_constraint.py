from .action_modifier import ActionModifier

class TimeStartConstraint(ActionModifier):
    
    def __init__(self, time):
        self.time = time