from dots.ast import VisitorMission, ActionCall, Mission, Coordination, Geofence, NoFlyZone, StartTime, EndTime, Loop

from dots.planner.action import Straight, Patrol, SequentialAction, PrioritizedAction, ParallelAction
from dots.planner.action.modifier import GeofenceConstraint, NoFlyZoneConstraint, TimeStartConstraint, TimeEndConstraint
from dots.core.type import UndefinedDrone

from queue import LifoQueue
from copy import copy

class ActionBuilder(VisitorMission):
    
    def __init__(self, program):
        self.program = program
        self.actions = []
        self.modifiers = LifoQueue()
        self.using_drones = {}

    def build(self):
        self.program.operation.mission.accept(self)

    def visit_action_call(self, element: ActionCall):
        # Straight action
        # ActionCall with only 1 argument
        if element.action_name.lower() == 'straight':
            self.visit_straight_call(element)
        elif element.action_name.lower() == 'patrol':
            self.visit_patrol_call(element)
    
    def visit_straight_call(self, element: ActionCall):
        drone = None
        if element.left_argument.is_array():
            drone = self.get_argument_value(element.left_argument)
        elif element.left_argument.reference_name in self.program.arguments:
            drone = self.program.arguments[element.left_argument.reference_name]
        else:
            drone = UndefinedDrone(element.left_argument.reference_name, self.using_drones[element.left_argument.reference_name])
        self.actions.append(
            Straight(
                element,
                list(self.modifiers.queue),
                [drone],
                self.get_argument_value(element.arguments[0])
            )
        )

    def visit_patrol_call(self, element: ActionCall):
        self.actions.append(
            Patrol(
                element,
                list(self.modifiers.queue),
                [self.program.arguments[element.left_argument.reference_name]],
                self.get_argument_value(element.arguments[0])
            )
        )

    def visit_mission(self, element: Mission):
        element.node.accept(self)

    def visit_coordination(self, element: Coordination):
        coordination_action_cls = None
        if element.is_sequential():
            coordination_action_cls = SequentialAction
        elif element.is_prioritized():
            coordination_action_cls = PrioritizedAction
        elif element.is_parallel():
            coordination_action_cls = ParallelAction

        drones = []
        drone_name = None
        if element.is_using_drones():
            drones = self.program.arguments[element.drone_list_reference_name]
            drone_name = element.drone_reference_name
        coordination_action = coordination_action_cls(list(self.modifiers.queue), drones)
        coordination_action.drone_reference_name = drone_name

        self.actions.append(coordination_action)
        builder = ActionBuilder(self.program)
        builder.modifiers = copy(self.modifiers)
        if element.is_using_drones():
            builder.using_drones[drone_name] = drones
        for node in element.nodes:
            node.accept(builder)

        coordination_action.actions = builder.actions


    def visit_geofence_constraint(self, element: Geofence):
        self.modifiers.put(GeofenceConstraint(self.program.arguments[element.area_reference_name]))
        self.__after_visit_constraint(element)

    def visit_no_fly_zone_constraint(self, element: NoFlyZone):
        self.modifiers.put(NoFlyZoneConstraint(self.program.arguments[element.area_reference_name]))
        self.__after_visit_constraint(element)

    def visit_start_time_constraint(self, element: StartTime):
        self.modifiers.put(TimeStartConstraint(self.program.arguments[element.time_reference_name]))
        self.__after_visit_constraint(element)

    def visit_end_time_constraint(self, element: EndTime):
        self.modifiers.put(TimeEndConstraint(self.program.arguments[element.time_reference_name]))
        self.__after_visit_constraint(element)

    def __after_visit_constraint(self, element):
        for node in element.nodes:
            node.accept(self)
        self.modifiers.get()

    def get_argument_value(self, ast_argument):
        if ast_argument.is_array():
            return self.program.arguments[ast_argument.array_reference()][ast_argument.array_index()]
        else:
            return self.program.arguments[ast_argument.reference_name]