from .abstract_action import AbstractAction
from .visitor import Visitor

class CoordinationAction(AbstractAction):

    def __init__(self, modifiers, drones):
        super().__init__(modifiers, drones)
        self.actions = []
        self.drone_reference_name = None

class ParallelAction(CoordinationAction):
    
    def accept(self, visitor: Visitor):
        visitor.visit_parallel_action(self)

class PrioritizedAction(CoordinationAction):
    
    def accept(self, visitor: Visitor):
        visitor.visit_prioritized_action(self)

class SequentialAction(CoordinationAction):
    
    def accept(self, visitor: Visitor):
        visitor.visit_sequential_action(self)