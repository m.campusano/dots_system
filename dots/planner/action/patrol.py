from .abstract_action import AbstractAction
from .visitor import Visitor

class Patrol(AbstractAction):

    def __init__(self, ast, modifiers, drones, area):
        super().__init__(modifiers, drones)
        self.area = area
        self.ast_element = ast

    def accept(self, visitor: Visitor):
        visitor.visit_patrol_action(self)