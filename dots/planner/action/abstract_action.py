from abc import ABC, abstractmethod

from .modifier import GeofenceConstraint, NoFlyZoneConstraint, TimeStartConstraint, TimeEndConstraint
from .visitor import Visitor

class AbstractAction(ABC):

    def __init__(self, modifiers, drones):
        self.drones = drones
        self.modifiers = modifiers

    def get_geofence_constraints(self):
        return list(filter(lambda modifier: type(modifier) == GeofenceConstraint, self.modifiers))

    def get_no_fly_zone_constraints(self):
        return list(filter(lambda modifier: type(modifier) == NoFlyZoneConstraint, self.modifiers))

    def get_after_time_constraints(self):
        return list(filter(lambda modifier: type(modifier) == TimeStartConstraint, self.modifiers))

    def get_before_time_constraints(self):
        return list(filter(lambda modifier: type(modifier) == TimeEndConstraint, self.modifiers))

    def get_geofences(self):
        return list(map(lambda geofence: geofence.geofence, self.get_geofence_constraints()))

    def get_no_fly_zones(self):
        return list(map(lambda no_fly_zone: no_fly_zone.no_fly_zone, self.get_no_fly_zone_constraints()))

    def get_lastest_start_time(self):
        return_time = None
        for after_time_constraint in self.get_after_time_constraints():
            if return_time is None:
                return_time = after_time_constraint.time
                continue
            if return_time.time < after_time_constraint.time.time:
                return_time = after_time_constraint.time
        return return_time

    def get_earliest_end_time(self):
        return_time = None
        for after_time_constraint in self.get_before_time_constraints():
            if return_time is None:
                return_time = after_time_constraint.time
                continue
            if return_time.time > after_time_constraint.time.time:
                return_time = after_time_constraint.time
        return return_time

    @abstractmethod
    def accept(self, visitor: Visitor):
        pass