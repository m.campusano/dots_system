from .action import Patrol
from .default_straight_planner import DefaultStraightPlanner

class DefaultPatrolPlanner:

    def __init__(self, action: Patrol, drone, last_path_point):
        self.action = action
        self.drone = drone
        self.last_path_point = last_path_point

    def plan(self, no_fly_zones = []):

        plan = []
        last_point = self.last_path_point
        for point in self.action.area.boundingBox:
            plan = plan + DefaultStraightPlanner(self.action, self.drone, point, last_point).plan(no_fly_zones)
            last_point = plan[-1]
        plan = plan + DefaultStraightPlanner(self.action, self.drone, self.action.area.boundingBox[0], last_point).plan(no_fly_zones)

        return plan


    def __get_duration_between_points(self, point_a, point_b):
        # https://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters
        # One movement in lat lon = 111km
        # We can move about 11 meters per second -> 0.0001 lat lon
        
        speed = 0.0001; # meters per second
        distance = point_a.distance(point_b);
        duration = distance / speed;
        return datetime.timedelta(seconds = duration)