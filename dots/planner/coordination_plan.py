from dots.core.plan import Plan
from dots.core.type import Time

class CoordinationPlan(Plan):
    def __init__(self):
        self.plans = []
        self.defined_drones = {}

    def add_defined_drone(self, undefined_drone, drone):
        self.defined_drones[undefined_drone.reference_name] = drone

    def is_drone_defined(self, undefined_drone):
        return undefined_drone.reference_name in self.defined_drones

    def get_defined_drone(self, undefined_drone):
        return self.defined_drones[undefined_drone.reference_name]

    def set_start_time(self, time):
        pass

    def get_drones(self):
        drones = []
        for plan in self.plans:
            drones = drones + plan.get_drones()
        return drones

class SequentialCoordinationPlan(CoordinationPlan):

    def set_start_time(self, time):
        previous_time = time
        for plan in self.plans:
            plan.set_start_time(previous_time)
            previous_time = plan.get_end_time()

    def get_start_time(self):
        return self.plans[0].get_start_time()

    def get_end_time(self):
        return self.plans[-1].get_end_time()

    def get_drones(self):
        drones = []
        for plan in self.plans:
            drones = drones + plan.get_drones()
        return drones

## Parallel coordination time
class ParallelCoordinationPlan(CoordinationPlan):

    def set_start_time(self, time):
        drone_time = {}
        for plan in self.plans:
            start_time = time
            for drone in plan.get_drones():
                if drone in drone_time:
                    start_time = drone_time[drone]
                plan.set_start_time(start_time)
                drone_time[drone] = plan.get_end_time()

    def get_start_time(self):
        min_time = None
        for plan in self.plans:
            if min_time is None:
                min_time = plan.get_end_time().time
                continue

            plan_time = plan.get_end_time().time
            if min_time > plan_time:
                min_time = plan_time
        return Time(min_time)

    def get_end_time(self):
        max_time = None
        for plan in self.plans:
            if max_time is None:
                max_time = plan.get_end_time().time
                continue

            plan_time = plan.get_end_time().time
            if max_time < plan_time:
                max_time = plan_time
        return Time(max_time)

    def get_drones(self):
        drones = []
        for plan in self.plans:
            drones = drones + plan.get_drones()
        return drones


## Prioritized coordination time
class PrioritizedCoordinationPlan(CoordinationPlan):

    def set_start_time(self, time):
        drone_time = {}
        start_time = time
        for plan in self.plans:
            for drone in plan.get_drones():
                if drone in drone_time:
                    if start_time.time < drone_time[drone].time:
                        start_time = drone_time[drone]
                plan.set_start_time(start_time)
                drone_time[drone] = plan.get_end_time()

    def get_start_time(self):
        min_time = None
        for plan in self.plans:
            if min_time is None:
                min_time = plan.get_end_time().time
                continue

            plan_time = plan.get_end_time().time
            if min_time > plan_time:
                min_time = plan_time
        return Time(min_time)

    def get_end_time(self):
        max_time = None
        for plan in self.plans:
            if max_time is None:
                max_time = plan.get_end_time().time
                continue

            plan_time = plan.get_end_time().time
            if max_time < plan_time:
                max_time = plan_time
        return Time(max_time)
