from .default_straight_planner import DefaultStraightPlanner
from .default_patrol_planner import DefaultPatrolPlanner
from dots.core.type import Time
from dots.core.plan import DronePlan, OperationPlan
from dots.utils import ConflictDetector
from .coordination_plan import SequentialCoordinationPlan, ParallelCoordinationPlan, PrioritizedCoordinationPlan
from .action import Visitor, Straight, Patrol, SequentialAction, ParallelAction, PrioritizedAction

from dots.planner.action import ActionBuilder

import datetime

class DefaultPlanner(Visitor):

    def __init__(self):
        self.__init_variables()
        self.conflict_detector = ConflictDetector()
        self.root_planner = self
        self.parent_plan = None

    def is_root(self):
        return self.root_planner == self

    def __init_variables(self):
        self.global_plan = OperationPlan()
        self.coordination_plan = None
        self.no_fly_zones = []
    
    def plan(self, program, no_fly_zones = [], planned_programs = []):

        self.__init_variables()

        builder = ActionBuilder(program)
        builder.build()
        actions = builder.actions

        self.__plan(actions, no_fly_zones)
        self.__set_times()

        concurrent_programs = []
        non_concurrent_programs = []
        for planned_program in planned_programs:
            if self.conflict_detector.concurrent_plans(self.global_plan, planned_program.operation_plan):
                concurrent_programs.append(planned_program)
            else:
                non_concurrent_programs.append(planned_program)
        
        if len(concurrent_programs) != 0:
            for concurrent_program in concurrent_programs:
                no_fly_zones.extend(concurrent_program.operation_plan.as_no_fly_zones())
            self.plan(program, no_fly_zones, non_concurrent_programs)

        program.add_new_plan(self.global_plan)
        return self.global_plan

    def re_plan(self, programs, no_fly_zones):
        conflicted_programs = []
        non_conflicted_programs = []
        for program in programs:
            if self.conflict_detector.plan_in_no_fly_zones(program.operation_plan, no_fly_zones):
                conflicted_programs.append(program)
            else:
                non_conflicted_programs.append(program)
        for program in conflicted_programs:
            self.plan(program, no_fly_zones, non_conflicted_programs)
            non_conflicted_programs.append(program)

        return conflicted_programs

    def __plan(self, actions, no_fly_zones):
        self.no_fly_zones = list(no_fly_zones)
        for action in actions:
            action.accept(self)

    def visit_straight_action(self, action: Straight):
        last_path_point = None
        drone = action.drones[0]
        available_drone = None
        
        if drone.is_undefined():
            available_drone = self.__get_available_drone(drone)
        else:
            available_drone = drone

        plan = self.global_plan.drone_plan(available_drone)
        if not(plan is None):
            last_path_point = plan.path[-1]
        else:
            plan = DronePlan(available_drone)
            self.global_plan.add_plan(plan)
        
        # add to global plan
        straight_plan = DefaultStraightPlanner(action, available_drone, action.navigation_point, last_path_point).plan(self.no_fly_zones)
        plan.add_waypoints(straight_plan)

        # add to local plan
        action_plan = DronePlan(available_drone)
        action_plan.add_waypoints(straight_plan)
        self.parent_plan.plans.append(action_plan)
        self.root_planner.__set_times()

    def visit_patrol_action(self, action: Patrol):
        last_path_point = None
        drone = action.drones[0]
        available_drone = None
        
        if drone.is_undefined():
            available_drone = self.__get_available_drone(drone)
        else:
            available_drone = drone

        plan = self.global_plan.drone_plan(available_drone)
        if not(plan is None):
            last_path_point = plan.path[-1]
        else:
            plan = DronePlan(available_drone)
            self.global_plan.add_plan(plan)
        
        # add to global plan
        straight_plan = DefaultPatrolPlanner(action, available_drone, last_path_point).plan(self.no_fly_zones)
        plan.add_waypoints(straight_plan)

        # add to local plan
        action_plan = DronePlan(available_drone)
        action_plan.add_waypoints(straight_plan)

    def visit_sequential_action(self, action: SequentialAction):
        self.__visit_coordination_action(action, SequentialCoordinationPlan)

    def visit_parallel_action(self, action: ParallelAction):
        self.__visit_coordination_action(action, ParallelCoordinationPlan)

    def visit_prioritized_action(self, action: PrioritizedAction):
        self.__visit_coordination_action(action, PrioritizedCoordinationPlan)

    def __visit_coordination_action(self, action, coordinationPlanClass):
        coordination_plan = coordinationPlanClass()
        self.coordination_plan = coordination_plan

        if not self.is_root():
            self.parent_plan.plans.append(coordination_plan)

        body_planner = DefaultPlanner()
        body_planner.global_plan = self.global_plan
        body_planner.root_planner = self.root_planner
        body_planner.parent_plan = coordination_plan
        body_planner.__plan(action.actions, self.no_fly_zones)
        # coordination_plan.plans = body_planner.local_plan
        self.root_planner.__set_times()

    def __set_times(self):
        # Root mission start always with sequential action
        self.coordination_plan.set_start_time(self.get_default_time())
        previous_time = self.get_default_time()

    ##
    # Get available drones for a the extended use d from ds contruction
    # If the planner already picked the drone beforehand, use the same
    # If there are available drones to use, get one of the list
    # If there are no available drones to use, get the first one available
    ##
    def __get_available_drone(self, undefined_drone):

        if self.parent_plan.is_drone_defined(undefined_drone):
            return self.parent_plan.get_defined_drone(undefined_drone)

        using_drone_list = undefined_drone.drones
        flying_drone_list = list(map(lambda plan: plan.drone, self.global_plan.drone_plans))

        # check for unnused drones
        unnused_drones = [drone for drone in using_drone_list if drone not in flying_drone_list]
        if len(unnused_drones) != 0:
            self.parent_plan.add_defined_drone(undefined_drone, unnused_drones[0])
            return unnused_drones[0]
        
        # get available drone from plan (all drones are being used)
        using_drone_uuid_list = list(map(lambda drone: drone.uuid, using_drone_list))
        available_plans = list(filter(lambda plan: plan.drone.uuid in using_drone_uuid_list, self.global_plan.drone_plans))

        earliest_time = None
        earliest_available_drone = None
        for available_plan in available_plans:
            if earliest_time is None:
                earliest_time = available_plan.get_end_time()
                earliest_available_drone = available_plan.drone
                continue

            if earliest_time.time > available_plan.get_end_time().time:
                earliest_time = available_plan.get_end_time()
                earliest_available_drone = available_plan.drone

        self.parent_plan.add_defined_drone(undefined_drone, earliest_available_drone)
        return earliest_available_drone


    @classmethod
    def get_default_time(cls):
        return Time(datetime.datetime.now().astimezone() + datetime.timedelta(days = 1))