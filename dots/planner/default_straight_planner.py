from .action import Straight
from dots.core.plan import PathPoint
from pyvisgraph import VisGraph, Point as VGPoint

import datetime

class DefaultStraightPlanner:

    def __init__(self, action, drone, destination, last_path_point):
        self.action = action
        self.destination = destination
        self.drone = drone
        self.last_path_point = last_path_point

    def plan(self, no_fly_zones = []):
        return_path = []
        first_point = None
        if self.last_path_point is None:
            first_point = VGPoint(self.drone.take_off_position.latitude, self.drone.take_off_position.longitude)
        else:
            first_point = VGPoint(self.last_path_point.latitude, self.last_path_point.longitude)
        last_point = VGPoint(self.destination.latitude, self.destination.longitude)

        polys = []
        polys.append([first_point])
        polys.append([last_point])

        # no fly zones
        all_no_fly_zones = []
        all_no_fly_zones.extend(self.action.get_no_fly_zones())
        all_no_fly_zones.extend(no_fly_zones)
        for no_fly_zone in all_no_fly_zones:
            polys.append(list(map(lambda p: VGPoint(p.latitude, p.longitude), no_fly_zone.boundingBox)))
        # geofences
        for geofence in self.action.get_geofences():
            polys.append(list(map(lambda p: VGPoint(p.latitude, p.longitude), geofence.boundingBox)))

        g = VisGraph()
        g.build(polys)

        shortest_path = list(map(lambda p: PathPoint(p.x, p.y, self.action), g.shortest_path(first_point, last_point)))
        
        last_point = None
        for point in shortest_path:
            if last_point is None:
                last_point = point
                continue

            point.flying_duration_from_previous_point = self.__get_duration_between_points(last_point, point)
            last_point = point

        # If we receive the last added point, we should not re-added again to the path
        if not(self.last_path_point is None):
            shortest_path.pop(0)


        return shortest_path


    def __get_duration_between_points(self, point_a, point_b):
        # https://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters
        # One movement in lat lon = 111km
        # We can move about 11 meters per second -> 0.0001 lat lon
        
        speed = 0.0001; # meters per second
        distance = point_a.distance(point_b);
        duration = distance / speed;
        return datetime.timedelta(seconds = duration)