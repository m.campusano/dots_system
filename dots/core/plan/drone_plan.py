from .plan import Plan
from dots.core.type import Time, Area, NavigationPoint
import datetime

class DronePlan(Plan):

    def __init__(self, drone):
        self.drone = drone
        self.path = []

    def add_waypoints(self, path_points):
        self.path.extend(path_points)

    def get_start_time(self):
        return self.path[0].depart_to_here_time

    def get_end_time(self):
        last_point = self.path[-1]
        return Time(last_point.depart_to_here_time.time + last_point.flying_duration_from_previous_point)

    # used only when path is produced for the same action
    def set_start_time(self, time):
        waypoint = self.path[0]
        start_time = time.time
        constraint_time = waypoint.action.get_lastest_start_time()
        if constraint_time and (constraint_time.time > start_time):
            start_time = constraint_time.time
        previous_waypoint = None
        for waypoint in self.path:
            if previous_waypoint is None:
                waypoint.depart_to_here_time = Time(start_time)
                previous_waypoint = waypoint
                continue
            waypoint.depart_to_here_time = Time(previous_waypoint.depart_to_here_time.time + previous_waypoint.flying_duration_from_previous_point)
            previous_waypoint = waypoint

    # an Area
    def as_no_fly_zone(self):
        no_fly_zone = Area()

        for point in self.path:
            no_fly_zone.add_point(NavigationPoint(point.latitude, point.longitude))

        reversed_path = self.path[::-1]

        for point in reversed_path[1:-1]:
            no_fly_zone.add_point(NavigationPoint(point.latitude, point.longitude))
        return no_fly_zone

    def get_drones(self):
        return [self.drone]