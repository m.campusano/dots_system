from dots.core.type import Time
from .plan import Plan

class OperationPlan(Plan):

    def __init__(self):
        self.drone_plans = []

    def drone_plan(self, drone):
        return self.drone_plan_from_id(drone.uuid)

    def drone_plan_from_id(self, drone_id):
        return next(filter(lambda plan: plan.drone.uuid == drone_id, self.drone_plans), None)

    def add_plan(self, plan):
        self.drone_plans.append(plan)

    def get_start_time(self):
        start_time = None
        for plan in self.drone_plans:
            if start_time is None:
                start_time = plan.get_start_time().time
                continue
            if start_time > plan.get_start_time().time:
                start_time = plan.get_start_time().time
        return Time(start_time)

    def get_end_time(self):
        end_time = None
        for plan in self.drone_plans:
            if end_time is None:
                end_time = plan.get_end_time().time
                continue
            if end_time < plan.get_end_time().time:
                end_time = plan.get_end_time().time
        return Time(end_time)

    def as_no_fly_zones(self):
        no_fly_zones = []
        for plan in self.drone_plans:
            no_fly_zones.append(plan.as_no_fly_zone())
        return no_fly_zones