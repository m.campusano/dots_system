from dots.core.type import Time
import datetime
import math

class PathPoint:
    def __init__(self, lat, lon, action, time=None):
        self.latitude = lat
        self.longitude = lon
        self.depart_to_here_time = time
        self.flying_duration_from_previous_point = datetime.timedelta(seconds = 0)
        self.action = action

    def __str__(self):
        return "PathPoint(lat:%s, lon:%s)" % (self.latitude, self.longitude)

    def __repr__(self):
        return self.__str__()

    # Only 2D distance
    def distance(self, other_point):
        return math.hypot(self.latitude - other_point.latitude, self.longitude - other_point.longitude)