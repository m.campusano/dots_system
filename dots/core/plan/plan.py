from abc import ABC, abstractmethod

class Plan(ABC):

    @abstractmethod
    def get_start_time(self):
        pass

    @abstractmethod
    def get_end_time(self):
        pass