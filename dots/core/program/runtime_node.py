from abc import ABC, abstractmethod

class RuntimeAbstractNode(ABC):

    def __init__(self, ast_node):
        self.ast_node = ast_node
    
    @abstractmethod
    def executing(self):
        pass

    @abstractmethod
    def finished(self):
        pass

    @abstractmethod
    def waiting(self):
        pass

class RuntimeNodeWithChildren(RuntimeAbstractNode):

    def __init__(self, ast_node):
        super().__init__(ast_node)
        self.nodes = []

    def waiting(self):
        for node in self.nodes:
            if not node.waiting():
                return False
        return True

    def executing(self):
        # if some node is executing or there are completed and waiting nodes
        finished = False
        waiting = False
        for node in self.nodes:
            if node.executing():
                return True

            if node.finished():
                finished = True
            if node.waiting():
                waiting = True
            if finished and waiting:
                return True

        return False

    def finished(self):
        for node in self.nodes:
            if not node.finished():
                return False
        return True

    def executing_nodes(self):
        nodes = []
        for node in self.nodes:
            if node.executing():
                nodes.append(node)
        return nodes

    def waiting_nodes(self):
        nodes = []
        for node in self.nodes:
            if node.waiting():
                nodes.append(node)
        return nodes

class RuntimeMission(RuntimeNodeWithChildren):

    @property
    def node(self):
       return self.nodes[0]
    
    @node.setter
    def node(self, node):
       self.nodes = [node]

class RuntimeCoordination(RuntimeNodeWithChildren):
    pass

class RuntimeConstraint(RuntimeNodeWithChildren):
    pass

class RuntimeActionCall(RuntimeAbstractNode):
    
    def __init__(self, ast_node):
        super().__init__(ast_node)
        self.drones = []