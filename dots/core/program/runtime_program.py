from dots.ast import VisitorMission, ActionCall, Mission, Coordination, Geofence, NoFlyZone, StartTime, EndTime
from .runtime_node import RuntimeMission, RuntimeCoordination, RuntimeConstraint
from .runtime_straight_action import RuntimeStraightAction
from .runtime_patrol_action import RuntimePatrolAction

class RuntimeProgram(VisitorMission):

    def __init__(self):
        self.mission = None
        self._canceled = False

        # refactoring #
        self.drones = {}
        self.drones_position = {}
        self.arguments = None
        ###############

        self.__ast_dictionary = {}

    def get_node_from_ast(self, ast_node):
        return self.__ast_dictionary[ast_node]

    ## Execution
    def executing(self):
        return not self.canceled() and self.mission.executing()

    def finished(self):
        return self.mission.finished()

    def waiting(self):
        return self.mission.waiting()

    def canceled(self):
        return self._canceled

    def cancel(self):
        self._canceled = True

    def drone_position(self, drone_id, latitude, longitude):
        # Refactoring chance
        if not drone_id in self.drones:
            return

        self.drones_position[drone_id] = (latitude, longitude)

        action_list = self.drones[drone_id]
        non_finished_action = None
        for action in action_list:
            if not action.finished():
                non_finished_action = action
                break
        if non_finished_action is not None:
            non_finished_action.drone_position(latitude, longitude)
        

    ## Building
    def build_tree(self, operation):
        operation.mission.accept(self)

    def build_drone_runtime(self, operation_plan):
        for drone_plan in operation_plan.drone_plans:
            action_list = []
            
            last_ast_action = None
            runtime_action = None
            for atomic_action in drone_plan.path:
                if last_ast_action != atomic_action.action.ast_element:
                    last_ast_action = atomic_action.action.ast_element
                    runtime_action = self.__ast_dictionary[last_ast_action]
                    action_list.append(runtime_action)
                    runtime_action.drones = [drone_plan.drone]

                runtime_action.waypoints.append(atomic_action)
            self.drones[drone_plan.drone.uuid] = action_list

    def visit_action_call(self, element: ActionCall):
        if element.action_name.lower() == 'straight':
            runtime_action = RuntimeStraightAction(element)
            self.__ast_dictionary[element] = runtime_action
            return runtime_action
        elif element.action_name.lower() == 'patrol':
            runtime_action = RuntimePatrolAction(element, self.arguments[element.arguments[0].reference_name])
            self.__ast_dictionary[element] = runtime_action
            return runtime_action


    def visit_mission(self, element: Mission):
        self.mission = RuntimeMission(element)
        self.mission.node = element.node.accept(self)


    def visit_coordination(self, element: Coordination):
        return self.__visit_element_with_children(RuntimeCoordination, element)


    def visit_geofence_constraint(self, element: Geofence):
        return self.__visit_element_with_children(RuntimeConstraint, element)

    def visit_no_fly_zone_constraint(self, element: NoFlyZone):
        return self.__visit_element_with_children(RuntimeConstraint, element)

    def visit_start_time_constraint(self, element: StartTime):
        return self.__visit_element_with_children(RuntimeConstraint, element)

    def visit_end_time_constraint(self, element: EndTime):
        return self.__visit_element_with_children(RuntimeConstraint, element)

    def __visit_element_with_children(self, cls, element):
        runtime = cls(element)
        for node in element.nodes:
            runtime.nodes.append(node.accept(self))
        self.__ast_dictionary[element] = runtime
        return runtime

    @classmethod
    def from_program(cls, program):
        runtime = cls()
        runtime.arguments = program.arguments
        runtime.build_tree(program.operation)
        runtime.build_drone_runtime(program.operation_plan)
        return runtime