import copy

from geopy.distance import geodesic

from .runtime_node import RuntimeActionCall

class RuntimePatrolAction(RuntimeActionCall):
    
    NEIGHBORHOOD = 100 # in meters

    def __init__(self, ast_node, patrol_area):
        super().__init__(ast_node)
        self.waypoints = []
        self.reached_index = -1
        self.patrol_area = copy.deepcopy(patrol_area)
        self.patrol_area.add_point(patrol_area[0])
        self.reached_patrol_index = -1 
        self.started = False

    def waiting(self):
        return not self.started

    def executing(self):
        return self.started and self.reached_index + 1 < len(self.waypoints)

    def finished(self):
        return self.reached_index >= len(self.waypoints) - 1

    def drone_position(self, latitude, longitude):
        self.started = True

        next_path_point = self.waypoints[self.reached_index + 1]
        
        drone_point = (latitude, longitude)
        next_point = (next_path_point.latitude, next_path_point.longitude)
        if geodesic(drone_point, next_point).meters < self.NEIGHBORHOOD:
            self.reached_index += 1
            next_patrol_point = self.patrol_area.boundingBox[self.reached_patrol_index+1]
            if next_path_point.latitude == next_patrol_point.latitude and \
                next_path_point.longitude == next_patrol_point.longitude:

                self.reached_patrol_index += 1