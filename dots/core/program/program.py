import datetime
import copy

from dots.core.type import Drone, NavigationPoint, Area, Time
from .runtime_program import RuntimeProgram
from .syntax_sugar import SyntaxSugar

class Program:
    count = 0

    def __init__(self, operation, arguments_dict):
        self.operation = operation
        self.original_operation = operation
        self.arguments = {}
        self.__parse_arguments(arguments_dict)
        self.operation_plan = None
        self.old_plans = []
    
        Program.count = Program.count + 1
        self.__id__ = Program.count

        self.runtime = None

    @classmethod
    def create_with_arguments(cls, operation, arguments):
        program = cls(operation, arguments)
        program.apply_syntax_sugar()
        return program

    def has_plan(self):
        return self.operation_plan is not None

    def add_new_plan(self, operation_plan):
        self.old_plans.append(self.operation_plan)
        self.operation_plan = operation_plan
        self.__generate_runtime()

    def get_id(self):
        return self.__id__

    def __eq__(self, other):
        return self.__id__ == other.__id__

    def __parse_arguments(self, arguments_dict):
        for key, value in arguments_dict.items():
            argument_dict = arguments_dict[key]
            parameter = self.operation.parameters[key]
            if parameter.is_array:
                arguments = []
                for value_dict in argument_dict:
                    arguments.append(globals()[parameter.type].from_dictionary(value_dict))
                self.arguments[key] = arguments
            else:
                self.arguments[key] = globals()[parameter.type].from_dictionary(argument_dict)

    def __generate_runtime(self):
        self.runtime = RuntimeProgram.from_program(self)

    ## Execution
    def executing(self):
        return self.runtime.executing()

    def finished(self):
        return self.runtime.finished()

    def waiting(self):
        return self.runtime is None or self.runtime.waiting()

    def canceled(self):
        return self.runtime.canceled()

    def non_canceled(self):
        return self.waiting() or (not self.canceled())

    def cancel(self):
        self.runtime.cancel()

    def has_started(self):
        return self.executing() or self.operation_plan.get_start_time().time > datetime.datetime.now().astimezone()

    def drone_position(self, drone_id, latitude, longitude):
        self.runtime.drone_position(drone_id, latitude, longitude)

    def get_drone(self, drone_id):
        return next(filter(lambda drone: drone.uuid == drone_id, self.get_all_drones()), None)

    def get_all_drones(self):
        drones = []
        for argument in self.arguments.values():
            if isinstance(argument, Drone):
                drones.append(argument)
                continue
            if isinstance(argument, list):
                if isinstance(argument[0], Drone):
                    drones = drones + argument
                    continue

        return drones

    def apply_syntax_sugar(self):
        self.operation = copy.deepcopy(self.original_operation) 
        SyntaxSugar(self.arguments).apply(self.operation)

    def all_no_fly_zones(self):
        references = self.operation.mission.all_no_fly_zone_references()
        no_fly_zones = []
        for reference in references:
            no_fly_zones.append(self.arguments[reference])
        return no_fly_zones