import copy

from dots.ast import VisitorDots, ActionCall, Mission, Coordination, Loop, \
                      Argument, Parameter, ActionDefinition, Operation, \
                      Geofence, StartTime, EndTime, NoFlyZone

class SyntaxSugar(VisitorDots):

    def __init__(self, arguments):
        self.parent = None
        self.arguments = arguments
        self.modified_parameters = {}

    def apply(self, operation):
        operation.accept(self)

    def visit_action_call(self, element: ActionCall):
        if (element.left_argument.reference_name in self.arguments) and \
            (isinstance(self.arguments[element.left_argument.reference_name], list)):
            self.__visit_action_call_array(element)
        else:
            self.__visit_action_call_no_array(element)
        

    def visit_mission(self, element: Mission):
        self.visit_node_with_body(element)

    def visit_coordination(self, element: Coordination):
        parent = self.parent
        self.parent = element
        self.visit_node_with_body(element)
        self.parent = parent

    def visit_loop(self, element: Loop):
        parent = self.parent
        original_modified_parameters = self.modified_parameters
        modified_parameters = copy.deepcopy(original_modified_parameters)

        coordination = None
        if element.is_sequential():
            coordination = Coordination.sequential()
        elif element.is_prioritized():
            coordination = Coordination.prioritized()
        elif element.is_parallel():
            coordination = Coordination.parallel()

        locations = self.arguments[element.location_list_reference_name]
        for index, location in enumerate(locations):
            sequential = Coordination.sequential()
            sequential.drone_reference_name = element.drone_reference_name
            sequential.drone_list_reference_name = element.drone_list_reference_name

            modified_parameters[element.location_reference_name] = f'{element.location_list_reference_name}[{index}]'

            self.parent = element
            self.modified_parameters = modified_parameters
            for node in element.nodes:
                copied_node = copy.deepcopy(node)
                sequential.add_node(copied_node)
                copied_node.accept(self)
            self.parent = parent
            self.modified_parameters = original_modified_parameters

            coordination.add_node(sequential)

        parent.exchange_node(element, coordination)

    def visit_operation(self, element: Operation):
        parent = self.parent
        element.name = f'{element.name}_sugar'

        self.parent = element
        element.mission.accept(self)
        self.parent = parent

    def visit_geofence_constraint(self, element: Geofence):
        self.__visit_element_with_children(element)

    def visit_no_fly_zone_constraint(self, element: NoFlyZone):
        self.__visit_element_with_children(element)

    def visit_start_time_constraint(self, element: StartTime):
        self.__visit_element_with_children(element)

    def visit_end_time_constraint(self, element: EndTime):
        self.__visit_element_with_children(element)

    def __visit_element_with_children(self, element):
        parent = self.parent
        self.parent = element
        self.visit_node_with_body(element)
        self.parent = parent


    def __visit_action_call_no_array(self, element):
        for argument in element.arguments:
            if argument.reference_name in self.modified_parameters:
                argument.reference_name = self.modified_parameters[argument.reference_name]

    def __visit_action_call_array(self, element):
        coordination = Coordination.parallel()
        for index, drone in enumerate(self.arguments[element.left_argument.reference_name]):
            new_action = copy.deepcopy(element)
            for argument in new_action.arguments:
                if argument.reference_name in self.modified_parameters:
                    argument.reference_name = self.modified_parameters[argument.reference_name]
            new_action.left_argument = Argument(f'{element.left_argument.reference_name}[{index}]')
            coordination.add_node(new_action)
        self.parent.exchange_node(element, coordination)