
class NavigationPoint:

    def __init__(self, lat, lon):
        self.latitude = lat
        self.longitude = lon
    
    def __str__(self):
        return "NavigationPoint(lat:%s, lon:%s)" % (self.latitude, self.longitude)

    def __repr__(self):
        return self.__str__()

    @classmethod
    def as_dictionary(cls):
        return {
            'latitude' : 'Integer',
            'longitude' : 'Integer'
        }

    @classmethod
    def from_dictionary(cls, values_dict):
        return cls(values_dict['latitude'], values_dict['longitude'])