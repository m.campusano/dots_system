from .navigation_point import NavigationPoint

class Area:

    def __init__(self, boundingBox = []):
        self.boundingBox = list(boundingBox)

    def add_point(self, navigation_point):
        self.boundingBox.append(navigation_point)

    def __getitem__(self, arg):
        return self.boundingBox[arg]

    @classmethod
    def as_dictionary(cls):
        return {
            'bounding_box' : [NavigationPoint.as_dictionary()]
        }

    @classmethod
    def from_dictionary(cls, values_dict):
        return cls(list(map(lambda values: NavigationPoint.from_dictionary(values), values_dict['boundingBox'])))