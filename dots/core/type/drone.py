from abc import ABC, abstractmethod
from .navigation_point import NavigationPoint

class AbstractDrone(ABC):
    
    def is_undefined(self):
        return True

class UndefinedDrone(AbstractDrone):

    def __init__(self, reference_name, drones):
        self.reference_name = reference_name
        self.drones = drones

class Drone(AbstractDrone):

    def __init__(self, uuid, take_off_position):
        self.uuid = uuid
        self.take_off_position = take_off_position
    
    def is_undefined(self):
        return False

    def __str__(self):
        return "Drone(%s)" % (self.uuid)

    def __repr__(self):
        return self.__str__()
    
    @classmethod
    def as_dictionary(cls):
        return {
            'uuid' : "String",
            "take_off_position" : NavigationPoint.as_dictionary()
        }

    @classmethod
    def from_dictionary(cls, values_dict):
        return cls(values_dict["uuid"], NavigationPoint.from_dictionary(values_dict['takeOffPosition']))