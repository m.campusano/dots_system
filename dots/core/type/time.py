import dateutil.parser

class Time:

    def __init__(self, time):
        self.time = time
    
    def get_serialized_string(self):
        return self.time.isoformat()

    def __str__(self):
        return self.time.__str__()

    def __repr__(self):
        return self.__str__()

    @classmethod
    def from_dictionary(cls, values_dict):
        return cls(dateutil.parser.parse(values_dict['time']))