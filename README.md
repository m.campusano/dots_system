# DOTS System

## Installation

For plain DOTS and testing, we need the following packages: 

    pip3 install inflection pytest

For server node, we need the following packages:

    pip3 install flask flask-restful flask-jsonpify flask-socketio eventlet

To run ROS nodes, you should expose our Python API by modifing the Python path (inside the .bashrc / .zshrc):

    export PYTHONPATH=$PYTHONPATH:<path-to-this-project>

For the planner, we need a modified version of [pyvisgraph](https://gitlab.com/m.campusano/pyvisgraph).

For the dynamic replanner, we need[geopy](https://github.com/geopy/geopy):

    pip3 install geopy

For Unifly connectivity, we need to link our very own [library](https://gitlab.com/healthdrone/uniflylib).

For DAA system, we refer to its own documentation in the DAA ROS package

## Services

At the moment, we provide the following services:

* UTM Publisher
* Web Server
* DOTS Parser
* Planner
* Database service
* DAA for OUH operations

## Planning and dynamic replanning

The DOTS system allows for specification of multi-drone missions in a two step process:

1. Upload the program (using a XML representation)
2. Upload the parameters of the program (using a JSON representation of them)

In the first step, the system stores the program in a node (no persistent storage).
Then, when having the parameters, the system plans the path of each of the drones (that are also specified in the system).
Moreover, the system exposes several URLs to allow for uploading programs and parameters, and for retrieving data of plans and status of missions.

### Installation

To use these services, you should:

* Clone the repository
* Prepare your [ROS system](https://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment)
* Prepare the steps from the previous section, except for Unifly and DAA system.
* Link the folders in the ROS Workspace (using symlinks)
    * dots_shared
    * dots_system
    * dots_web_server
* Run the system `roslaunch dots_web_server server.launch`
    
### Usage

Steps to use the system after it runs:

1. Upload a program in the following url: localhost:8080/template
    * You can use the program in dots/example/xml/gpce.xml
    * With curl: `curl -X POST localhost:8080/template -d@gpce.xml `
2. Upload the parameters in the following url: localhost:8080/template/\<name_of_mission\>
    * If you use our previous example, the mission is called *visit_nursing_homes*
    * You can use the parameters in dots/example/xml/gpce.json
    * With curl: `curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" localhost:8080/template/visit_nursing_homes -d@gpce.json`
3. You can retrieve the path of every drone by calling the following url with a get: localhost:8080/template/\<name_of_mission\>
    * **This is not tested**


## Notes

### ROS-DOTS Design

- ROS is used as a middleware for this architecture:
    + Multiple programming languages
    + Familiarity of the robot/drone community
    + The familiarity of the community could help develop new services (or improve existing ones)

- Services register important data to a database:
    + Parsed Template, DOTS Programs and Flight paths
    + No-fly-zones

### Planner

- A planner takes a program and return a plans for each drone.
    + The program is first converted to a list of *actions*.
- A plan contains a path.
- A path is a list of rich waypoints, i.e., not only the waypoint, but more information necessary for the execution of the operation (e.g., the time when to go to the next waypoint).
- The planner service is in charge of the re-planning too
- It is a static planner:
    + It generates plans, but it does not monitor if the plan is being followed or not, this is a job for the operator
        * There is no complete autonomy of drones
        * Drones are assigned to a list of waypoints to follow, the operator is in charge of starting the operation at the given time
    + For coordination mechanisms, the operator should use  the start time for the different waypoints
        * When we have 2 actions running sequentially, action A *time to finish* is the same *time to start* of action B (i.e., planner simulates sequential execution by coordinating the time of the actions)
        * When we have 2 actions running in parallel, action A *time to start* is the same *time to start* of action B
    + When we have true autonomy of drones (i.e., no operator per drone, but maybe 1 operator for several drones) we can implement a dynamic planner
        * A dynamic planer would not simulate coordination mechanisms, but it would truly implement them by monitoring every action and controlling drones directly

### Constraints

* General:
    - An action is associated to all its constraints
    - It depends on the planner on how to follow them
    - They should *make sense*
        + The DSL does not check if the constraints make sense or not
        + Start time always before end time
        + It should make a feasible mission
            * Enough time for the action given start and end time
            * Geofence and no fly zones should allow flying (i.e., creating geographic constraint may result on impossible flight paths)
        + It should be more restrictive for certain cases
            * Inner start time should be later that outer starting times
* Default planner:
    - The default planner does not check if the constraints make sense or not
    - If an action has several geofences, the overall geofence is the union of all geofences
        + This can be problematic if we want to have a more specific geofence. Maybe for disjointed geofences we go with the union and for jointed geofences we go with the intersection
    - If an action has several no fly zones, the overall no fly zone is the union of them all
        + In this case, no-fly-zones should only *grow*, no fly zones are inherently restrictive areas. We do not have the same problem with geofences
    - It does not use *end_time*
        + Because constraints should *make sense*, the default planner follows an eager approach, the only important time in this case is the *start time*. Nevertheless, other planner may use the *end_time* as their wish
    - This planner always assume there is a feasible plan

### Drone Plans

* For MVP: a drone is considered to have only one active plan at a time

### UTM Connection

- The Unifly UTM node manages the published programs and flight paths with their Unifly operation_id.

## Limitations


### ROS

The focus of ROS is to develop robots using a SOA style.
While this is enough, it has a lot of other features that are not necessary in a project like ROS-DOTS.
Besides, it may fall short in:

* Scalability of services: stateless services have the potential to scale with a load balancer and multiple instances of the service (hence the stateless part). ROS Nodes cannot scale by having multiple instances, nor there is a load balancer (it seems like a difficult thing to do).
* Service monitoring: monitoring the availability of services
* Multiple-nodes services: some services need many nodes to work (like the DAA service). These multi-node services also need to be monitored as a whole.


Alternatives to ROS:

* [Nameko](https://nameko.readthedocs.io/en/stable)

### Variable Shadowing

Now the system does not support variable shadowing.
It is crucial to notice that variable shadowing should be supported in the dots language, in the planner (which Drone and/or Navigation Point it should use) and the slicer (which Drone and/or Navigation Point is being used). 

## Versions

## 0.10

GPCE Iteration: dynamic replanning with program slicing.

### Important for next version


## TODOs

Take into consideration that any *idea* of implementation explained in this section is only a quick thought on how to implement the problem.
It is not a guide, but just quick notes that may be useful in the future.

* Error handling:
    - At the moment, there is no error handling. 
        + Any error would be seen as 500 internal error without any meaningful meaning. This should be changed
* AST:
    - The Parameter contains a reference to the type of its value, which is a String
        + It should be the Python Class type
* Refactoring:
    - Conflict Detector
        + maybe this should be provided by its own service
* Security issues:
    - We are using pickle to send and receive python objects in ros msgs. This is completely unsafe and must be revisited.
* Action plug-in:
    - The plug-in for actions is disable for the moment.
    - Ideas to enable it again:
        + List of actions, that are passed to the action builder as parameters (could be a list of classes defined one class method that init the action)
        + The planner decide a list of supported actions, this means, actions depend on the planner
* Support for different planners:
    - Idea:
        + Planners should extend from an abstract class `AbstractPlan` and redefine the methods `plan` and `re-plan`
        + For each action, there is an *executor* in charge of deciding how to plan a simple action, without taking into considerations the other actions
        + With this method a planner may use or redefine any executor or even redefine the whole way of doing the plan.
* Planner with multiple drone and operations:
    - At the moment, the planner does not support multiple operations that use the same drone. It is important to take this when creating new flight plans
* Server:
    - When a new service is added, the server must change. Probably a good way of handling this is to define some kind of *global* list of [Blueprint](https://flask.palletsprojects.com/en/1.1.x/tutorial/views/), and add blueprints for different services
* Plan: 
    - To allow more drones to perform other commands in a plan, the representation of the plan of a drone needs to change from 'list of rich waypoints' to 'list of commands' (e.g., takeOff, move, land, pickup, finish, etc).
    - In ros-dots, a drone only has one planned plan at a time (one plan in the future). This must change, a drone may have only one executing plan, but any numbers of plans planned into the future.
* Multiple drones:
    - Now, any action that is defined over an array of drone, it is converted (using syntax sugar) to a parallel execution over each drone. While this seems reasonable for the Straight action, it may not be reasonable for other actions, for example, Patrol an area using a collaboration between drones.
* Database:
    - DOTS Template names should be unique in the system, but uniqueness is not being checked right now.
    - World state is published via DB and it is only composed by no fly zones
        + this should probably not be the DB responsibility, but because there is no more interesting things to report, it is like this
* Multi Program:
    - Planner does not takes into consideration if drones are being used in another plan
    - dynamic replanning only work with one program, this should change and support:
        + multiple running programs
        + dynamic replanning of all running programs