#!/bin/bash

# setup python environment
export PYTHONPATH=$PYTHONPATH:/dots
export PYTHONPATH=$PYTHONPATH:/pyvisgraph

# setup ROS environment
source "/opt/ros/$ROS_DISTRO/setup.bash"
source "/dots_ws/devel/setup.bash"



roslaunch dots_web_server server.launch