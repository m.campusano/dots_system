from dots.ast_parser import DotsXML
from dots.ast import Geofence, NoFlyZone, StartTime, EndTime

class TestXMLParser:


	def testParseOperation(self):
		xml = """
			<operation name="opTest">
			</operation>
			"""

		operation = DotsXML().parse(xml)
		assert operation.name == "opTest"

	def testParseParameter(self):
		xml = """
			<operation name="opTest">
				<parameter type="Drone" array="false" name="uav" />
				<parameter type="Drone" array="true" name="uavs" />
				<parameter type="NavigationPoint" array="false" name="point" />
				<parameter type="NavigationPoint" array="true" name="points" />
				<parameter type="Area" array="false" name="zone" />
				<parameter type="Area" array="true" name="zones" />
			</operation>
			"""

		operation = DotsXML().parse(xml)

		assert len(operation.parameters) == 6

		assert operation.parameters["uav"].type == "Drone"
		assert not operation.parameters["uav"].is_array
		assert operation.parameters["uav"].name == "uav"

		assert operation.parameters["uavs"].type == "Drone"
		assert operation.parameters["uavs"].is_array
		assert operation.parameters["uavs"].name == "uavs"

		assert operation.parameters["point"].type == "NavigationPoint"
		assert not operation.parameters["point"].is_array
		assert operation.parameters["point"].name == "point"

		assert operation.parameters["points"].type == "NavigationPoint"
		assert operation.parameters["points"].is_array
		assert operation.parameters["points"].name == "points"

		assert operation.parameters["zone"].type == "Area"
		assert not operation.parameters["zone"].is_array
		assert operation.parameters["zone"].name == "zone"

		assert operation.parameters["zones"].type == "Area"
		assert operation.parameters["zones"].is_array
		assert operation.parameters["zones"].name == "zones"

	def testParseMission(self):
		xml = """
			<operation name="opTest">
				<mission>
				</mission>
			</operation>
			"""
		operation = DotsXML().parse(xml)

		assert operation.mission

	def testParseActionDefinition(self):
		xml ="""
			<operation name="opTest">
				<action-definition name="noParameters">
				</action-definition>
				<action-definition name="parameters">
					<parameter type="Location" array="true" />
				</action-definition>
			</operation>
			"""
		operation = DotsXML().parse(xml)

		assert len(operation.action_definitions) == 2

		assert operation.action_definitions[0].name == "noParameters"
		assert not operation.action_definitions[0].has_parameters()

		assert operation.action_definitions[1].name == "parameters"
		assert operation.action_definitions[1].has_parameters()

		parameters = operation.action_definitions[1].parameters
		assert len(parameters) == 1
		assert parameters[0].type == "Location"
		assert parameters[0].is_array


	def testParseActionExecution(self):
		xml = """
			<operation name="opTest">
				<action-definition name="noParameters">
				</action-definition>
				<action-definition name="parameters">
					<parameter type="Location" array="true" />
				</action-definition>
				<parameter type="Drone" array="false" name="uav" />
				<parameter type="NavigationPoint" array="false" name="pointA" />
				<parameter type="NavigationPoint" array="false" name="pointB" />
				<mission>
					<sequential>
						<action name="noParameters">
							<left reference="uav" />
						</action>
						<action name="parameters">
							<left reference="uav" />
							<argument reference="pointA" />
							<argument reference="pointB" />
						</action>
					</sequential>
				</mission>
			</operation>
			"""
		operation = DotsXML().parse(xml)
		sequence = operation.mission.node
		assert len(sequence.nodes) == 2

		call1 = sequence.nodes[0]
		call2 = sequence.nodes[1]

		assert call1.action_name == "noParameters"
		assert not call1.has_arguments()
		assert call1.left_argument.reference_name == "uav"

		assert call2.action_name == "parameters"
		assert call2.has_arguments()
		assert call2.left_argument.reference_name == "uav"
		assert len(call2.arguments) == 2
		assert call2.arguments[0].reference_name == "pointA"
		assert call2.arguments[1].reference_name == "pointB"

	def testParseCoordinationTypes(self):
		xml = """
			<operation name="opTest">
				<mission>
					<sequential>
						<parallel>
						</parallel>
						<prioritized>
						</prioritized>
						<sequential>
						</sequential>
					</sequential>
				</mission>
			</operation>
			"""

		operation = DotsXML().parse(xml)
		sequence = operation.mission.node
		assert len(sequence.nodes) == 3

		parallel = sequence.nodes[0]
		prioritized = sequence.nodes[1]
		sequential = sequence.nodes[2]

		assert parallel.is_parallel()
		assert not parallel.is_prioritized()
		assert not parallel.is_sequential()
		assert not parallel.is_using_drones()

		assert not prioritized.is_parallel()
		assert prioritized.is_prioritized()
		assert not prioritized.is_sequential()
		assert not prioritized.is_using_drones()

		assert not sequential.is_parallel()
		assert not sequential.is_prioritized()
		assert sequential.is_sequential()
		assert not sequential.is_using_drones()

	def testParseCoordination(self):
		xml = """
			<operation name="opTest">
				<action-definition name="straight">
					<parameter type="Location" array="true" />
				</action-definition>
				<parameter type="Drone" array="false" name="uav1" />
				<parameter type="Drone" array="false" name="uav2" />
				<parameter type="Area" array="false" name="field" />
				<mission>
					<sequential>
						<parallel>
							<action name="straight">
								<left reference="uav1" />
								<argument reference="field" />
							</action>
							<action name="straight">
								<left reference="uav2" />
								<argument reference="field" />
							</action>
						</parallel>
					</sequential>
				</mission>
			</operation>
			"""

		operation = DotsXML().parse(xml)
		sequence = operation.mission.node
		parallel = sequence.nodes[0]

		assert parallel.is_parallel()
		assert len(parallel.nodes) == 2

		call1 = parallel.nodes[0]
		call2 = parallel.nodes[1]

		assert call1.action_name == "straight"
		assert call1.has_arguments()
		assert call1.left_argument.reference_name == "uav1"
		assert call1.arguments[0].reference_name == "field"

		assert call2.action_name == "straight"
		assert call2.has_arguments()
		assert call2.left_argument.reference_name == "uav2"
		assert call2.arguments[0].reference_name == "field"
	
	def testParserCoordinationUsingDrones(self):
		xml = """
			<operation name="opTest">
				<parameter type="Drone" array="true" name="uavs" />
				<mission>
					<sequential>
						<parallel drone="uav" drone-list="uavs">
						</parallel>
					</sequential>
				</mission>
			</operation>
			"""
		operation = DotsXML().parse(xml)
		sequence = operation.mission.node
		parallel = sequence.nodes[0]
		assert parallel.is_using_drones()
		assert parallel.drone_list_reference_name == "uavs"
		assert parallel.drone_reference_name == "uav"

	def testParseConstraints(self):
		xml = """
			<operation name="opTest">
				<parameter type="Area" array="false" name="geofence" />
				<parameter type="Area" array="false" name="noFlyZone" />
				<parameter type="Time" array="false" name="beforeTime" />
				<parameter type="Time" array="false" name="afterTime" />
				<mission>
					<sequential>
						<constraint type="Geofence" variable="geofence">
						</constraint>
						<constraint type="NoFlyZone" variable="noFlyZone">
						</constraint>
						<constraint type="EndTime" variable="beforeTime">
						</constraint>
						<constraint type="StartTime" variable="afterTime">
						</constraint>
					</sequential>
				</mission>
			</operation>
			"""
		operation = DotsXML().parse(xml)
		sequence = operation.mission.node

		assert len(sequence.nodes) == 4

		geofence = sequence.nodes[0]
		noFlyZone = sequence.nodes[1]
		endTime = sequence.nodes[2]
		startTime = sequence.nodes[3]

		assert type(geofence) is Geofence
		assert type(noFlyZone) is NoFlyZone
		assert type(endTime) is EndTime
		assert type(startTime) is StartTime

		assert geofence.area_reference_name == "geofence"
		assert noFlyZone.area_reference_name == "noFlyZone"
		assert endTime.time_reference_name == "beforeTime"
		assert startTime.time_reference_name == "afterTime"

	def testParseConstraintBody(self):
		xml = """
			<operation name="opTest">
				<action-definition name="straight">
					<parameter type="Location" array="true" />
				</action-definition>
				<parameter type="Area" array="false" name="geofence" />
				<parameter type="Drone" array="false" name="uav" />
				<parameter type="NavigationPoint" array="true" name="points" />
				<mission>
					<sequential>
						<constraint type="Geofence" variable="geofence">
							<action name="straight">
								<left reference="uav" />
								<argument reference="points" />
							</action>
						</constraint>
					</sequential>
				</mission>
			</operation>
			"""

		operation = DotsXML().parse(xml)
		geofence = operation.mission.node.nodes[0]

		call = geofence.nodes[0]

		assert call.action_name == "straight"
		assert call.has_arguments()
		assert call.left_argument.reference_name == "uav"
		assert len(call.arguments) == 1

		assert call.arguments[0].reference_name == "points"

	def testParseLoops(self):
		xml = """
			<operation name="loop">
				<action-definition name="Straight">
					<parameter type="Location" array="false" />
				</action-definition>
				<parameter type="NavigationPoint" array="true" name="points" />
				<parameter type="Drone" array="true" name="uavs" />
				<mission>
					<sequential>
						<for type="parallel" location="point" location-list="points" drone="uav" drone-list="uavs">
							<action name="Straight">
								<left reference="uav" />
								<argument reference="point" />
							</action>
						</for>
						<for type="prioritized" location="point" location-list="points" drone="uav" drone-list="uavs">
							<action name="Straight">
								<left reference="uav" />
								<argument reference="point" />
							</action>
						</for>
						<for type="sequential" location="point" location-list="points" drone="uav" drone-list="uavs">
							<action name="Straight">
								<left reference="uav" />
								<argument reference="point" />
							</action>
						</for>
					</sequential>
				</mission>
			</operation>
			"""

		operation = DotsXML().parse(xml)
		do = operation.mission.node
		parallel = do.nodes[0]
		prioritized = do.nodes[1]
		sequential = do.nodes[2]

		assert parallel.is_parallel()
		assert not parallel.is_prioritized()
		assert not parallel.is_sequential()
		assert parallel.is_using_drones()
		assert parallel.drone_list_reference_name == "uavs"
		assert parallel.drone_reference_name == "uav"
		assert parallel.location_list_reference_name == "points"
		assert parallel.location_reference_name == "point"

		assert not prioritized.is_parallel()
		assert prioritized.is_prioritized()
		assert not prioritized.is_sequential()
		assert prioritized.is_using_drones()
		assert prioritized.drone_list_reference_name == "uavs"
		assert prioritized.drone_reference_name == "uav"
		assert prioritized.location_list_reference_name == "points"
		assert prioritized.location_reference_name == "point"


		assert not sequential.is_parallel()
		assert not sequential.is_prioritized()
		assert sequential.is_sequential()
		assert sequential.is_using_drones()
		assert sequential.drone_list_reference_name == "uavs"
		assert sequential.drone_reference_name == "uav"
		assert sequential.location_list_reference_name == "points"
		assert sequential.location_reference_name == "point"