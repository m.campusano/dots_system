from dots.ast import Operation
from dots.core.program import Program
from dots.core.type import Drone, NavigationPoint, Time, Area

from dots.ast_parser import DotsXML

import json
import dateutil.parser

class TestProgram:

    def testProgram(self):
       operation = Operation("opTest")
       program = Program(operation, {})

       assert program.operation == operation


    def testOperationArguments(self):
       xml = """
          <operation name="opTest">
              <parameter type="Drone" array="false" name="uav" />
              <parameter type="NavigationPoint" array="false" name="point" />
              <parameter type="Area" array="false" name="zone" />
              <parameter type="Time" array="false" name="start" />
          </operation>
         """
       arguments = """
         {
           "uav":
          {
               "uuid": "uav",
               "takeOffPosition": {
                 "latitude": 0,
                 "longitude": 0
               }
             },
           "point" :
          {
               "latitude": 0,
               "longitude": 0
             }, 
           "zone": {
             "boundingBox": [
               {
                 "latitude": 0,
                 "longitude": 0
               }
             ]
           },
           "start": {
             "time": "2020-12-25T12:15+01:00"
           }
         }
         """

       operation = DotsXML().parse(xml)
       arguments = json.loads(arguments)

       program = Program(operation, arguments)

       uav = program.arguments['uav']
       point = program.arguments['point']
       zone = program.arguments['zone']
       start = program.arguments['start']

       assert type(uav) is Drone
       assert uav.uuid == 'uav'
       assert type(uav.take_off_position) is NavigationPoint
       assert uav.take_off_position.latitude == 0
       assert uav.take_off_position.longitude == 0

       assert type(point) is NavigationPoint
       assert point.latitude == 0
       assert point.longitude == 0

       assert type(zone) is Area
       assert len(zone.boundingBox) == 1
       assert type(zone.boundingBox[0]) is NavigationPoint
       assert zone.boundingBox[0].latitude == 0
       assert zone.boundingBox[0].longitude == 0

       assert type(start) is Time
       assert start.time == dateutil.parser.parse("2020-12-25T12:15+01:00")


    def testOperationArgumentsArray(self):

        xml = """
              <operation name="opTest">
                  <parameter type="Drone" array="true" name="uavs" />
                  <parameter type="NavigationPoint" array="true" name="points" />
                  <parameter type="Area" array="true" name="zones" />
              </operation>
            """
        arguments = """
            {
              "uavs": [
                {
                  "uuid": "uavs1",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                }
              ],
              "points": [
                {
                  "latitude": 0,
                  "longitude": 0
                }
              ],
              "zones":[ {
                "boundingBox": [
                  {
                    "latitude": 0,
                    "longitude": 0
                  }
                ]
              }]
            }
            """

        operation = DotsXML().parse(xml)
        arguments = json.loads(arguments)

        program = Program(operation, arguments)

        uavs = program.arguments['uavs']
        points = program.arguments['points']
        zones = program.arguments['zones']


        assert type(uavs) is list
        assert len(uavs) == 1
        uav = uavs[0]
        assert type(uav) is Drone
        assert uav.uuid == 'uavs1'
        assert type(uav.take_off_position) is NavigationPoint
        assert uav.take_off_position.latitude == 0
        assert uav.take_off_position.longitude == 0

        assert type(points) is list
        assert len(points) == 1
        point = points[0]
        assert type(point) is NavigationPoint
        assert point.latitude == 0
        assert point.longitude == 0

        assert type(zones) is list
        assert len(zones) == 1
        zone = zones[0]
        assert type(zone) is Area
        assert len(zone.boundingBox) == 1
        assert type(zone.boundingBox[0]) is NavigationPoint
        assert zone.boundingBox[0].latitude == 0
        assert zone.boundingBox[0].longitude == 0

    def testNoFlyZones(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Area" array="false" name="noFlyZone1" />
                <parameter type="Area" array="false" name="noFlyZone2" />
                <parameter type="Area" array="false" name="noFlyZone3" />
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <constraint type="NoFlyZone" variable="noFlyZone1">
                            <constraint type="NoFlyZone" variable="noFlyZone2">
                            </constraint>
                        </constraint>
                        <constraint type="NoFlyZone" variable="noFlyZone3">
                            <action name="straight">
                                <left reference="uav" />
                                <argument reference="point" />
                            </action>
                        </constraint>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "noFlyZone1": {
                    "boundingBox": [
                      {
                        "latitude": 0,
                        "longitude": 0
                      },
                      {
                        "latitude": 0,
                        "longitude": 1
                      },
                      {
                        "latitude": 1,
                        "longitude": 1
                      },
                      {
                        "latitude": 1,
                        "longitude": 0
                      }
                    ]
                },
                "noFlyZone2": {
                    "boundingBox": [
                      {
                        "latitude": 0,
                        "longitude": 0
                      },
                      {
                        "latitude": 0,
                        "longitude": 10
                      },
                      {
                        "latitude": 10,
                        "longitude": 10
                      },
                      {
                        "latitude": 10,
                        "longitude": 0
                      }
                    ]
                },
                "noFlyZone3": {
                    "boundingBox": [
                      {
                        "latitude": 0,
                        "longitude": 0
                      },
                      {
                        "latitude": 0,
                        "longitude": 100
                      },
                      {
                        "latitude": 100,
                        "longitude": 100
                      },
                      {
                        "latitude": 100,
                        "longitude": 0
                      }
                    ]
                }
            }
            """

        operation = DotsXML().parse(xml)
        arguments = json.loads(arguments)

        program = Program(operation, arguments)

        zone1 = program.arguments['noFlyZone1']
        zone2 = program.arguments['noFlyZone2']
        zone3 = program.arguments['noFlyZone3']

        no_fly_zones = program.all_no_fly_zones()

        assert zone1 in no_fly_zones
        assert zone2 in no_fly_zones
        assert zone3 in no_fly_zones