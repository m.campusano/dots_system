from dots.ast import Operation, ActionDefinition, \
                    Parameter, Mission, Coordination, \
                    StartTime, EndTime, Geofence, NoFlyZone, \
                    ActionCall, Argument
from dots.ast_parser import DotsXML

import textwrap
import pytest
import os
import pathlib

class TestAST:

    @pytest.fixture
    def operation(self):
        xml = ''
        with open(os.path.join(pathlib.Path(__file__).parent.absolute(), 'dots_programs', 'all_statements.xml'), 'r') as file:
            xml = file.read()
        return DotsXML().parse(xml)
    
    @pytest.fixture
    def template(self):
        template = ''
        with open(os.path.join(pathlib.Path(__file__).parent.absolute(), 'dots_programs', 'all_statements.dots'), 'r') as file:
            template = file.read()
        return template
    
    def testOperation(self):
        operation = Operation('test')
        dots = operation.to_dots_string()
        assert dots == 'operation test:'

    def testParameterNoNameNoArray(self):
        parameter = Parameter()
        parameter.type = 'Location'
        dots = parameter.to_dots_string()
        assert dots == 'location'

    def testParameterNoName(self):
        parameter = Parameter()
        parameter.type = 'Location'
        parameter.is_array = True
        dots = parameter.to_dots_string()
        assert dots == 'location[]'

    def testParameterNoArray(self):
        parameter = Parameter()
        parameter.name = 'geofence'
        parameter.type = 'Area'
        dots = parameter.to_dots_string()
        assert dots == 'geofence : area'

    def testParameter(self):
        parameter = Parameter()
        parameter.name = 'geofence'
        parameter.type = 'Area'
        parameter.is_array = True
        dots = parameter.to_dots_string()
        assert dots == 'geofence : area[]'
    
    def testActionDefinitionNoParams(self):
        action = ActionDefinition('Straight')
        dots = action.to_dots_string() 
        assert dots == 'import action Straight'

    def testActionDefinition(self):
        action = ActionDefinition('Straight')
        parameter1 = Parameter()
        parameter1.type = 'Drone'
        action.add_parameter(parameter1)
        parameter2 = Parameter()
        parameter2.type = 'Location'
        parameter2.is_array = True
        action.add_parameter(parameter2)

        dots = action.to_dots_string() 
        assert dots == 'import action Straight<drone,location[]>'
        

    def testMission(self):
        mission = Mission()
        dots = mission.to_dots_string()
        assert dots == 'implementation:'

    def testSequential(self):
        dots = Coordination.sequential().to_dots_string()
        assert dots == textwrap.dedent('''\
                        sequential
                        end''')

    def testParallel(self):
        dots = Coordination.parallel().to_dots_string()
        assert dots == textwrap.dedent('''\
                        parallel
                        end''')

    def testPrioritized(self):
        dots = Coordination.prioritized().to_dots_string()
        assert dots == textwrap.dedent('''\
                        prioritized
                        end''')

    def testConstraintStart(self):
        dots = StartTime('start_time').to_dots_string()
        assert dots == textwrap.dedent('''\
                        constraint after start_time
                        end''')

    def testConstraintEnd(self):
        dots = EndTime('end_time').to_dots_string()
        assert dots == textwrap.dedent('''\
                        constraint before end_time
                        end''')

    def testConstraintGeofence(self):
        dots = Geofence('geofence').to_dots_string()
        assert dots == textwrap.dedent('''\
                        constraint inside geofence
                        end''')

    def testConstraintNoFlyZone(self):
        dots = NoFlyZone('no_fly_zone').to_dots_string()
        assert dots == textwrap.dedent('''\
                        constraint outside no_fly_zone
                        end''')

    def testArgument(self):
        dots = Argument('uav').to_dots_string()
        assert dots == 'uav'

    def testActionCallNoArgument(self):
        action = ActionCall('Straight')
        action.left_argument = Argument('uav')
        dots = action.to_dots_string()
        assert dots == 'uav += Straight()'

    def testActionCall(self):
        action = ActionCall('Straight')
        action.left_argument = Argument('uav')
        action.add_argument(Argument('location1'))
        action.add_argument(Argument('location2'))
        dots = action.to_dots_string()
        assert dots == 'uav += Straight(location1,location2)'

    def testFullProgram(self, operation, template):
        assert operation.to_dots_string() == template