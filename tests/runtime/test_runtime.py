from dots.ast_parser import DotsXML
from dots.core.program import Program
from dots.planner import DefaultPlanner
import os
import pathlib
import json
import pytest

class TestRuntime:

    @pytest.fixture
    def program(self):
        xml = ''
        with open(os.path.join(pathlib.Path(__file__).parent.parent.absolute(), 'dots_programs', 'base_program.xml'), 'r') as file:
            xml = file.read()
        arguments = ''
        with open(os.path.join(pathlib.Path(__file__).parent.parent.absolute(), 'dots_programs', 'base_program.json'), 'r') as file:
            arguments = file.read()
        operation = DotsXML().parse(xml)
        arguments = json.loads(arguments)
        program = Program(operation, arguments)
        DefaultPlanner().plan(program)
        return program

    @pytest.fixture
    def full_program(self):
        xml = ''
        with open(os.path.join(pathlib.Path(__file__).parent.parent.absolute(), 'dots_programs', 'all_statements.xml'), 'r') as file:
            xml = file.read()
        arguments = ''
        with open(os.path.join(pathlib.Path(__file__).parent.parent.absolute(), 'dots_programs', 'all_statements.json'), 'r') as file:
            arguments = file.read()
        operation = DotsXML().parse(xml)
        arguments = json.loads(arguments)
        program = Program(operation, arguments)
        DefaultPlanner().plan(program)
        return program

    def testExecuteWithDrone(self, program):
        drone_id = 'uav'

        assert program.waiting()
        program.drone_position(drone_id, 0.0, 0.0)
        assert program.executing()

    def testExecuteWrongDrone(self, program):
        drone_id = 'uav2'

        assert program.waiting()
        program.drone_position(drone_id, 0.0, 0.0)
        assert program.waiting()

    def testOneAction(self, program):
        drone_id = 'uav'

        drone_plan = program.operation_plan.drone_plan_from_id(drone_id)
        for waypoint in drone_plan.path:
            program.drone_position(drone_id, waypoint.latitude, waypoint.longitude)
        assert program.finished()

    def testMissionState(self, full_program):
        assert full_program.runtime.mission.waiting()
        assert full_program.runtime.mission.node.waiting()

        full_program.drone_position('uav1', 10.3827, 55.4044)
        assert full_program.runtime.mission.executing()
        assert full_program.runtime.mission.node.executing()

    def testActionState(self, full_program):
        assert full_program.runtime.mission.node.nodes[0].waiting()

        full_program.drone_position('uav1', 10.3827, 55.4044)
        assert full_program.runtime.mission.node.nodes[0].executing()
        assert full_program.runtime.mission.node.nodes[1].waiting()

        full_program.drone_position('uav1', 10.3766, 55.4122)
        assert full_program.runtime.mission.node.nodes[0].finished()
        assert full_program.runtime.mission.node.nodes[1].waiting()

        full_program.drone_position('uav2', 10.3872, 55.4055)
        assert full_program.runtime.mission.node.nodes[0].finished()
        assert full_program.runtime.mission.node.nodes[1].executing()

        full_program.drone_position('uav2', 10.3809, 55.4132)
        assert full_program.runtime.mission.node.nodes[0].finished()
        assert full_program.runtime.mission.node.nodes[1].finished()

    def testCoordinationState(self, full_program):
        # nodes[2] is the first sequential coordination statement
        coordination = full_program.runtime.mission.node.nodes[2]
        assert coordination.waiting()

        drone_id1 = 'uav1'
        drone_id2 = 'uav2'
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id1, 10.3766, 55.4122)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        full_program.drone_position(drone_id2, 10.3809, 55.4132)

        # just before executing coordination
        assert coordination.waiting()
        # every body statement should be waiting too
        for node in coordination.nodes:
            node.waiting()

        # drone1 is in the way
        full_program.drone_position(drone_id1, 10.3788, 55.4079)
        assert coordination.executing()
        assert coordination.nodes[0].executing()
        assert coordination.nodes[1].waiting()

        # drone1 arrived, waiting for drone2 to start
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        assert coordination.executing()
        assert coordination.nodes[0].finished()
        assert coordination.nodes[1].waiting()

        # drone2 is in the way
        full_program.drone_position(drone_id2, 10.3788, 55.4079)
        assert coordination.executing()
        assert coordination.nodes[0].finished()
        assert coordination.nodes[1].executing()

        # drone2 arrived
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        assert coordination.finished()
        assert coordination.nodes[0].finished()
        assert coordination.nodes[1].finished()


    def testConstraintState(self, full_program):
        # nodes[5] is constraint node
        constraint = full_program.runtime.mission.node.nodes[5]
        assert constraint.waiting()

        drone_id1 = 'uav1'
        drone_id2 = 'uav2'
        drone1_plan = full_program.operation_plan.drone_plan_from_id(drone_id1).path
        drone2_plan = full_program.operation_plan.drone_plan_from_id(drone_id2).path
        full_program.drone_position(drone_id1, drone1_plan[0].latitude, drone1_plan[0].longitude)
        full_program.drone_position(drone_id1, drone1_plan[1].latitude, drone1_plan[1].longitude)
        full_program.drone_position(drone_id2, drone2_plan[0].latitude, drone2_plan[0].longitude)
        full_program.drone_position(drone_id2, drone2_plan[1].latitude, drone2_plan[1].longitude)
        full_program.drone_position(drone_id1, drone1_plan[2].latitude, drone1_plan[2].longitude)
        full_program.drone_position(drone_id2, drone2_plan[2].latitude, drone2_plan[2].longitude)
        full_program.drone_position(drone_id1, drone1_plan[3].latitude, drone1_plan[3].longitude)
        full_program.drone_position(drone_id2, drone2_plan[3].latitude, drone2_plan[3].longitude)
        full_program.drone_position(drone_id1, drone1_plan[4].latitude, drone1_plan[4].longitude)
        full_program.drone_position(drone_id2, drone2_plan[4].latitude, drone2_plan[4].longitude)
        # just before executing constraint
        assert constraint.waiting()

        # drone1 traveling to its next position
        full_program.drone_position(drone_id1, drone1_plan[5].latitude, drone1_plan[5].longitude)
        assert constraint.executing()
        assert constraint.nodes[0].executing()
        assert constraint.nodes[1].waiting()
        full_program.drone_position(drone_id1, drone1_plan[6].latitude, drone1_plan[6].longitude)
        assert constraint.executing()
        assert constraint.nodes[0].executing()
        assert constraint.nodes[1].waiting()

        # drone1 arrived
        full_program.drone_position(drone_id1, drone1_plan[7].latitude, drone1_plan[7].longitude)
        assert constraint.executing()
        assert constraint.nodes[0].finished()
        assert constraint.nodes[1].waiting()

        # drone2 traveling to its next position
        full_program.drone_position(drone_id2, drone2_plan[5].latitude, drone2_plan[5].longitude)
        assert constraint.executing()
        assert constraint.nodes[0].finished()
        assert constraint.nodes[1].executing()

        # drone2 arrived
        full_program.drone_position(drone_id2, drone2_plan[6].latitude, drone2_plan[6].longitude)
        assert constraint.nodes[0].finished()
        assert constraint.nodes[1].finished()
        assert constraint.finished()

    def testMissionFinishedStatements(self, full_program):
        drone_id1 = 'uav1'
        drone_id2 = 'uav2'
        drone1_plan = full_program.operation_plan.drone_plan_from_id(drone_id1).path
        drone2_plan = full_program.operation_plan.drone_plan_from_id(drone_id2).path
        full_program.drone_position(drone_id1, drone1_plan[0].latitude, drone1_plan[0].longitude)
        full_program.drone_position(drone_id1, drone1_plan[1].latitude, drone1_plan[1].longitude)
        full_program.drone_position(drone_id2, drone2_plan[0].latitude, drone2_plan[0].longitude)
        full_program.drone_position(drone_id2, drone2_plan[1].latitude, drone2_plan[1].longitude)
        full_program.drone_position(drone_id1, drone1_plan[2].latitude, drone1_plan[2].longitude)
        full_program.drone_position(drone_id2, drone2_plan[2].latitude, drone2_plan[2].longitude)
        full_program.drone_position(drone_id1, drone1_plan[3].latitude, drone1_plan[3].longitude)
        full_program.drone_position(drone_id2, drone2_plan[3].latitude, drone2_plan[3].longitude)
        full_program.drone_position(drone_id1, drone1_plan[4].latitude, drone1_plan[4].longitude)
        full_program.drone_position(drone_id2, drone2_plan[4].latitude, drone2_plan[4].longitude)
        full_program.drone_position(drone_id1, drone1_plan[5].latitude, drone1_plan[5].longitude)
        full_program.drone_position(drone_id1, drone1_plan[6].latitude, drone1_plan[6].longitude)
        full_program.drone_position(drone_id1, drone1_plan[7].latitude, drone1_plan[7].longitude)
        full_program.drone_position(drone_id2, drone2_plan[5].latitude, drone2_plan[5].longitude)
        full_program.drone_position(drone_id2, drone2_plan[6].latitude, drone2_plan[6].longitude)
        full_program.drone_position(drone_id1, drone1_plan[8].latitude, drone1_plan[8].longitude)
        full_program.drone_position(drone_id2, drone2_plan[7].latitude, drone2_plan[7].longitude)

        assert full_program.runtime.mission.finished()