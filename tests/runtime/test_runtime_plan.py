from dots.ast_parser import DotsXML
from dots.core.program import Program, RuntimeStraightAction, RuntimePatrolAction
from dots.planner import DefaultPlanner
import os
import pathlib
import json
import pytest

class TestRuntimePlan:

    @pytest.fixture
    def program(self):
        xml = ''
        with open(os.path.join(pathlib.Path(__file__).parent.parent.absolute(), 'dots_programs', 'base_program.xml'), 'r') as file:
            xml = file.read()
        arguments = ''
        with open(os.path.join(pathlib.Path(__file__).parent.parent.absolute(), 'dots_programs', 'base_program.json'), 'r') as file:
            arguments = file.read()
        operation = DotsXML().parse(xml)
        arguments = json.loads(arguments)
        program = Program(operation, arguments)
        DefaultPlanner().plan(program)
        return program

    def testRunTimeActionCallGeneration(self, program):
        
        assert len(program.runtime.drones) is 1
        assert len(program.runtime.drones['uav']) is 1
        assert isinstance(program.runtime.drones['uav'][0], RuntimeStraightAction)
        assert program.runtime.mission.node.nodes[0].nodes[0] is \
                    program.runtime.drones['uav'][0]

    def testRunTimePatrolCallGeneration(self, program):
        xml = """
            <operation name="opTest">
                <action_definition name="patrol">
                    <parameter type="Area" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="Area" array="false" name="patrol_area" />
                <mission>
                    <sequential>
                        <action name="patrol">
                            <left reference="uav" />
                            <argument reference="patrol_area" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
              "patrol_area" : {
                "boundingBox" : [
                    {
                      "latitude": 0,
                      "longitude": 0
                    },
                    {
                      "latitude": 0,
                      "longitude": 1
                    },
                    {
                      "latitude": 1,
                      "longitude": 1
                    },
                    {
                      "latitude": 1,
                      "longitude": 0
                    }
                ]
              }
                
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        DefaultPlanner().plan(program)
        assert len(program.runtime.drones) is 1
        assert len(program.runtime.drones['uav']) is 1
        assert isinstance(program.runtime.drones['uav'][0], RuntimePatrolAction)