from dots.ast_parser import DotsXML
from dots.core.program import Program, RuntimeMission, \
                            RuntimeCoordination, RuntimeConstraint, \
                            RuntimeActionCall
from dots.planner import DefaultPlanner
import os
import pathlib
import json
import pytest

class TestRuntimeTree:

    @pytest.fixture
    def program(self):
        xml = ''
        with open(os.path.join(pathlib.Path(__file__).parent.parent.absolute(), 'dots_programs', 'base_program.xml'), 'r') as file:
            xml = file.read()
        arguments = ''
        with open(os.path.join(pathlib.Path(__file__).parent.parent.absolute(), 'dots_programs', 'base_program.json'), 'r') as file:
            arguments = file.read()
        operation = DotsXML().parse(xml)
        arguments = json.loads(arguments)
        program = Program(operation, arguments)
        DefaultPlanner().plan(program)
        return program

    def testRunTimeTreeGeneration(self, program):
        
        assert program.runtime is not None
        assert isinstance(program.runtime.mission, RuntimeMission)
        assert isinstance(program.runtime.mission.node, RuntimeCoordination)
        assert isinstance(program.runtime.mission.node.nodes[0], RuntimeConstraint)
        assert isinstance(program.runtime.mission.node.nodes[0].nodes[0], RuntimeActionCall)

        runtime_action = program.runtime.mission.node.nodes[0].nodes[0]
        ast_action = program.operation.mission.node.nodes[0].nodes[0]
        assert runtime_action.ast_node == ast_action