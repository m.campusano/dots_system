from dots.utils.decision_tree import DecisionNode, DecisionLeaf

class TestDecisionTree:

    def testSimpleTree(self):
        leafA = LeafA()
        nodeA = NodeA(leafA)

        assert nodeA.decide() == leafA

    def testDecision(self):
        leafA = LeafA()
        leafB = LeafB()
        
        nodeB = NodeB(-1, leafA, leafB)
        assert nodeB.decide() == leafA

        nodeB = NodeB(1, leafA, leafB)
        assert nodeB.decide() == leafB

    def testMultipleNodes(self):
        leafA = LeafA()
        leafB = LeafB()
        leafC = LeafC()
        
        index = 0
        nodeB = NodeB(index, leafA, leafB)
        nodeC = NodeC(index, leafC, nodeB)
        assert nodeC.decide() == leafC

        index = 1
        nodeB = NodeB(index, leafA, leafB)
        nodeC = NodeC(index, leafC, nodeB)
        assert nodeC.decide() == leafB

        index = -1
        nodeB = NodeB(index, leafA, leafB)
        nodeC = NodeC(index, leafC, nodeB)
        assert nodeC.decide() == leafA

class NodeA(DecisionNode):

    def __init__(self, leaf):
        super().__init__(["pass"])
        self.add_child("pass", leaf)

    def decide_function(self):
        return "pass"

class NodeB(DecisionNode):

    def __init__(self, index, leafA, leafB):
        super().__init__(["left", 'right'])
        self.add_child("left", leafA)
        self.add_child("right", leafB)
        self.index = index

    def decide_function(self):
        if self.index < 0:
            return 'left'
        else:
            return 'right'

class NodeC(DecisionNode):

    def __init__(self, index, zero, rest):
        super().__init__(["zero", 'rest'])
        self.add_child("zero", zero)
        self.add_child("rest", rest)
        self.index = index

    def decide_function(self):
        if self.index == 0:
            return 'zero'
        else:
            return 'rest'

class LeafA(DecisionLeaf):

    def value(self):
        return 'LeafA'

class LeafB(DecisionLeaf):

    def value(self):
        return 'LeafB'

class LeafC(DecisionLeaf):

    def value(self):
        return 'LeafC'