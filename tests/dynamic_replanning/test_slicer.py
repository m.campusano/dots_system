from dots.ast_parser import DotsXML
from dots.core.program import Program
from dots.planner import DefaultPlanner
from dots.ast import NoFlyZone
from dots.slicer import Slicer
import os
import pathlib
import json
import pytest
import textwrap

class TestSlicer:

    @pytest.fixture
    def full_program(self):
        xml = ''
        with open(os.path.join(pathlib.Path(__file__).parent.parent.absolute(), 'dots_programs', 'all_statements.xml'), 'r') as file:
            xml = file.read()
        arguments = ''
        with open(os.path.join(pathlib.Path(__file__).parent.parent.absolute(), 'dots_programs', 'all_statements.json'), 'r') as file:
            arguments = file.read()
        operation = DotsXML().parse(xml)
        arguments = json.loads(arguments)
        program = Program(operation, arguments)
        DefaultPlanner().plan(program)
        return program

    def testSliceAction(self, full_program):
        drone_id = 'uav1'
        full_program.drone_position(drone_id, 10.3806, 55.4078)

        slicer = Slicer()
        sliced_program = slicer.slice(full_program)

        drone2 = full_program.arguments['uav2']
        sliced_drone1 = sliced_program.arguments['uav1']
        sliced_drone2 = sliced_program.arguments['uav2']

        # change take off position of moving drone
        assert sliced_drone1.take_off_position.latitude == 10.3806
        assert sliced_drone1.take_off_position.longitude == 55.4078
        assert sliced_drone2.take_off_position.latitude == \
                drone2.take_off_position.latitude
        assert sliced_drone2.take_off_position.longitude == \
                drone2.take_off_position.longitude

        # sliced program
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Straight<location>
                    point1 : navigation_point
                    point2 : navigation_point
                    point3 : navigation_point
                    point4 : navigation_point
                    uav1 : drone
                    uav2 : drone
                    noFlyZone : area
                    implementation:
                        uav1 += Straight(point1)
                        uav2 += Straight(point2)
                        sequential
                            uav1 += Straight(point3)
                            uav2 += Straight(point4)
                        end
                        parallel
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        prioritized
                            uav1 += Straight(point3)
                            uav2 += Straight(point4)
                        end
                        constraint outside noFlyZone
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        uav1 += Straight(point3)
                        uav2 += Straight(point4)''')


    def testSliceExecutedActions(self, full_program):
        drone_id = 'uav1'
        full_program.drone_position(drone_id, 10.3827, 55.4044)
        full_program.drone_position(drone_id, 10.3766, 55.4122)

        slicer = Slicer()
        sliced_program = slicer.slice(full_program)

        sliced_drone1 = sliced_program.arguments['uav1']
        assert sliced_drone1.take_off_position.latitude == 10.3766
        assert sliced_drone1.take_off_position.longitude == 55.4122

        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Straight<location>
                    point1 : navigation_point
                    point2 : navigation_point
                    point3 : navigation_point
                    point4 : navigation_point
                    uav1 : drone
                    uav2 : drone
                    noFlyZone : area
                    implementation:
                        uav2 += Straight(point2)
                        sequential
                            uav1 += Straight(point3)
                            uav2 += Straight(point4)
                        end
                        parallel
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        prioritized
                            uav1 += Straight(point3)
                            uav2 += Straight(point4)
                        end
                        constraint outside noFlyZone
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        uav1 += Straight(point3)
                        uav2 += Straight(point4)''')

    def testSliceCoordinationSequential(self, full_program):
        drone_id1 = 'uav1'
        drone_id2 = 'uav2'
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id1, 10.3766, 55.4122)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        full_program.drone_position(drone_id2, 10.3809, 55.4132)
        full_program.drone_position(drone_id1, 10.3788, 55.4079)

        slicer = Slicer()
        sliced_program = slicer.slice(full_program)

        sliced_drone1 = sliced_program.arguments['uav1']
        sliced_drone2 = sliced_program.arguments['uav2']
        assert sliced_drone1.take_off_position.latitude == 10.3788
        assert sliced_drone1.take_off_position.longitude == 55.4079
        assert sliced_drone2.take_off_position.latitude == 10.3809
        assert sliced_drone2.take_off_position.longitude == 55.4132
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Straight<location>
                    point1 : navigation_point
                    point2 : navigation_point
                    point3 : navigation_point
                    point4 : navigation_point
                    uav1 : drone
                    uav2 : drone
                    noFlyZone : area
                    implementation:
                        sequential
                            uav1 += Straight(point3)
                            uav2 += Straight(point4)
                        end
                        parallel
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        prioritized
                            uav1 += Straight(point3)
                            uav2 += Straight(point4)
                        end
                        constraint outside noFlyZone
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        uav1 += Straight(point3)
                        uav2 += Straight(point4)''')


    def testSliceCoordinationParallel(self, full_program):
        drone_id1 = 'uav1'
        drone_id2 = 'uav2'
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id1, 10.3766, 55.4122)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        full_program.drone_position(drone_id2, 10.3809, 55.4132)
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        full_program.drone_position(drone_id1, 10.3788, 55.4079)

        slicer = Slicer()
        sliced_program = slicer.slice(full_program)
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Straight<location>
                    point1 : navigation_point
                    point2 : navigation_point
                    point3 : navigation_point
                    point4 : navigation_point
                    uav1 : drone
                    uav2 : drone
                    noFlyZone : area
                    implementation:
                        prioritized
                            uav1 += Straight(point1)
                            parallel
                                uav2 += Straight(point2)
                            end
                        end
                        prioritized
                            uav1 += Straight(point3)
                            uav2 += Straight(point4)
                        end
                        constraint outside noFlyZone
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        uav1 += Straight(point3)
                        uav2 += Straight(point4)''')

    def testSliceCoordinationParallelAllActionExecuting(self, full_program):
        drone_id1 = 'uav1'
        drone_id2 = 'uav2'
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id1, 10.3766, 55.4122)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        full_program.drone_position(drone_id2, 10.3809, 55.4132)
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        full_program.drone_position(drone_id1, 10.3788, 55.4079)
        full_program.drone_position(drone_id2, 10.3788, 55.4079)

        slicer = Slicer()
        sliced_program = slicer.slice(full_program)
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Straight<location>
                    point1 : navigation_point
                    point2 : navigation_point
                    point3 : navigation_point
                    point4 : navigation_point
                    uav1 : drone
                    uav2 : drone
                    noFlyZone : area
                    implementation:
                        prioritized
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        prioritized
                            uav1 += Straight(point3)
                            uav2 += Straight(point4)
                        end
                        constraint outside noFlyZone
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        uav1 += Straight(point3)
                        uav2 += Straight(point4)''')

    def testSliceCoordinationPrioritized(self, full_program):
        drone_id1 = 'uav1'
        drone_id2 = 'uav2'
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id1, 10.3766, 55.4122)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        full_program.drone_position(drone_id2, 10.3809, 55.4132)
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        full_program.drone_position(drone_id1, 10.3766, 55.4122)
        full_program.drone_position(drone_id2, 10.3809, 55.4132)
        full_program.drone_position(drone_id1, 10.3788, 55.4079)

        slicer = Slicer()
        sliced_program = slicer.slice(full_program)
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Straight<location>
                    point1 : navigation_point
                    point2 : navigation_point
                    point3 : navigation_point
                    point4 : navigation_point
                    uav1 : drone
                    uav2 : drone
                    noFlyZone : area
                    implementation:
                        prioritized
                            uav1 += Straight(point3)
                            prioritized
                                uav2 += Straight(point4)
                            end
                        end
                        constraint outside noFlyZone
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        uav1 += Straight(point3)
                        uav2 += Straight(point4)''')

    def testSliceExecutedCoordination(self, full_program):
        drone_id1 = 'uav1'
        drone_id2 = 'uav2'
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id1, 10.3766, 55.4122)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        full_program.drone_position(drone_id2, 10.3809, 55.4132)
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)
        full_program.drone_position(drone_id1, 10.3766, 55.4122)
        full_program.drone_position(drone_id2, 10.3809, 55.4132)
        full_program.drone_position(drone_id1, 10.3827, 55.4044)
        full_program.drone_position(drone_id2, 10.3872, 55.4055)

        slicer = Slicer()
        sliced_program = slicer.slice(full_program)
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Straight<location>
                    point1 : navigation_point
                    point2 : navigation_point
                    point3 : navigation_point
                    point4 : navigation_point
                    uav1 : drone
                    uav2 : drone
                    noFlyZone : area
                    implementation:
                        constraint outside noFlyZone
                            uav1 += Straight(point1)
                            uav2 += Straight(point2)
                        end
                        uav1 += Straight(point3)
                        uav2 += Straight(point4)''')

    def testSliceConstraint(self, full_program):
        drone_id1 = 'uav1'
        drone_id2 = 'uav2'
        drone1_plan = full_program.operation_plan.drone_plan_from_id(drone_id1).path
        drone2_plan = full_program.operation_plan.drone_plan_from_id(drone_id2).path
        full_program.drone_position(drone_id1, drone1_plan[0].latitude, drone1_plan[0].longitude)
        full_program.drone_position(drone_id1, drone1_plan[1].latitude, drone1_plan[1].longitude)
        full_program.drone_position(drone_id2, drone2_plan[0].latitude, drone2_plan[0].longitude)
        full_program.drone_position(drone_id2, drone2_plan[1].latitude, drone2_plan[1].longitude)
        full_program.drone_position(drone_id1, drone1_plan[2].latitude, drone1_plan[2].longitude)
        full_program.drone_position(drone_id2, drone2_plan[2].latitude, drone2_plan[2].longitude)
        full_program.drone_position(drone_id1, drone1_plan[3].latitude, drone1_plan[3].longitude)
        full_program.drone_position(drone_id2, drone2_plan[3].latitude, drone2_plan[3].longitude)
        full_program.drone_position(drone_id1, drone1_plan[4].latitude, drone1_plan[4].longitude)
        full_program.drone_position(drone_id2, drone2_plan[4].latitude, drone2_plan[4].longitude)
        full_program.drone_position(drone_id1, drone1_plan[5].latitude, drone1_plan[5].longitude)
        full_program.drone_position(drone_id1, drone1_plan[6].latitude, drone1_plan[6].longitude)
        full_program.drone_position(drone_id1, drone1_plan[7].latitude, drone1_plan[7].longitude)
        full_program.drone_position(drone_id2, drone2_plan[5].latitude, drone2_plan[5].longitude)

        ## DOING: check plan how many points are
        slicer = Slicer()
        sliced_program = slicer.slice(full_program)
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Straight<location>
                    point1 : navigation_point
                    point2 : navigation_point
                    point3 : navigation_point
                    point4 : navigation_point
                    uav1 : drone
                    uav2 : drone
                    noFlyZone : area
                    implementation:
                        constraint outside noFlyZone
                            uav2 += Straight(point2)
                        end
                        uav1 += Straight(point3)
                        uav2 += Straight(point4)''')

    def testSliceExecutedConstraint(self, full_program):
        drone_id1 = 'uav1'
        drone_id2 = 'uav2'
        drone1_plan = full_program.operation_plan.drone_plan_from_id(drone_id1).path
        drone2_plan = full_program.operation_plan.drone_plan_from_id(drone_id2).path
        full_program.drone_position(drone_id1, drone1_plan[0].latitude, drone1_plan[0].longitude)
        full_program.drone_position(drone_id1, drone1_plan[1].latitude, drone1_plan[1].longitude)
        full_program.drone_position(drone_id2, drone2_plan[0].latitude, drone2_plan[0].longitude)
        full_program.drone_position(drone_id2, drone2_plan[1].latitude, drone2_plan[1].longitude)
        full_program.drone_position(drone_id1, drone1_plan[2].latitude, drone1_plan[2].longitude)
        full_program.drone_position(drone_id2, drone2_plan[2].latitude, drone2_plan[2].longitude)
        full_program.drone_position(drone_id1, drone1_plan[3].latitude, drone1_plan[3].longitude)
        full_program.drone_position(drone_id2, drone2_plan[3].latitude, drone2_plan[3].longitude)
        full_program.drone_position(drone_id1, drone1_plan[4].latitude, drone1_plan[4].longitude)
        full_program.drone_position(drone_id2, drone2_plan[4].latitude, drone2_plan[4].longitude)
        full_program.drone_position(drone_id1, drone1_plan[5].latitude, drone1_plan[5].longitude)
        full_program.drone_position(drone_id1, drone1_plan[6].latitude, drone1_plan[6].longitude)
        full_program.drone_position(drone_id1, drone1_plan[7].latitude, drone1_plan[7].longitude)
        full_program.drone_position(drone_id2, drone2_plan[5].latitude, drone2_plan[5].longitude)
        full_program.drone_position(drone_id2, drone2_plan[6].latitude, drone2_plan[6].longitude)

        slicer = Slicer()
        sliced_program = slicer.slice(full_program)
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Straight<location>
                    point1 : navigation_point
                    point2 : navigation_point
                    point3 : navigation_point
                    point4 : navigation_point
                    uav1 : drone
                    uav2 : drone
                    noFlyZone : area
                    implementation:
                        uav1 += Straight(point3)
                        uav2 += Straight(point4)''')

    def testSliceUsingDrone(self):
        xml = """
            <operation name="opTest">
                <action-definition name="Straight">
                    <parameter type="NavigationPoint" array="false" />
                </action-definition>
                <parameter type="Drone" array="true" name="uavs" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential drone="uav" drone-list="uavs">
                        <action name="Straight">
                            <left reference="uav" />
                            <argument reference="point1" />
                        </action>
                        <action name="Straight">
                            <left reference="uav" />
                            <argument reference="point2" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    }
                ],
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """
        operation = DotsXML().parse(xml)
        arguments = json.loads(arguments)
        program = Program(operation, arguments)
        DefaultPlanner().plan(program)
        drone_id = program.operation_plan.drone_plans[0].drone.uuid
        program.drone_position(drone_id, 0, 0)
        program.drone_position(drone_id, 10, 10)
        program.drone_position(drone_id, 15, 15)
        
        index = 0
        if drone_id == 'uav2':
            index = 1

        slicer = Slicer()
        sliced_program = slicer.slice(program)
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Straight<navigation_point>
                    uavs : drone[]
                    point1 : navigation_point
                    point2 : navigation_point
                    implementation:
                        uavs[{index}] += Straight(point2)'''.format(index=index))

    def testSlicePatrolAction(self):
        xml = """
            <operation name="opTest">
                <action-definition name="Patrol">
                    <parameter type="Area" array="false" />
                </action-definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="Area" array="false" name="patrol_area" />
                <mission>
                    <sequential>
                        <action name="Patrol">
                            <left reference="uav" />
                            <argument reference="patrol_area" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 1,
                    "longitude": 0
                  }
                },
              "patrol_area" : {
                "boundingBox" : [
                    {
                      "latitude": 0,
                      "longitude": 0
                    },
                    {
                      "latitude": 0,
                      "longitude": 1
                    },
                    {
                      "latitude": 1,
                      "longitude": 1
                    },
                    {
                      "latitude": 1,
                      "longitude": 0
                    }
                ]
              }
                
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        DefaultPlanner().plan(program)

        drone_id = 'uav'
        program.drone_position(drone_id, 1, 0)
        program.drone_position(drone_id, 0, 0)
        program.drone_position(drone_id, 0, 1)
        program.drone_position(drone_id, 0.5, 1)

        slicer = Slicer()
        sliced_program = slicer.slice(program)

        sliced_drone = sliced_program.arguments['uav']

        # change take off position of moving drone
        assert sliced_drone.take_off_position.latitude == 0.5
        assert sliced_drone.take_off_position.longitude == 1
        # sliced program
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_opTest:
                    import action Patrol<area>
                    import action Straight<navigation_point>
                    uav : drone
                    patrol_area : area
                    implementation:
                        sequential
                            uav += Straight(patrol_area[2])
                            uav += Straight(patrol_area[3])
                            uav += Straight(patrol_area[0])
                        end''')

    def testSliceUsingDrone(self):
        xml = """
            <operation name="test">
                <action-definition name="Straight">
                    <parameter type="location" array="false" />
                </action-definition>
                <parameter type="Drone" array="true" name="uavs" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <parameter type="NavigationPoint" array="false" name="point3" />
                <parameter type="NavigationPoint" array="false" name="point4" />
                <mission>
                    <sequential>
                        <prioritized drone="uav" drone-list="uavs">
                            <action name="Straight">
                                <left reference="uav" />
                                <argument reference="point1" />
                            </action>
                            <action name="Straight">
                                <left reference="uav" />
                                <argument reference="point2" />
                            </action>
                            <action name="Straight">
                                <left reference="uav" />
                                <argument reference="point3" />
                            </action>
                            <action name="Straight">
                                <left reference="uav" />
                                <argument reference="point4" />
                            </action>
                        </prioritized>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
                "uavs": [
                    {
                      "uuid": "uav",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    }
                ],
                "point1" :
                    {
                      "latitude": 0,
                      "longitude": 1
                    },
                "point2" :
                    {
                      "latitude": 0,
                      "longitude": 2
                    },
                "point3" :
                    {
                      "latitude": 0,
                      "longitude": 3
                    },
                "point4" :
                    {
                      "latitude": 0,
                      "longitude": 4
                    }
                
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        DefaultPlanner().plan(program)

        drone_id = 'uav'
        program.drone_position(drone_id, 0, 0)
        program.drone_position(drone_id, 0, 1)
        program.drone_position(drone_id, 0, 1.5)

        slicer = Slicer()
        sliced_program = slicer.slice(program)
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_test:
                    import action Straight<location>
                    uavs : drone[]
                    point1 : navigation_point
                    point2 : navigation_point
                    point3 : navigation_point
                    point4 : navigation_point
                    implementation:
                        prioritized
                            uavs[0] += Straight(point2)
                            prioritized
                                uavs[0] += Straight(point3)
                                uavs[0] += Straight(point4)
                            end
                        end''')

    def testSliceUsingDronePoints(self):
        xml = """
            <operation name="test">
                <action-definition name="Straight">
                    <parameter type="location" array="false" />
                </action-definition>
                <parameter type="Drone" array="true" name="uavs" />
                <parameter type="NavigationPoint" array="true" name="points" />
                <mission>
                    <sequential>
                        <parallel>
                            <sequential drone="uav" drone-list="uavs">
                                <action name="Straight">
                                    <left reference="uav" />
                                    <argument reference="points[0]" />
                                </action>
                            </sequential>
                            <sequential drone="uav" drone-list="uavs">
                                <action name="Straight">
                                    <left reference="uav" />
                                    <argument reference="points[1]" />
                                </action>
                            </sequential>
                            <sequential drone="uav" drone-list="uavs">
                                <action name="Straight">
                                    <left reference="uav" />
                                    <argument reference="points[2]" />
                                </action>
                            </sequential>
                        </parallel>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
                "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    }
                ],
                "points" : [
                    {
                      "latitude": 1,
                      "longitude": 1
                    },
                    {
                      "latitude": 0,
                      "longitude": 1
                    },
                    {
                      "latitude": 1,
                      "longitude": 0
                    }
                ]
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        DefaultPlanner().plan(program)

        program.drone_position("uav1", 0, 0)
        program.drone_position("uav2", 0, 0)
        program.drone_position("uav1", 0.5, 0.5)
        program.drone_position("uav2", 0, 0.5)

        slicer = Slicer()
        sliced_program = slicer.slice(program)
        assert sliced_program.operation.to_dots_string().replace('\t', '    ') == textwrap.dedent('''\
                operation sliced_test:
                    import action Straight<location>
                    uavs : drone[]
                    points : navigation_point[]
                    implementation:
                        prioritized
                            sequential
                                uavs[0] += Straight(points[0])
                            end
                            sequential
                                uavs[1] += Straight(points[1])
                            end
                            parallel
                                sequential using uav from uavs
                                    uav += Straight(points[2])
                                end
                            end
                        end''')