import pytest
from dots.daa.hospital import USpace
from geopy.distance import geodesic

class TestUSpaceDetection:

    @pytest.fixture
    def u_space(self):
        return USpace()

    def testVolumeOutside(self, u_space):
        flight_point = [55.36261998117, 10.335890008137]
        altitude = 350

        assert not u_space.is_inside_detection_volume(flight_point, altitude)

    def testVolume2DInsideTooHigh(self, u_space):
        flight_point = [55.362849980593, 10.33612000756]
        altitude = 550

        assert not u_space.is_inside_detection_volume(flight_point, altitude)

    def testVolumeInside(self, u_space):
        flight_point = [55.362849980593, 10.33612000756]
        altitude = 290

        assert u_space.is_inside_detection_volume(flight_point, altitude)

    def testVolumeInsideAltitudeLimt(self, u_space):
        flight_point = [55.362849980593, 10.33612000756]
        altitude = 500

        assert u_space.is_inside_detection_volume(flight_point, altitude)

    def testWellClearOutside(self, u_space):
        flight_point = [55.362849980593, 10.33612000756]
        altitude = 150

        assert not u_space.is_inside_well_clear_volume(flight_point, altitude)

    def testWellClear2DInsideTooHigh(self, u_space):
        flight_point = [55.380219966174, 10.350950025022]
        altitude = 550

        assert not u_space.is_inside_well_clear_volume(flight_point, altitude)

    def testWellClearInside(self, u_space):
        flight_point = [55.380219966174, 10.350950025022]
        altitude = 100

        assert u_space.is_inside_detection_volume(flight_point, altitude)

    def testWellClearInsideAltitudeLimt(self, u_space):
        flight_point = [55.380219966174, 10.350950025022]
        altitude = 500

        assert u_space.is_inside_detection_volume(flight_point, altitude)