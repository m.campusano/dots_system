import pytest

from dots.daa import OperationStatus, FlightPoint
from dots.daa.hospital import RiskIdentifier

class TestRiskIdentifier:

    @pytest.fixture
    def risk_identifier(self):
        return RiskIdentifier()

    @pytest.fixture
    def started_operation(self):
        return OperationStatus(True)

    def testRiskIdentifierInit(self):
        RiskIdentifier()

    def testNonStartedOperation(self, risk_identifier):
        point = FlightPoint()
        point.latitude = 55.363409975544 
        point.longitude = 10.336470035836
        point.altitude = 350

        risk = risk_identifier.identify(OperationStatus(), point)
        assert not risk.is_risky()

    def testOutsideDetection(self, risk_identifier, started_operation):
        point = FlightPoint()
        point.latitude = 55.36261998117
        point.longitude = 10.335890008137
        point.altitude = 350
        risk = risk_identifier.identify(started_operation, point)

        assert not risk.is_risky()

    def testInsideDetectionNonFlying(self, risk_identifier, started_operation):
        point = FlightPoint()
        point.latitude = 55.363409975544 
        point.longitude = 10.336470035836
        point.altitude = 250
        started_operation.land()
        risk = risk_identifier.identify(started_operation, point)

        assert risk.is_risky()
        assert risk.code == 11

    def testInsideDetectionFlying(self, risk_identifier, started_operation):
        point = FlightPoint()
        point.latitude = 55.363409975544 
        point.longitude = 10.336470035836
        point.altitude = 250
        started_operation.fly()
        risk = risk_identifier.identify(started_operation, point)

        assert risk.is_risky()
        assert risk.code == 12

    def testInsideWellClearNonFlying(self, risk_identifier, started_operation):
        point = FlightPoint()
        point.latitude = 55.380219966174
        point.longitude = 10.350950025022
        point.altitude = 100
        started_operation.land()
        risk = risk_identifier.identify(started_operation, point)

        assert risk.is_risky()
        assert risk.code == 21

    def testInsideWellClearFlying(self, risk_identifier, started_operation):
        point = FlightPoint()
        point.latitude = 55.380219966174
        point.longitude = 10.350950025022
        point.altitude = 100
        started_operation.fly()
        risk = risk_identifier.identify(started_operation, point)

        assert risk.is_risky()
        assert risk.code == 22

    ### 
    # This is not longer necessary
    # Altitude of WC and DV are the same
    ##
    # def testInsideVolumeAboveWellClearNonFlying(self, risk_identifier, started_operation):
    #     point = FlightPoint()
    #     point.latitude = 55.380219966174
    #     point.longitude = 10.350950025022
    #     point.altitude = 200
    #     started_operation.land()
    #     risk = risk_identifier.identify(started_operation, point)

    #     assert risk.is_risky()
    #     assert risk.code == 11

    ### 
    # This is not longer necessary
    # Altitude of WC and DV are the same
    ##
    # def testInsideVolumeAboveWellClearFlying(self, risk_identifier, started_operation):
    #     point = FlightPoint()
    #     point.latitude = 55.380219966174
    #     point.longitude = 10.350950025022
    #     point.altitude = 200
    #     started_operation.fly()
    #     risk = risk_identifier.identify(started_operation, point)

    #     assert risk.is_risky()
    #     assert risk.code == 12