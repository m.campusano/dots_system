import pytest

from dots.daa import Alert


class TestAlert:

    def testRiskComparisonDifferentLevels(self):
        assert Alert.warning().riskier_than(Alert.corrective())
        assert Alert.warning().riskier_than(Alert.preventive())
        assert Alert.warning().riskier_than(Alert.informative())

        assert Alert.corrective().riskier_than(Alert.preventive())
        assert Alert.corrective().riskier_than(Alert.informative())

        assert Alert.preventive().riskier_than(Alert.informative())

    def testRiskComparisonSameLevels(self):
        warning1 = Alert.warning()
        warning1.risk_code = 1

        warning2 = Alert.warning()
        warning2.risk_code = 2

        assert warning2.riskier_than(warning1)