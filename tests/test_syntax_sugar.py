
from dots.ast_parser import DotsXML
from dots.ast import Coordination, ActionCall
from dots.core.program import Program

import json

class TestSyntaxSugar:

    def testParallelLoops(self):
        xml = """
            <operation name="loop">
                <action-definition name="Straight">
                    <parameter type="Location" array="false" />
                </action-definition>
                <parameter type="NavigationPoint" array="true" name="points" />
                <parameter type="Drone" array="true" name="uavs" />
                <mission>
                    <sequential>
                        <for type="parallel" location="point" location-list="points" drone="uav" drone-list="uavs">
                            <action name="Straight">
                                <left reference="uav" />
                                <argument reference="point" />
                            </action>
                        </for>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 1
                      }
                    }
                ],
              "points" : [
                    {
                      "latitude": 1,
                      "longitude": 0
                    },
                    {
                      "latitude": 1,
                      "longitude": 1
                    }
                ]
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        program.apply_syntax_sugar()
        first_node = program.operation.mission.node
        
        parallel = first_node.nodes[0]
        assert type(parallel) is Coordination
        assert parallel.is_parallel()
        assert not parallel.is_using_drones()

        for index, node in enumerate(parallel.nodes):
            assert type(node) is Coordination
            assert node.is_sequential()
            assert node.is_using_drones()
            assert node.drone_reference_name == 'uav'
            assert node.drone_list_reference_name == 'uavs'

            action = node.nodes[0]
            assert type(action) is ActionCall
            assert action.left_argument.reference_name == 'uav'
            assert action.arguments[0].reference_name == f'points[{index}]'

    def testPrioritizedlLoops(self):
        xml = """
            <operation name="loop">
                <action-definition name="Straight">
                    <parameter type="Location" array="false" />
                </action-definition>
                <parameter type="NavigationPoint" array="true" name="points" />
                <parameter type="Drone" array="true" name="uavs" />
                <mission>
                    <sequential>
                        <for type="prioritized" location="point" location-list="points" drone="uav" drone-list="uavs">
                            <action name="Straight">
                                <left reference="uav" />
                                <argument reference="point" />
                            </action>
                        </for>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 1
                      }
                    }
                ],
              "points" : [
                    {
                      "latitude": 1,
                      "longitude": 0
                    },
                    {
                      "latitude": 1,
                      "longitude": 1
                    }
                ]
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        program.apply_syntax_sugar()
        first_node = program.operation.mission.node
        
        prioritized = first_node.nodes[0]
        assert type(prioritized) is Coordination
        assert prioritized.is_prioritized()
        assert not prioritized.is_using_drones()

        for index, node in enumerate(prioritized.nodes):
            assert type(node) is Coordination
            assert node.is_sequential()
            assert node.is_using_drones()
            assert node.drone_reference_name == 'uav'
            assert node.drone_list_reference_name == 'uavs'

            action = node.nodes[0]
            assert type(action) is ActionCall
            assert action.left_argument.reference_name == 'uav'
            assert action.arguments[0].reference_name == f'points[{index}]'

    def testSequentiallLoops(self):
        xml = """
            <operation name="loop">
                <action-definition name="Straight">
                    <parameter type="Location" array="false" />
                </action-definition>
                <parameter type="NavigationPoint" array="true" name="points" />
                <parameter type="Drone" array="true" name="uavs" />
                <mission>
                    <sequential>
                        <for type="sequential" location="point" location-list="points" drone="uav" drone-list="uavs">
                            <action name="Straight">
                                <left reference="uav" />
                                <argument reference="point" />
                            </action>
                        </for>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 1
                      }
                    }
                ],
              "points" : [
                    {
                      "latitude": 1,
                      "longitude": 0
                    },
                    {
                      "latitude": 1,
                      "longitude": 1
                    }
                ]
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        program.apply_syntax_sugar()
        first_node = program.operation.mission.node
        
        sequential = first_node.nodes[0]
        assert type(sequential) is Coordination
        assert sequential.is_sequential()
        assert not sequential.is_using_drones()

        for index, node in enumerate(sequential.nodes):
            assert type(node) is Coordination
            assert node.is_sequential()
            assert node.is_using_drones()
            assert node.drone_reference_name == 'uav'
            assert node.drone_list_reference_name == 'uavs'

            action = node.nodes[0]
            assert type(action) is ActionCall
            assert action.left_argument.reference_name == 'uav'
            assert action.arguments[0].reference_name == f'points[{index}]'


    def testArrayStraight(self):
        xml = """
            <operation name="loop">
                <action-definition name="Straight">
                    <parameter type="Location" array="false" />
                </action-definition>
                <parameter type="NavigationPoint" array="false" name="point" />
                <parameter type="Drone" array="true" name="uavs" />
                <mission>
                    <sequential>
                        <action name="Straight">
                            <left reference="uavs" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 1
                      }
                    }
                ],
              "point" : 
                    {
                      "latitude": 1,
                      "longitude": 0
                    }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        program.apply_syntax_sugar()
        first_node = program.operation.mission.node
        
        parallel = first_node.nodes[0]
        assert type(parallel) is Coordination
        assert parallel.is_parallel()
        assert not parallel.is_using_drones()

        for index, node in enumerate(parallel.nodes):
            assert type(node) is ActionCall
            assert node.left_argument.reference_name == f'uavs[{index}]'
            assert node.arguments[0].reference_name == f'point'