from dots.ast import Operation, ActionDefinition, Parameter, Mission, Coordination

class TestAST:

	def testActionDefinition(self):
		action = ActionDefinition("action")
		assert action.name == "action"

		parameter = Parameter()
		parameter.name = "param1"
		action.add_parameter(parameter)
		assert len(action.parameters) == 1
		assert action.parameters[0] == parameter

		parameter = Parameter()
		parameter.name = "param2"
		action.add_parameter(parameter)
		assert len(action.parameters) == 2
		assert action.parameters[1] == parameter

	def testOperation(self):
		operation = Operation("operation")
		assert operation.name == "operation"

		operation.mission = Mission()

	def testCoordination(self):
		prioritized = Coordination.prioritized()
		assert prioritized.is_prioritized()

		parallel = Coordination.parallel()
		assert parallel.is_parallel()

		sequential = Coordination.sequential()
		assert sequential.is_sequential()