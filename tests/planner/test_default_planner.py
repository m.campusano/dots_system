from dots.ast_parser import DotsXML
from dots.core.program import Program
from dots.core.type import Area, NavigationPoint
from dots.planner import DefaultPlanner

import json
import datetime

class TestDefaultPlanner:

    def testSimpleAction(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        
        operation_plan = DefaultPlanner().plan(program)
        plans = operation_plan.drone_plans
        assert len(plans) == 1

        # A path it is a rich collection of waypoints:
        # - latitude, longitude: latitude and longitude
        # - depart_to_here_time: When to start the navigation to this point
        plan = plans[0]
        start_time = DefaultPlanner.get_default_time().time
        assert plan.drone == program.arguments['uav']
        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        end_time = start_time + datetime.timedelta(seconds = 141421)
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

        assert len(plan.path) == 2
        assert plan.path[0].latitude == 0
        assert plan.path[0].longitude == 0
        assert plan.path[0].depart_to_here_time.time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.path[1].latitude == 10
        assert plan.path[1].longitude == 10


    def testMultipleAction(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point1" />
                        </action>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point2" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 10,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plans = operation_plan.drone_plans
        assert len(plans) == 1

        path = plans[0].path
        assert len(path) == 3
        assert path[0].latitude == 0
        assert path[0].longitude == 0
        assert path[1].latitude == 10
        assert path[1].longitude == 10
        assert path[2].latitude == 10
        assert path[2].longitude == 20

    def testSimpleActionMultipleDrone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav1" />
                <parameter type="Drone" array="false" name="uav2" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav1" />
                            <argument reference="point" />
                        </action>
                        <action name="straight">
                            <left reference="uav2" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav1":
                {
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "uav2":
                {
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 1,
                    "longitude": 1
                  }
                },
                "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plans = operation_plan.drone_plans
        assert len(plans) == 2

        path = plans[0].path
        assert len(path) == 2
        assert path[0].latitude == 0
        assert path[0].longitude == 0
        assert path[1].latitude == 10
        assert path[1].longitude == 10
        path = plans[1].path
        assert len(path) == 2
        assert path[0].latitude == 1
        assert path[0].longitude == 1
        assert path[1].latitude == 10
        assert path[1].longitude == 10
        

    def testNoFlyZone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Area" array="false" name="noFlyZone" />
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <constraint type="NoFlyZone" variable="noFlyZone">
                            <action name="straight">
                                <left reference="uav" />
                                <argument reference="point" />
                            </action>
                        </constraint>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "noFlyZone": {
                    "boundingBox": [
                      {
                        "latitude": 2,
                        "longitude": 2
                      },
                      {
                        "latitude": 2,
                        "longitude": 8
                      },
                      {
                        "latitude": 8,
                        "longitude": 8
                      },
                      {
                        "latitude": 8,
                        "longitude": 2
                      }
                    ]
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plans = operation_plan.drone_plans
        assert len(plans) == 1

        path = plans[0].path
        assert len(path) == 3
        assert path[0].latitude == 0
        assert path[0].longitude == 0

        assert (path[1].latitude == 2 and path[1].longitude == 8) or (path[1].latitude == 8 and path[1].longitude == 2)
        
        assert path[2].latitude == 10
        assert path[2].longitude == 10


    def testGeofence(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Area" array="false" name="geofence" />
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <constraint type="Geofence" variable="geofence">
                            <action name="straight">
                                <left reference="uav" />
                                <argument reference="point" />
                            </action>
                        </constraint>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 3,
                    "longitude": 3
                  }
                },
                "point" :
                {
                  "latitude": 7,
                  "longitude": 7
                },
                "geofence": {
                    "boundingBox": [
                      {
                        "latitude": 0,
                        "longitude": 0
                      },
                      {
                        "latitude": 0,
                        "longitude": 5
                      },
                      {
                        "latitude": 6,
                        "longitude": 5
                      },
                      {
                        "latitude": 6,
                        "longitude": 10
                      },
                      {
                        "latitude": 10,
                        "longitude": 10
                      },
                      {
                        "latitude": 10,
                        "longitude": 0
                      }
                    ]
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plans = operation_plan.drone_plans
        assert len(plans) == 1

        path = plans[0].path
        assert len(path) == 3
        assert path[0].latitude == 3
        assert path[0].longitude == 3
        assert path[1].latitude == 6
        assert path[1].longitude == 5
        assert path[2].latitude == 7
        assert path[2].longitude == 7

        assert path[1].depart_to_here_time.time < path[2].depart_to_here_time.time


    def testTime(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Time" array="false" name="afterTime" />
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <constraint type="StartTime" variable="afterTime">
                            <action name="straight">
                                <left reference="uav" />
                                <argument reference="point" />
                            </action>
                        </constraint>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "afterTime": {
                    "time": "2120-12-25T12:15+01:00"
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plans = operation_plan.drone_plans
        
        assert len(plans) == 1

        path = plans[0].path
        assert len(path) == 2
        assert path[0].latitude == 0
        assert path[0].longitude == 0
        assert path[0].depart_to_here_time.time == program.arguments['afterTime'].time
        assert path[1].latitude == 10
        assert path[1].longitude == 10
        
        end_time = program.arguments['afterTime'].time + datetime.timedelta(seconds = 141421)
        assert plans[0].get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plans[0].get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

    def testExternalNoFlyZone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))

        no_fly_zone = Area()
        no_fly_zone.boundingBox.append(NavigationPoint(2,2))
        no_fly_zone.boundingBox.append(NavigationPoint(2,8))
        no_fly_zone.boundingBox.append(NavigationPoint(8,8))
        no_fly_zone.boundingBox.append(NavigationPoint(8,2))

        operation_plan = DefaultPlanner().plan(program, [no_fly_zone])
        plans = operation_plan.drone_plans
        assert len(plans) == 1

        path = plans[0].path
        assert len(path) == 3
        assert path[0].latitude == 0
        assert path[0].longitude == 0

        assert (path[1].latitude == 2 and path[1].longitude == 8) or (path[1].latitude == 8 and path[1].longitude == 2)
        
        assert path[2].latitude == 10
        assert path[2].longitude == 10

    def testMultipleExternalNoFlyZone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))

        no_fly_zones = []
        no_fly_zone = Area()
        no_fly_zone.boundingBox.append(NavigationPoint(2,2))
        no_fly_zone.boundingBox.append(NavigationPoint(2,8))
        no_fly_zone.boundingBox.append(NavigationPoint(8,2))
        no_fly_zones.append(no_fly_zone)

        no_fly_zone = Area()
        no_fly_zone.boundingBox.append(NavigationPoint(2,8))
        no_fly_zone.boundingBox.append(NavigationPoint(8,8))
        no_fly_zone.boundingBox.append(NavigationPoint(8,2))
        no_fly_zones.append(no_fly_zone)

        operation_plan = DefaultPlanner().plan(program, no_fly_zones)
        plans = operation_plan.drone_plans
        assert len(plans) == 1

        path = plans[0].path
        assert len(path) == 3
        assert path[0].latitude == 0
        assert path[0].longitude == 0

        assert (path[1].latitude == 2 and path[1].longitude == 8) or (path[1].latitude == 8 and path[1].longitude == 2)
        
        assert path[2].latitude == 10
        assert path[2].longitude == 10

    def testReplan(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)

        plans = operation_plan.drone_plans
        assert len(plans) == 1

        path = plans[0].path
        assert len(path) == 2
        assert path[0].latitude == 0
        assert path[0].longitude == 0
        assert path[1].latitude == 10
        assert path[1].longitude == 10

        no_fly_zone = Area()
        no_fly_zone.boundingBox.append(NavigationPoint(2,2))
        no_fly_zone.boundingBox.append(NavigationPoint(2,8))
        no_fly_zone.boundingBox.append(NavigationPoint(8,8))
        no_fly_zone.boundingBox.append(NavigationPoint(8,2))


        re_planned_program = DefaultPlanner().re_plan([program], [no_fly_zone])
        assert len(re_planned_program) == 1
        plans = re_planned_program[0].operation_plan.drone_plans
        assert len(plans) == 1

        path = plans[0].path
        assert len(path) == 3
        assert path[0].latitude == 0
        assert path[0].longitude == 0

        assert (path[1].latitude == 2 and path[1].longitude == 8) or (path[1].latitude == 8 and path[1].longitude == 2)
        
        assert path[2].latitude == 10
        assert path[2].longitude == 10


    def testPatrolAction(self):
        xml = """
            <operation name="opTest">
                <action_definition name="patrol">
                    <parameter type="Area" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="Area" array="false" name="patrol_area" />
                <mission>
                    <sequential>
                        <action name="patrol">
                            <left reference="uav" />
                            <argument reference="patrol_area" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 10,
                    "longitude": 0
                  }
                },
              "patrol_area" : {
                "boundingBox" : [
                    {
                      "latitude": 0,
                      "longitude": 0
                    },
                    {
                      "latitude": 0,
                      "longitude": 10
                    },
                    {
                      "latitude": 10,
                      "longitude": 10
                    },
                    {
                      "latitude": 10,
                      "longitude": 0
                    }
                ]
              }
                
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        
        operation_plan = DefaultPlanner().plan(program)
        plans = operation_plan.drone_plans
        assert len(plans) == 1

        # A path it is a rich collection of waypoints:
        # - latitude, longitude: latitude and longitude
        # - depart_to_here_time: When to start the navigation to this point
        plan = plans[0]

        assert len(plan.path) == 6
        assert plan.path[0].latitude == 10
        assert plan.path[0].longitude == 0
        assert plan.path[1].latitude == 0
        assert plan.path[1].longitude == 0
        assert plan.path[2].latitude == 0
        assert plan.path[2].longitude == 10
        assert plan.path[3].latitude == 10
        assert plan.path[3].longitude == 10
        assert plan.path[4].latitude == 10
        assert plan.path[4].longitude == 0
        assert plan.path[5].latitude == 0
        assert plan.path[5].longitude == 0

    def testPatrolAndStraightAction(self):
        xml = """
            <operation name="opTest">
                <action-definition name="patrol">
                    <parameter type="Area" array="false" />
                </action-definition>
                <action-definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action-definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="Area" array="false" name="patrol_area" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="patrol">
                            <left reference="uav" />
                            <argument reference="patrol_area" />
                        </action>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 10,
                    "longitude": 0
                  }
                },
              "patrol_area" : {
                "boundingBox" : [
                    {
                      "latitude": 0,
                      "longitude": 0
                    },
                    {
                      "latitude": 0,
                      "longitude": 10
                    },
                    {
                      "latitude": 10,
                      "longitude": 10
                    },
                    {
                      "latitude": 10,
                      "longitude": 0
                    }
                ]
              },
              "point" : {
                "latitude" : 20,
                "longitude" : 20
              }
                
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        
        operation_plan = DefaultPlanner().plan(program)
        plans = operation_plan.drone_plans
        assert len(plans) == 1

        # A path it is a rich collection of waypoints:
        # - latitude, longitude: latitude and longitude
        # - depart_to_here_time: When to start the navigation to this point
        plan = plans[0]

        assert len(plan.path) == 7
        assert plan.path[0].latitude == 10
        assert plan.path[0].longitude == 0
        assert plan.path[1].latitude == 0
        assert plan.path[1].longitude == 0
        assert plan.path[2].latitude == 0
        assert plan.path[2].longitude == 10
        assert plan.path[3].latitude == 10
        assert plan.path[3].longitude == 10
        assert plan.path[4].latitude == 10
        assert plan.path[4].longitude == 0
        assert plan.path[5].latitude == 0
        assert plan.path[5].longitude == 0
        assert plan.path[6].latitude == 20
        assert plan.path[6].longitude == 20