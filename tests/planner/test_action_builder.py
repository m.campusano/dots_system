from dots.ast_parser import DotsXML
from dots.core.program import Program
from dots.core.type import UndefinedDrone
from dots.planner.action import ActionBuilder, Straight, Patrol, \
                                CoordinationAction, SequentialAction, PrioritizedAction, ParallelAction

import json

class TestActionBuilder:

    def testSimpleAction(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 0,
                  "longitude": 0
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        
        builder = ActionBuilder(program)
        builder.build()
        actions = builder.actions[0].actions
        assert len(actions) == 1

        action = actions[0]
        assert type(action) is Straight

        assert action.drones[0] == program.arguments['uav']
        assert action.navigation_point == program.arguments['point']

    def testConstraintBody(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="Location" array="true" />
                </action_definition>
                <parameter type="Area" array="false" name="geofence" />
                <parameter type="Area" array="false" name="noFlyZone" />
                <parameter type="Time" array="false" name="beforeTime" />
                <parameter type="Time" array="false" name="afterTime" />
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <constraint type="Geofence" variable="geofence">
                            <constraint type="NoFlyZone" variable="noFlyZone">
                                <constraint type="EndTime" variable="beforeTime">
                                    <constraint type="StartTime" variable="afterTime">
                                        <action name="straight">
                                            <left reference="uav" />
                                            <argument reference="point" />
                                        </action>
                                    </constraint>
                                </constraint>
                            </constraint>
                        </constraint>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
              "geofence": {
                "boundingBox": [
                  {
                    "latitude": 2,
                    "longitude": 2
                  }
                ]
               },
              "noFlyZone": {
                "boundingBox": [
                  {
                    "latitude": 3,
                    "longitude": 3
                  }
                ]
               },
              "afterTime": {
                "time": "2020-12-25T12:15+01:00"
              },
              "beforeTime": {
                "time": "2020-12-30T12:15+01:00"
              }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        
        builder = ActionBuilder(program)
        builder.build()
        actions = builder.actions[0].actions
        assert len(actions) == 2

        action1 = actions[0]
        action2 = actions[1]

        assert len(action1.modifiers) == 4
        assert len(action1.get_geofence_constraints()) == 1
        assert len(action1.get_no_fly_zone_constraints()) == 1
        assert len(action1.get_after_time_constraints()) == 1
        assert len(action1.get_before_time_constraints()) == 1

        assert action1.get_geofences()[0] == program.arguments['geofence']
        assert action1.get_no_fly_zones()[0] == program.arguments['noFlyZone']
        assert action1.get_lastest_start_time() == program.arguments['afterTime']
        assert action1.get_earliest_end_time() == program.arguments['beforeTime']

        assert len(action2.modifiers) == 0

    def testEmptyCoordination(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 0,
                  "longitude": 0
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        
        builder = ActionBuilder(program)
        builder.build()
        actions = builder.actions
        assert len(actions) == 1

        action = actions[0]
        assert type(action) is SequentialAction
        assert len(action.actions) == 0

    def testEmptyCoordinations(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <sequential>
                        </sequential>
                        <prioritized>
                        </prioritized>
                        <parallel>
                        </parallel>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 0,
                  "longitude": 0
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        
        builder = ActionBuilder(program)
        builder.build()
        actions = builder.actions[0].actions
        assert len(actions) == 3

        action = actions[0]
        assert type(action) is SequentialAction
        assert len(action.actions) == 0

        action = actions[1]
        assert type(action) is PrioritizedAction
        assert len(action.actions) == 0

        action = actions[2]
        assert type(action) is ParallelAction
        assert len(action.actions) == 0

    def testUsingDroneCoordination(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="true" name="uavs" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential drone="uav" drone-list="uavs">
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    }
                ],
              "point" :
                {
                  "latitude": 0,
                  "longitude": 0
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        
        builder = ActionBuilder(program)
        builder.build()
        sequential = builder.actions[0]

        assert sequential.drones == program.arguments['uavs']
        assert sequential.drone_reference_name == 'uav'

        action = sequential.actions[0]
        assert type(action.drones[0]) is UndefinedDrone
        assert action.drones[0].reference_name == 'uav'


    def testParallelLoops(self):
        xml = """
            <operation name="loop">
                <action-definition name="Straight">
                    <parameter type="Location" array="false" />
                </action-definition>
                <parameter type="NavigationPoint" array="true" name="points" />
                <parameter type="Drone" array="true" name="uavs" />
                <mission>
                    <sequential>
                        <for type="parallel" location="point" location-list="points" drone="uav" drone-list="uavs">
                            <action name="Straight">
                                <left reference="uav" />
                                <argument reference="point" />
                            </action>
                        </for>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 1
                      }
                    }
                ],
              "points" : [
                    {
                      "latitude": 1,
                      "longitude": 0
                    },
                    {
                      "latitude": 1,
                      "longitude": 1
                    }
                ]
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        program.apply_syntax_sugar()
        
        builder = ActionBuilder(program)
        builder.build()
        actions = builder.actions[0].actions
        parallel = actions[0]
        assert type(parallel) is ParallelAction

        body = parallel.actions
        assert len(body) == 2

        # first iteration
        first_body = body[0]
        first_body_action = first_body.actions[0]
        assert type(first_body) is SequentialAction
        assert first_body.drones == program.arguments['uavs']
        assert first_body.drone_reference_name == 'uav'
        assert type(first_body_action.drones[0]) is UndefinedDrone
        assert first_body_action.drones[0].reference_name == 'uav'
        assert first_body_action.navigation_point == program.arguments['points'][0]

        # second interation
        second_body = body[1]
        second_body_action = second_body.actions[0]
        assert type(second_body) is SequentialAction
        assert second_body.drones == program.arguments['uavs']
        assert second_body.drone_reference_name == 'uav'
        assert type(first_body_action.drones[0]) is UndefinedDrone
        assert second_body_action.drones[0].reference_name == 'uav'
        assert second_body_action.navigation_point == program.arguments['points'][1]


    def testArrays(self):
        xml = """
            <operation name="loop">
                <action-definition name="Straight">
                    <parameter type="Location" array="false" />
                </action-definition>
                <parameter type="NavigationPoint" array="true" name="points" />
                <parameter type="Drone" array="true" name="uavs" />
                <mission>
                    <sequential>
                        <action name="Straight">
                            <left reference="uavs[0]" />
                            <argument reference="points[0]" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 1
                      }
                    }
                ],
              "points" : [
                    {
                      "latitude": 1,
                      "longitude": 0
                    },
                    {
                      "latitude": 1,
                      "longitude": 1
                    }
                ]
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        program.apply_syntax_sugar()
        
        builder = ActionBuilder(program)
        builder.build()
        action = builder.actions[0].actions[0]
        
        assert type(action) is Straight
        assert action.drones[0] == program.arguments['uavs'][0]
        assert action.navigation_point == program.arguments['points'][0]

    def testArrayStraight(self):
        xml = """
            <operation name="loop">
                <action-definition name="Straight">
                    <parameter type="Location" array="false" />
                </action-definition>
                <parameter type="NavigationPoint" array="false" name="point" />
                <parameter type="Drone" array="true" name="uavs" />
                <mission>
                    <sequential>
                        <action name="Straight">
                            <left reference="uavs" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 1
                      }
                    }
                ],
              "point" : 
                    {
                      "latitude": 1,
                      "longitude": 0
                    }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        program.apply_syntax_sugar()
        
        builder = ActionBuilder(program)
        builder.build()
        parallel = builder.actions[0].actions[0]
        assert type(parallel) is ParallelAction

        body = parallel.actions
        assert len(body) == 2

        # first drone
        first_action = body[0]
        assert type(first_action) is Straight
        assert first_action.drones[0] == program.arguments['uavs'][0]
        assert first_action.navigation_point == program.arguments['point']

        # second drone
        second_action = body[1]
        assert type(second_action) is Straight
        assert second_action.drones[0] == program.arguments['uavs'][1]
        assert second_action.navigation_point == program.arguments['point']

    def testPatrolCall(self):
        xml = """
            <operation name="opTest">
                <action_definition name="patrol">
                    <parameter type="Area" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="Area" array="false" name="patrol_area" />
                <mission>
                    <sequential>
                        <action name="patrol">
                            <left reference="uav" />
                            <argument reference="patrol_area" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
              "patrol_area" : {
                "boundingBox" : [
                    {
                      "latitude": 0,
                      "longitude": 0
                    },
                    {
                      "latitude": 0,
                      "longitude": 1
                    },
                    {
                      "latitude": 1,
                      "longitude": 1
                    },
                    {
                      "latitude": 1,
                      "longitude": 0
                    }
                ]
              }
                
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        
        builder = ActionBuilder(program)
        builder.build()
        actions = builder.actions[0].actions
        assert len(actions) == 1

        action = actions[0]
        assert type(action) is Patrol

        assert action.drones[0] == program.arguments['uav']
        assert action.area == program.arguments['patrol_area']


    def testAreaAsArray(self):
        xml = """
            <operation name="loop">
                <action-definition name="Straight">
                    <parameter type="Location" array="false" />
                </action-definition>
                <parameter type="Area" array="false" name="area" />
                <parameter type="Drone" array="false" name="uav" />
                <mission>
                    <sequential>
                        <action name="Straight">
                            <left reference="uav" />
                            <argument reference="area[1]" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                    {
                      "uuid": "uav",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },

              "area" : {
                "boundingBox" : 
                [
                   {
                      "latitude": 1,
                      "longitude": 0
                    },
                    {
                      "latitude": 1,
                      "longitude": 1
                    }
                ]
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        
        builder = ActionBuilder(program)
        builder.build()
        action = builder.actions[0].actions[0]
        
        assert type(action) is Straight
        assert action.drones[0] == program.arguments['uav']
        assert action.navigation_point.latitude == program.arguments['area'][1].latitude
        assert action.navigation_point.longitude == program.arguments['area'][1].longitude