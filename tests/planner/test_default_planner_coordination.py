from dots.ast_parser import DotsXML
from dots.core.program import Program
from dots.planner import DefaultPlanner

import json
import datetime

class TestDefaultPlannerCoordination:

    def testSequentialSameDrone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point1" />
                        </action>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point2" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan = operation_plan.drone_plans[0]
        assert plan.drone == program.arguments['uav']
        
        start_time = DefaultPlanner.get_default_time().time
        next_depart = start_time
        assert plan.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        # Same as before, we consider take off position as an odd point, it takes no time to arrive there
        assert plan.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        next_depart = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.path[2].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[2].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))


    def testSequentialMultipleDrone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav1" />
                <parameter type="Drone" array="false" name="uav2" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav1" />
                            <argument reference="point1" />
                        </action>
                        <action name="straight">
                            <left reference="uav2" />
                            <argument reference="point2" />
                        </action>
                        <action name="straight">
                            <left reference="uav1" />
                            <argument reference="point2" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav1":
                {
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "uav2":
                {
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan1 = operation_plan.drone_plans[0]
        plan2 = operation_plan.drone_plans[1]
        default_start_time = DefaultPlanner.get_default_time()

        start_time = default_start_time.time
        next_depart = start_time
        assert plan1.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan1.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        next_depart = next_depart + datetime.timedelta(seconds = 141421)*3

        assert plan1.path[2].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[2].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan1.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan1.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan1.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan1.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

        
        start_time = default_start_time.time + datetime.timedelta(seconds = 141421)
        next_depart = start_time
        assert plan2.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan2.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan2.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan2.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)*2

        assert plan2.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan2.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan2.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan2.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))


    def testParallelSameDrone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <parallel>
                            <action name="straight">
                                <left reference="uav" />
                                <argument reference="point1" />
                            </action>
                            <action name="straight">
                                <left reference="uav" />
                                <argument reference="point2" />
                            </action>
                        </parallel>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan = operation_plan.drone_plans[0]
        assert plan.drone == program.arguments['uav']
        
        start_time = DefaultPlanner.get_default_time().time
        next_depart = start_time
        assert plan.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        # Same as before, we consider take off position as an odd point, it takes no time to arrive there
        assert plan.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        next_depart = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.path[2].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[2].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

    def testParallelMultipleDrone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav1" />
                <parameter type="Drone" array="false" name="uav2" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <parallel>
                            <action name="straight">
                                <left reference="uav1" />
                                <argument reference="point1" />
                            </action>
                            <action name="straight">
                                <left reference="uav2" />
                                <argument reference="point2" />
                            </action>
                            <action name="straight">
                                <left reference="uav1" />
                                <argument reference="point2" />
                            </action>
                        </parallel>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav1":
                {
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "uav2":
                {
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan1 = operation_plan.drone_plans[0]
        plan2 = operation_plan.drone_plans[1]
        default_start_time = DefaultPlanner.get_default_time()

        start_time = default_start_time.time
        next_depart = start_time
        assert plan1.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan1.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        next_depart = next_depart + datetime.timedelta(seconds = 141421)

        assert plan1.path[2].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[2].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan1.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan1.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan1.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan1.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

        
        start_time = default_start_time.time
        next_depart = start_time

        assert plan2.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan2.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan2.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan2.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)*2

        assert plan2.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan2.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan2.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan2.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))


    def testPrioritizedSameDrone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <prioritized>
                            <action name="straight">
                                <left reference="uav" />
                                <argument reference="point1" />
                            </action>
                            <action name="straight">
                                <left reference="uav" />
                                <argument reference="point2" />
                            </action>
                        </prioritized>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan = operation_plan.drone_plans[0]
        assert plan.drone == program.arguments['uav']
        
        start_time = DefaultPlanner.get_default_time().time
        next_depart = start_time
        assert plan.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        # Same as before, we consider take off position as an odd point, it takes no time to arrive there
        assert plan.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        next_depart = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.path[2].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[2].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))


    def testSequentialMultipleDrone1(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav1" />
                <parameter type="Drone" array="false" name="uav2" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <prioritized>
                            <action name="straight">
                                <left reference="uav1" />
                                <argument reference="point1" />
                            </action>
                            <action name="straight">
                                <left reference="uav1" />
                                <argument reference="point2" />
                            </action>
                            <action name="straight">
                                <left reference="uav2" />
                                <argument reference="point2" />
                            </action>
                        </prioritized>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav1":
                {
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "uav2":
                {
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan1 = operation_plan.drone_plans[0]
        plan2 = operation_plan.drone_plans[1]
        default_start_time = DefaultPlanner.get_default_time()

        start_time = default_start_time.time
        next_depart = start_time
        assert plan1.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan1.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        next_depart = next_depart + datetime.timedelta(seconds = 141421)

        assert plan1.path[2].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[2].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan1.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan1.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan1.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan1.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

        
        start_time = default_start_time.time + datetime.timedelta(seconds = 141421)
        next_depart = start_time
        assert plan2.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan2.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan2.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan2.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)*2

        assert plan2.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan2.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan2.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan2.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

    def testSequentialMultipleDrone2(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav1" />
                <parameter type="Drone" array="false" name="uav2" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <prioritized>
                            <action name="straight">
                                <left reference="uav1" />
                                <argument reference="point1" />
                            </action>
                            <action name="straight">
                                <left reference="uav2" />
                                <argument reference="point2" />
                            </action>
                            <action name="straight">
                                <left reference="uav1" />
                                <argument reference="point2" />
                            </action>
                        </prioritized>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav1":
                {
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "uav2":
                {
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan1 = operation_plan.drone_plans[0]
        plan2 = operation_plan.drone_plans[1]
        default_start_time = DefaultPlanner.get_default_time()

        start_time = default_start_time.time
        next_depart = start_time
        assert plan1.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan1.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        next_depart = next_depart + datetime.timedelta(seconds = 141421)

        assert plan1.path[2].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[2].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan1.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan1.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan1.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan1.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

        
        start_time = default_start_time.time
        next_depart = start_time
        assert plan2.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan2.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan2.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan2.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)*2

        assert plan2.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan2.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan2.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan2.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

    def testSequentialUsingDrone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="true" name="uavs" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential drone="uav" drone-list="uavs">
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point1" />
                        </action>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point2" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    }
                ],
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan = operation_plan.drone_plans[0]

        assert plan.drone == program.arguments['uavs'][0]

        default_start_time = DefaultPlanner.get_default_time()
        start_time = default_start_time.time
        next_depart = start_time
        assert plan.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        next_depart = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.path[2].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[2].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

    def testEmbeddedCoordinations(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav1" />
                <parameter type="Drone" array="false" name="uav2" />
                <parameter type="Drone" array="false" name="uav3" />
                <parameter type="Drone" array="false" name="uav4" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <parallel>
                            <sequential>
                                <action name="straight">
                                    <left reference="uav1" />
                                    <argument reference="point1" />
                                </action>
                                <action name="straight">
                                    <left reference="uav2" />
                                    <argument reference="point1" />
                                </action>
                            </sequential>
                            <sequential>
                                <action name="straight">
                                    <left reference="uav3" />
                                    <argument reference="point2" />
                                </action>
                                <action name="straight">
                                    <left reference="uav4" />
                                    <argument reference="point2" />
                                </action>
                            </sequential>
                        </parallel>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
                "uav1":{
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "uav2":{
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "uav3":{
                  "uuid": "uav3",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "uav4":{
                  "uuid": "uav4",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point1" :
                {
                  "latitude": 0,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 10,
                  "longitude": 0
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan1 = operation_plan.drone_plan_from_id('uav1')
        plan2 = operation_plan.drone_plan_from_id('uav2')
        plan3 = operation_plan.drone_plan_from_id('uav3')
        plan4 = operation_plan.drone_plan_from_id('uav4')

        default_start_time = DefaultPlanner.get_default_time()
        start_time = default_start_time.time
        next_depart = start_time
        assert plan1.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan1.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan1.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan1.path[1].latitude == 0
        assert plan1.path[1].longitude == 10
        
        assert plan3.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan3.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan3.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan3.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan3.path[1].latitude == 10
        assert plan3.path[1].longitude == 0

        next_depart = next_depart + datetime.timedelta(seconds = 100000)

        assert plan2.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan2.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan2.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan2.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan2.path[1].latitude == 0
        assert plan2.path[1].longitude == 10

        assert plan4.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan4.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan4.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan4.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan4.path[1].latitude == 10
        assert plan4.path[1].longitude == 0

    def testParallelUsingDrone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="true" name="uavs" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <parallel>
                            <sequential drone="uav" drone-list="uavs">
                                <action name="straight">
                                    <left reference="uav" />
                                    <argument reference="point1" />
                                </action>
                            </sequential>
                            <sequential drone="uav" drone-list="uavs">
                                <action name="straight">
                                    <left reference="uav" />
                                    <argument reference="point2" />
                                </action>
                            </sequential>
                        </parallel>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 100,
                        "longitude": 100
                      }
                    }
                ],
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 110,
                  "longitude": 110
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan = operation_plan.drone_plans[0]
        assert plan.drone == program.arguments['uavs'][0]

        default_start_time = DefaultPlanner.get_default_time()
        start_time = default_start_time.time
        next_depart = start_time
        assert plan.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))
        plan = operation_plan.drone_plans[1]

        assert plan.drone == program.arguments['uavs'][1]

        default_start_time = DefaultPlanner.get_default_time()
        start_time = default_start_time.time
        next_depart = start_time
        assert plan.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

    def testParallelUsingLessDrones(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="true" name="uavs" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <parameter type="NavigationPoint" array="false" name="point3" />
                <mission>
                    <sequential>
                        <parallel>
                            <sequential drone="uav" drone-list="uavs">
                                <action name="straight">
                                    <left reference="uav" />
                                    <argument reference="point1" />
                                </action>
                            </sequential>
                            <sequential drone="uav" drone-list="uavs">
                                <action name="straight">
                                    <left reference="uav" />
                                    <argument reference="point2" />
                                </action>
                            </sequential>
                            <sequential drone="uav" drone-list="uavs">
                                <action name="straight">
                                    <left reference="uav" />
                                    <argument reference="point3" />
                                </action>
                            </sequential>
                        </parallel>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 100,
                        "longitude": 100
                      }
                    }
                ],
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 110,
                  "longitude": 110
                },
                "point3" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan = operation_plan.drone_plans[0]
        assert plan.drone == program.arguments['uavs'][0]

        default_start_time = DefaultPlanner.get_default_time()
        start_time = default_start_time.time
        next_depart = start_time
        assert plan.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))


        next_depart = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.path[2].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[2].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

        plan = operation_plan.drone_plans[1]
        assert plan.drone == program.arguments['uavs'][1]

        default_start_time = DefaultPlanner.get_default_time()
        start_time = default_start_time.time
        next_depart = start_time
        assert plan.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

    def testAdvancedParallelUsingDrones(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="true" name="uavs" />
                <parameter type="NavigationPoint" array="false" name="point1" />
                <parameter type="NavigationPoint" array="false" name="point2" />
                <mission>
                    <sequential>
                        <parallel>
                            <sequential drone="uav" drone-list="uavs">
                                <action name="straight">
                                    <left reference="uav" />
                                    <argument reference="point1" />
                                </action>
                                <action name="straight">
                                    <left reference="uav" />
                                    <argument reference="point2" />
                                </action>
                            </sequential>

                            <sequential drone="uav" drone-list="uavs">
                                <action name="straight">
                                    <left reference="uav" />
                                    <argument reference="point1" />
                                </action>
                            </sequential>
                        </parallel>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    }
                ],
                "point1" :
                {
                  "latitude": 10,
                  "longitude": 10
                },
                "point2" :
                {
                  "latitude": 20,
                  "longitude": 20
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)