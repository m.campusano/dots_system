from dots.ast_parser import DotsXML
from dots.core.program import Program
from dots.planner import DefaultPlanner

import json
import datetime

class TestDefaultPlannerCoordination:

    def testParallelLoop(self):
        xml = """
            <operation name="visit_nursing_homes">
                <action-definition name="Straight">
                    <parameter type="Location" array="false" />
                </action-definition>
                <parameter type="Drone" array="true" name="uavs" />
                <parameter type="NavigationPoint" array="true" name="points" />
                <mission>
                    <sequential>
                        <for type="parallel" location="point" location-list="points" drone="uav" drone-list="uavs">
                            <action name="Straight">
                                <left reference="uav" />
                                <argument reference="point" />
                            </action>
                        </for>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uavs": [
                    {
                      "uuid": "uav1",
                      "takeOffPosition": {
                        "latitude": 0,
                        "longitude": 0
                      }
                    },
                    {
                      "uuid": "uav2",
                      "takeOffPosition": {
                        "latitude": 100,
                        "longitude": 100
                      }
                    }
                ],
                "points" : [ 
                    {
                      "latitude": 10,
                      "longitude": 10
                    },
                    {
                      "latitude": 110,
                      "longitude": 110
                    },
                    {
                      "latitude": 20,
                      "longitude": 20
                    }
                ]
            }
            """

        operation = DotsXML().parse(xml)
        program = Program.create_with_arguments(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)
        plan = operation_plan.drone_plans[0]
        assert plan.drone == program.arguments['uavs'][0]

        default_start_time = DefaultPlanner.get_default_time()
        start_time = default_start_time.time
        next_depart = start_time
        assert plan.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))


        next_depart = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.path[2].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[2].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

        plan = operation_plan.drone_plans[1]
        assert plan.drone == program.arguments['uavs'][1]

        default_start_time = DefaultPlanner.get_default_time()
        start_time = default_start_time.time
        next_depart = start_time
        assert plan.path[0].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[0].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time < (next_depart + datetime.timedelta(seconds = 5))
        assert plan.path[1].depart_to_here_time.time > (next_depart - datetime.timedelta(seconds = 5))

        end_time = next_depart + datetime.timedelta(seconds = 141421)

        assert plan.get_start_time().time < (start_time + datetime.timedelta(seconds = 5))
        assert plan.get_start_time().time > (start_time - datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time < (end_time + datetime.timedelta(seconds = 5))
        assert plan.get_end_time().time > (end_time - datetime.timedelta(seconds = 5))

    # def testAdvancedParallelLoop(self):
    #     xml = """
    #         <operation name="visit_nursing_homes">
    #             <action-definition name="Straight">
    #                 <parameter type="Location" array="false" />
    #             </action-definition>
    #             <parameter type="Drone" array="true" name="uavs" />
    #             <parameter type="NavigationPoint" array="true" name="nursing_homes" />
    #             <parameter type="NavigationPoint" array="false" name="hospital" />
    #             <mission>
    #                 <sequential>
    #                     <for type="parallel" location="nursing_home" location-list="nursing_homes" drone="uav" drone-list="uavs">
    #                         <action name="Straight">
    #                             <left reference="uav" />
    #                             <argument reference="nursing_home" />
    #                         </action>
                            
    #                         <action name="Straight">
    #                             <left reference="uav" />
    #                             <argument reference="hospital" />
    #                         </action>
    #                     </for>
    #                 </sequential>
    #             </mission>
    #         </operation>
    #         """
    #     arguments = """
    #         {
    #         "uavs": [
    #                 {
    #                   "uuid": "uav1",
    #                   "takeOffPosition": {
    #                     "latitude": 10,
    #                     "longitude": 10
    #                   }
    #                 },
    #                 {
    #                   "uuid": "uav2",
    #                   "takeOffPosition": {
    #                     "latitude": -10,
    #                     "longitude": -10
    #                   }
    #                 }
    #             ],
    #         "nursing_homes": [
    #             {
    #                   "latitude": 10,
    #                   "longitude": 10
    #                 },
    #                 {
    #                   "latitude": -10,
    #                   "longitude": -10
    #                 },
    #                 {
    #                   "latitude": 20,
    #                   "longitude": 20
    #                 }
    #         ],
    #         "hospital": {
    #             "latitude": "0", 
    #             "longitude": "0"
    #         }
    #     }
    #     """

    #     operation = DotsXML().parse(xml)
    #     program = Program.create_with_arguments(operation, json.loads(arguments))
    #     operation_plan = DefaultPlanner().plan(program)

    #     ## Just testing no errors ##