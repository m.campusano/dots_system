from dots.ast_parser import DotsXML
from dots.core.program import Program
from dots.core.type import Area, NavigationPoint
from dots.planner.action import ActionBuilder
from dots.planner import DefaultPlanner
from dots.utils import ConflictDetector

import json

class TestConflictDetector:

    def testOutsideNoFlyZone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)

        no_fly_zones = []
        no_fly_zone = Area()
        no_fly_zone.boundingBox.append(NavigationPoint(11,11))
        no_fly_zone.boundingBox.append(NavigationPoint(11,12))
        no_fly_zone.boundingBox.append(NavigationPoint(12,12))
        no_fly_zone.boundingBox.append(NavigationPoint(12,11))
        no_fly_zones.append(no_fly_zone)

        conflict_detector = ConflictDetector()
        assert conflict_detector.plan_in_no_fly_zones(operation_plan, no_fly_zones)

    def testInNoFlyZone(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments = """
            {
              "uav":
                {
                  "uuid": "uav",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
                "point" :
                {
                  "latitude": 10,
                  "longitude": 10
                }
            }
            """

        operation = DotsXML().parse(xml)
        program = Program(operation, json.loads(arguments))
        operation_plan = DefaultPlanner().plan(program)

        no_fly_zones = []
        no_fly_zone = Area()
        no_fly_zone.boundingBox.append(NavigationPoint(2,2))
        no_fly_zone.boundingBox.append(NavigationPoint(2,8))
        no_fly_zone.boundingBox.append(NavigationPoint(8,8))
        no_fly_zone.boundingBox.append(NavigationPoint(8,2))
        no_fly_zones.append(no_fly_zone)

        conflict_detector = ConflictDetector()
        assert conflict_detector.plan_in_no_fly_zones(operation_plan, no_fly_zones)