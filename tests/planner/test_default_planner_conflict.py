from dots.ast_parser import DotsXML
from dots.core.program import Program
from dots.core.type import Area, NavigationPoint
from dots.planner import DefaultPlanner

import json
import datetime

#TODO: planner cannot use the same drone at the same time

class TestDefaultPlannerConflict:

    def testNoConflictSameTemplate(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments1 = """
            {
              "uav":
                {
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 5,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 5,
                  "longitude": 5
                }
            }
            """
        arguments2 = """
            {
              "uav":
                {
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 0,
                  "longitude": 5
                }
            }
            """

        operation = DotsXML().parse(xml)
        program1 = Program(operation, json.loads(arguments1))
        program1.add_new_plan(DefaultPlanner().plan(program1))
        
        program2 = Program(operation, json.loads(arguments2))
        program2.add_new_plan(DefaultPlanner().plan(program2, [], [program1]))
        
        #program1 and program2 plans without problems, they should move freely and start around the same time
        plan1 = program1.operation_plan.drone_plans[0]
        plan2 = program2.operation_plan.drone_plans[0]

        assert len(plan1.path) == 2
        assert len(plan2.path) == 2

        assert plan2.get_start_time().time - plan1.get_start_time().time < datetime.timedelta(seconds = 10)


    def testConflictSameTemplate(self):
        xml = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments1 = """
            {
              "uav":
                {
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 5,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 5,
                  "longitude": 5
                }
            }
            """
        arguments2 = """
            {
              "uav":
                {
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 2
                  }
                },
              "point" :
                {
                  "latitude": 10,
                  "longitude": 2
                }
            }
            """

        operation = DotsXML().parse(xml)
        program1 = Program(operation, json.loads(arguments1))
        program1.add_new_plan(DefaultPlanner().plan(program1))
        
        program2 = Program(operation, json.loads(arguments2))
        program2.add_new_plan(DefaultPlanner().plan(program2, [], [program1]))
        
        #program2 traverse throught program1, in this simple planner, program2 should plan around program1
        plan1 = program1.operation_plan.drone_plans[0]
        plan2 = program2.operation_plan.drone_plans[0]

        assert len(plan1.path) == 2
        assert len(plan2.path) == 3

        assert plan2.get_start_time().time - plan1.get_start_time().time < datetime.timedelta(seconds = 10)


    def testNoConflictDifferentTemplate(self):
        xml1 = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        xml2 = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments1 = """
            {
              "uav":
                {
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 5,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 5,
                  "longitude": 5
                }
            }
            """
        arguments2 = """
            {
              "uav":
                {
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 5
                  }
                },
              "point" :
                {
                  "latitude": 3,
                  "longitude": 5
                }
            }
            """

        operation = DotsXML().parse(xml1)
        program1 = Program(operation, json.loads(arguments1))
        program1.add_new_plan(DefaultPlanner().plan(program1))
        
        operation = DotsXML().parse(xml1)
        program2 = Program(operation, json.loads(arguments2))
        program2.add_new_plan(DefaultPlanner().plan(program2, [], [program1]))
        
        plan1 = program1.operation_plan.drone_plans[0]
        plan2 = program2.operation_plan.drone_plans[0]

        assert len(plan1.path) == 2
        assert len(plan2.path) == 2

        assert plan2.get_start_time().time - plan1.get_start_time().time < datetime.timedelta(seconds = 10)

    def testConflictDifferentTemplate(self):
        xml1 = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        xml2 = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments1 = """
            {
              "uav":
                {
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 5,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 5,
                  "longitude": 5
                }
            }
            """
        arguments2 = """
            {
              "uav":
                {
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 2
                  }
                },
              "point" :
                {
                  "latitude": 10,
                  "longitude": 2
                }
            }
            """

        operation = DotsXML().parse(xml1)
        program1 = Program(operation, json.loads(arguments1))
        program1.add_new_plan(DefaultPlanner().plan(program1))
        
        operation = DotsXML().parse(xml1)
        program2 = Program(operation, json.loads(arguments2))
        program2.add_new_plan(DefaultPlanner().plan(program2, [], [program1]))
        
        # Similar as conflict with same template
        plan1 = program1.operation_plan.drone_plans[0]
        plan2 = program2.operation_plan.drone_plans[0]

        assert len(plan1.path) == 2
        assert len(plan2.path) == 3

        assert plan2.get_start_time().time - plan1.get_start_time().time < datetime.timedelta(seconds = 10)

    def testNoConflictDifferentTimes(self):
        xml1 = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Time" array="false" name="afterTime" />
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <constraint type="StartTime" variable="afterTime">
                            <action name="straight">
                                <left reference="uav" />
                                <argument reference="point" />
                            </action>
                        </constraint>
                    </sequential>
                </mission>
            </operation>
            """
        xml2 = """
            <operation name="opTest">
                <action_definition name="straight">
                    <parameter type="NavigationPoint" array="false" />
                </action_definition>
                <parameter type="Drone" array="false" name="uav" />
                <parameter type="NavigationPoint" array="false" name="point" />
                <mission>
                    <sequential>
                        <action name="straight">
                            <left reference="uav" />
                            <argument reference="point" />
                        </action>
                    </sequential>
                </mission>
            </operation>
            """
        arguments1 = """
            {
              "uav":
                {
                  "uuid": "uav1",
                  "takeOffPosition": {
                    "latitude": 5,
                    "longitude": 0
                  }
                },
              "point" :
                {
                  "latitude": 5,
                  "longitude": 5
                },
                "afterTime": {
                    "time": "2120-12-25T12:15+01:00"
                }
            }
            """
        arguments2 = """
            {
              "uav":
                {
                  "uuid": "uav2",
                  "takeOffPosition": {
                    "latitude": 0,
                    "longitude": 2
                  }
                },
              "point" :
                {
                  "latitude": 10,
                  "longitude": 2
                },
                "afterTime": {
                    "time": "2121-12-25T12:15+01:00"
                }
            }
            """

        operation = DotsXML().parse(xml1)
        program1 = Program(operation, json.loads(arguments1))
        program1.add_new_plan(DefaultPlanner().plan(program1))
        
        operation = DotsXML().parse(xml1)
        program2 = Program(operation, json.loads(arguments2))
        program2.add_new_plan(DefaultPlanner().plan(program2, [], [program1]))
        
        # plan should be the same as no conflict, but starting at different times
        plan1 = program1.operation_plan.drone_plans[0]
        plan2 = program2.operation_plan.drone_plans[0]

        assert len(plan1.path) == 2
        assert len(plan2.path) == 2

        assert plan1.get_start_time().time - program1.arguments['afterTime'].time < datetime.timedelta(seconds = 1)
        assert plan2.get_start_time().time - program2.arguments['afterTime'].time < datetime.timedelta(seconds = 1)