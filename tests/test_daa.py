import pytest

from dots.daa import FlightPoint, FlightPointCollection


class TestDAA:

    def testMissingFlights(self):
        pA = FlightPoint()
        pA.hex = 'A' 
        pB = FlightPoint()
        pB.hex = 'B'
        pD = FlightPoint()
        pD.hex = 'D'
        pE = FlightPoint()
        pE.hex = 'E'

        old_points = FlightPointCollection([pA,pB, pD, pE])

        pA = FlightPoint()
        pA.hex = 'A'
        pC = FlightPoint()
        pC.hex = 'C'
        pD = FlightPoint()
        pD.hex = 'D'
        new_points = FlightPointCollection([pA,pC,pD])

        pA = FlightPoint()
        pA.hex = 'A' 
        pB = FlightPoint()
        pB.hex = 'B'
        pC = FlightPoint()
        pC.hex = 'C'
        pD = FlightPoint()
        pD.hex = 'D'
        pE = FlightPoint()
        pE.hex = 'E'

        missing_points = old_points.missing_points(new_points)
        assert(not pA in missing_points)
        assert(pB in missing_points)
        assert(not pC in missing_points)
        assert(not pD in missing_points)
        assert(pE in missing_points)