from setuptools import find_packages, setup

setup(
	name='dots',
	packages=find_packages(include=['dots']),
	version='0.1.0',
	description='DOTS System',
	author='Miguel Campusano',
	licence='TODO',
	install_requires=[],
	setup_requires=['pytest-runner'],
	tests_require=['pytest'],
	test_suite='tests'
)