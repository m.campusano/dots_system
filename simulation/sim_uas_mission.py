#!/usr/bin/env python3

import time
import requests
import socketio

from sim_drone import SimDrone

class SimUasMission:

    def __init__(self):
        # self.drones = ['a1c4e6e7-0816-4d05-a8db-beb304cfb96f', '35931ace-3e83-494a-962a-27a9d38a8bc7']
        self.drones = ['A', 'B', 'C', 'D']
        self.rate = 10
        self.sim_drones = []
        self.getting_plans = False

        self.sio = socketio.Client()
        self.sio.register_namespace(MonitorResource(self))
        self.sio.connect('http://localhost:8080', namespaces=['/monitor'])

        self.get_plans()        

    def get_plans(self):
        self.getting_plans = True
        self.sim_drones = []
        for drone in self.drones:
            r = requests.get(f'http://localhost:8080/drone/{drone}/plan')
            print(r)
            self.sim_drones.append(SimDrone(drone, r.json(), self.sio))
        self.getting_plans = False

    def run(self):
        time.sleep(1)
        finished = False
        while not finished:
            if not self.getting_plans:
                finished = True
                for sim_drone in self.sim_drones:
                    if not sim_drone.arrived_to_destination():
                        finished = False
                        sim_drone.step(self.sleep_time())
            self.sleep()

    def sleep_time(self):
        return 1/self.rate

    def sleep(self):
        time.sleep(self.sleep_time())

class MonitorResource(socketio.ClientNamespace):

    def __init__(self, sim):
        super().__init__('/monitor')
        self.sim = sim

    def on_mission(self, status):
        if status['replanned']:
            sim.get_plans()



if __name__ == "__main__":
    sim = SimUasMission()
    sim.run()