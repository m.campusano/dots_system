import math
import socketio
from geopy.distance import geodesic

## Drone movement discussion:
# TODO: Review this
# DESCRIPTION: 
# This is an old observation from the Ms guys who developed the simulator first
# My thoughts on this is it is a too simple view on how the drone moves
# While this view could be wrong in long drone paths,
# I think it is ok for small and incremental movement
# ORIGINAL:
# One movement in lat lon = 111km
# We can move about 11 meters per second -> 0.0001 lat lon

class SimDrone:
    NEIGHBORHOOD = 10 # in meters

    def __init__(self, drone_id, plan, sio):
        self.id = drone_id
        self.plan = plan
        self.speed = 0.0001 # latlon per second
        self.current_position = (plan[0]['latitude'], plan[0]['longitude'])
        self.current_index = 0
        # notify position client
        self.sio = sio

    def step(self, time):
        if self.arrived_to_destination():
            return

        next_point = self.peek_point()

        dlatitude = next_point[0] - self.current_position[0]
        dlongitude = next_point[1] - self.current_position[1]
        distance = math.hypot(dlatitude, dlongitude)

        speed_factor = self.speed / distance

        self.current_position = (self.current_position[0] + dlatitude*speed_factor, 
                                self.current_position[1] + dlongitude*speed_factor)
        self.notify()
        self._update_plan()

    def peek_point(self):
        return (self.plan[self.current_index+1]['latitude'], self.plan[self.current_index+1]['longitude'])

    def arrived_to_destination(self):
        return self.current_index >= len(self.plan) - 1

    def notify(self):
        print(f'drone[{self.id}]({self.current_position[0]},{self.current_position[1]})')
        self.sio.emit('drone', (self.id, {'latitude': self.current_position[0], 'longitude': self.current_position[1]}), '/monitor')

    def _update_plan(self):
        if self._arrived_to_next_destination():
            self.current_index += 1

    def _arrived_to_next_destination(self):
        if geodesic(self.current_position, self.peek_point()).meters < self.NEIGHBORHOOD:
            return True

        return False