FROM ros:noetic-ros-base
MAINTAINER miguel <mica@mmmi.sdu.dk>

RUN apt update

# GIT
RUN apt install git -y

# Prepare python libraries
RUN apt install python3-pip -y
RUN pip3 install inflection pytest flask flask-restful flask-jsonpify flask-socketio eventlet geopy

# Download modified pyvisgraph
WORKDIR /
RUN git clone https://gitlab.com/m.campusano/pyvisgraph.git

# Prepare workspace
WORKDIR /
RUN mkdir -p dots_ws/src

# Prepare DOTS
WORKDIR /
RUN git clone https://gitlab.com/m.campusano/dots_system.git /dots
RUN ln -s /dots/ros/dots_shared /dots_ws/src
RUN ln -s /dots/ros/dots_system /dots_ws/src
RUN ln -s /dots/ros/dots_web_server /dots_ws/src
RUN ln -s /dots/ros/dots_daa /dots_ws/src

# Prepare DOTS ROS
WORKDIR /dots_ws
RUN /bin/bash -c 'source /opt/ros/$ROS_DISTRO/setup.bash &&\
    catkin_make'

WORKDIR /
COPY ./dots_entrypoint.sh /
RUN chmod +x dots_entrypoint.sh
ENTRYPOINT ["./dots_entrypoint.sh"]