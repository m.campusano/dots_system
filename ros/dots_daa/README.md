# DAA

This DAA service is planned to work within the EKOH U-space.
While it uses some characteristics of ROS-DOTS, not every component is necessary to make the DAA Service works.

## Installation

We need [geopy](https://github.com/geopy/geopy), [flask-io](https://github.com/miguelgrinberg/Flask-SocketIO) and [eventlet](https://github.com/eventlet/eventlet)

    pip3 install geopy flask-socketio eventlet

We also need to install [SMACH](http://wiki.ros.org/smach), a nested state machine library:

    sudo apt get install ros-<version>-smach

## Services

This system provides the following services

* Alerting
* Conflict Detector
* Operation Status
* Traffic Symulation
* Traffic Information

### ROS-DOTS Services

To make the DAA service works, it needs the following ros-dots services running:

* DOTS Web Server

## Development

This system is divided into several subsystems:

* Drone/Operation status retriever
* Flight status retriever
* Conflict detector / Situation detector
* Alerting decision
* Alerting publisher

### Drone/Operation status retriever

This service gets the current status of the operation and the drone performing the operation.
It has several endpoints that communicate with the operator in charge of the drone, these endpoints are:

* Operation start
* Operation stop
* Operation drone fly
* Operation drone land

This service works as a web server for request/response type of calls..
This web server is the same used by ROS-DOTS, that is why we need that service to make the DAA works.

### Flight status retriever

#### Real data

To retrieve the aircraft data we use the information provided by a server.
To connect to that server we need an id and a token, which are provided via the environment variables `DAA_OUH_ID` and `DAA_OUH_TOKEN`.
For more information, we refer to the documentation on how retrieve data.

This server only supports retrieving data twice for every second.
To be safe, we retrieve only once per second.

Moreover, the data is not live, but it is close enough to be live.
The data of each flight has a delay since the aircraft send the information to the server, that is communicated in the retrieved data.
This means that we can know the exact time where the aircraft reports, but this time is always behind from the moment we retrieve the data.
This is a key detail that needs to be taken into consideration.

As a summary:

* Server to retrieve data
* Server supports call twice per second
* Service calls service one per second
* The data is not up-to-date, but it is very close
* The data informs about the last time an aircraft was detected

#### Simulated data

Moreover, there is a server with several data logs of previous recorded flights.
For more information about that server, we refer to the its own documentation.

These logs are represented as KML files, which a subservice used to simulated a flight.
The kml file has several waypoints where an aircraft informs its position, velocity and when it sent the signal to the server.
While it does not send the track, we can reconstruct it by using the next point in the list of waypoints.

To make the simulation as similar as possible to the service that retrieves real data, the simulation service does not publish the data at the exact time, but it publishes every second.
It orders the list of waypoints by time of signal, and it publishes every second the corresponding waypoint.

As a summary:

* Waypoints of an aircraft via a KML file
* Each waypoint has: altitude, longitude, altitude, speed and time of signal
* We reconstruct the track of the aircraft on each waypoint
* The data is published once every second. It publishes one waypoint until the time of signal of the next waypoint is greater that the amount of waited seconds.

### Conflict detector

This service is used to detect intruders in the OUH U-space.
This service is different from the conflict detection used by the planner service, that one checks about planned flights conflicts.

The DAA conflict detector uses the information of the operation and the air traffic and returns to the system a list of risk status.
To do that, it uses a decision tree, where the leaves represents the risk statuses.

The list of risk status depends on each flight.
In other word, the overall risk of the system is not given by this node, but it just analyses the risk of every flight inside the Detection Volume.

### Alerting service

This service decides which alert should send depending on the list of risk status retrieved from the conflict detector.
This service assign an alert to each risk status, and only publishes the alert with the higher risk.
This means, the alerts are associated a risk level, not the risk status themselves.

For every risk status there are two different alerts: one when there is a risk and one when that risk is no more.
When there is no more risk, the service takes the whole list of risk status.
There is a factory alert that takes a risk status and returns the corresponding alert

The implementation uses a state machine that decides first if it should send an alert, and then which alert should send.

### Pushing alert to operators

When there is an alert in the system, the web server reacts to it and push it to the operator via SocketIO.
The operator should be connected to the web server in the *daa* namespace.


## Considerations

Design decisions considerations and, possible, *TODOs*.

### Synchronized data

While the flight status is only retrieved once every second, we cannot be sure that the resulting alert is also sent once every second.
This all depends on how the architecture is built and how much time the internal algorithms take.

**Flight status**
A simple 1 second wait signal does not work, it is important to take into consideration the drifting caused by the call to the external server, which is not a depreciable amount of time.
If we take into consideration the drifting, then 1 second seems reasonable to achieve (i.e., it is highly improbable that the call to the external server would take more than 1 second)

**Conflict detector, Alerting decision and Alerting publisher**
The same as before, it is important to take into account the drifting.
But if the algorithm by itself takes a depreciable amount of time, then the problem is not that big.

### Flight status extrapolation

A 1 second rate may seem to slow, but at the moment we consider enough time to react.
However, if 1 second turns out to be far from ideal, then another subservice can extrapolate the data from the previous observations.
This is feasible because for each aircraft we know the position, the velocity and the track.
This subservice should be connected between the flight status and the conflict detector.

### Traffic retrieval

The code for getting the traffic data is embedded in the ROS node itself.
Maybe it is a better idea to refactor this code, similar to the UniflyLib.

### Alerting rate

While it is true that we cannot pull data at higher rates, we can send alerting at a much higher rate by re-publishing the same data until new data arrives.

### Risk Status design

The Risk Status names could be deceived, it does not represent the whole risk of the operation, but rather the risk for each flight in the detection volume.
Moreover, risk statuses do not have a risk level associated.
Alerts have a level of severity, and each alert is associated with only one risk status.
Then, the system uses the generated alerts to decide which alert is more important for the operator.

This seems a bit odd, it may be useful to have a RiskStatusPerFlight representing the actual RiskStatus and a new RiskStatus class that represents the actual risk of the operation as a whole.
This may have more sense if, in the future, the risk status may be more complex than just 'the riskier of the RiskStatusPerFlight'.

### System monitor

It is interesting to have a tool that *monitors* the service.
The DAA service needs several nodes to work and the data passing between these nodes may affect how this service behaves.
For example, when an operation is stopped then the system is not pulling traffic data, affectively *turning off* the DAA service.

An implementation of this could have a node that checks the other nodes of the system and the data passing through topics and, probably, services too.

This concept can be extended to any *super service* developed in ROS-DOTS.


## Versions

## 0.3

New Features:

* Conflict detector uses the altitude of the flights. There is a 3D detection now.
* Sym traffic data may send several simulation files at the same time. The files should be inside the folder: `resources\flights`
* The conflict detector and the alerting system takes into consideration the riskier of the flights to alert the operator: 
    - e.g.,* If there is an aircraft inside the detection volume and another inside the well clear volume, it alerts only the one that is inside the well clear volume.
    - In any case, if the aircraft in the well clear volume stops being a problem (*e.g., it landed*), then the system takes into consideration the aircraft in the detection volume. This means, it does not assume that the air space is clear, but it constantly checks every flight.

Limitations:

* No UI for alerts
* No time drifting in subservices
    - We are going to stop documenting the time drifting now, because it has its own subsection in the **Considerations** section. **DO NOT ASSUME** we tackle this in the next versions, unless it is explicitly stated.


## 0.2

For version 0.2, we have new features:

* Real traffic data, but we only consider 1 flight in the airspace.
* The decision tree takes into consideration if the drone is flying or landed.

We have the same limitations of v0.1

## 0.1

For version 0.1 we consider:

* Simulated data provided by 1 KML file of an aircraft that is landing in the well clear volume.

Limitations:

* We do not take into consideration the time drifting from the subservices.
* The conflict detector does not take into consideration the altitude of the aircraft (i.e., we assume 2D detection only).
* There is no Alert UI, only textual information
* Only 1 flight in the airspace