#!/usr/bin/env python3
import dots_process

import rospy
from dots_utils import patcher
patcher.rospy_monkey_patch()

from std_msgs.msg import UInt8MultiArray
from dots.daa import OperationStatus, RiskStatusCollection, FlightPointCollection
from dots.daa.hospital import RiskIdentifier

import pickle

class ConflictDetectorNode:

    def __init__(self):
        self.traffic_status_subscriber = rospy.Subscriber('flight/status', UInt8MultiArray, self.traffic_status_callback) # dots.daa.FlightPointCollection
        self.operation_status_subscriber = rospy.Subscriber('operation/status', UInt8MultiArray, self.operation_status_callback) # dots.daa.OperationStatus
        self.situation_publisher = rospy.Publisher('operation/situation', UInt8MultiArray, queue_size = 10) # dots.daa.RiskStatusCollection
        self.last_traffic_status = FlightPointCollection()
        self.last_operation_status = OperationStatus()
        self.risk_identifier = RiskIdentifier()

    def traffic_status_callback(self, msg):
        self.last_traffic_status = pickle.loads(msg.data)

    def operation_status_callback(self, msg):
        self.last_operation_status = pickle.loads(msg.data)

    def detect_and_publish_conflict(self, event):
        risk_statuses = RiskStatusCollection()
        last_traffic_status = self.last_traffic_status
        for flight in last_traffic_status:
            risk_statuses.append(self.risk_identifier.identify(self.last_operation_status, flight))
        publish_data = UInt8MultiArray()
        publish_data.data = pickle.dumps(risk_statuses)
        self.situation_publisher.publish(publish_data)
        self._log_flight_risk(last_traffic_status, risk_statuses)

    def run(self):
        rospy.init_node('conflict_detector')

        # Create ROS Timer for publishing data at 10 hz
        rospy.Timer(rospy.Duration(1.0/10.0), self.detect_and_publish_conflict)
        
        rospy.spin()

    def _log_flight_risk(self, traffic_status, risk_statuses):
        rospy.loginfo_no_repeat(f'Flight situations {len(risk_statuses)}: {list(map(lambda flight_condition: (flight_condition[0].hex, flight_condition[1].code), list(zip(traffic_status, risk_statuses))))}')
        

if __name__ == "__main__":
    ConflictDetectorNode().run()