#!/usr/bin/env python3
import dots_process

import datetime
import requests
import pickle
import os
import distutils.util
import time

import rospy
import rospkg

from dots_utils import patcher
patcher.rospy_monkey_patch()

from std_srvs.srv import Trigger
from std_msgs.msg import UInt8MultiArray

from dots.daa import FlightPoint, FlightPointCollection
from dots.daa.hospital import USpace

class TrafficNode:

    def __init__(self):
        
        self.url = 'https://healthdrone.dk/odense_hems/json/aircraft_json.php'
        self.credentials = {
            'id':os.environ['DAA_OUH_ID'],
            'token':os.environ['DAA_OUH_TOKEN']
        }

        self.traffic_pull_start_service = rospy.Service("traffic/pull/start", Trigger, self.start_traffic)
        self.traffic_pull_stop_service = rospy.Service("traffic/pull/stop", Trigger, self.stop_traffic)
        self.flight_publisher = rospy.Publisher('flight/status', UInt8MultiArray, queue_size=10) # dots.daa.FlightPoint[]

        self.should_pull = False
        self.last_flights = FlightPointCollection()
        self.u_space = USpace()

    def start_traffic(self, srv):
        self.should_pull = True
        self.last_flights = FlightPointCollection()
        rospy.loginfo('Traffic Pull started')
        return [True, 'success']

    def stop_traffic(self, srv):
        self.should_pull = False
        rospy.loginfo('Traffic Pull stopped')
        return [True, 'success']

    def pull_traffic_data(self, event):
        if self.should_pull:
            init_time = time.perf_counter()
            try:
                self.get_and_publish_traffic_data()
            except Exception as e:
                delta_time = time.perf_counter() - init_time
                dots_process.process_exception(e, {'time':delta_time})


    def get_and_publish_traffic_data(self):
        flight_points = self.get_traffic_data()
        publish_data = UInt8MultiArray()
        publish_data.data = pickle.dumps(flight_points)
        self.flight_publisher.publish(publish_data)

    def get_traffic_data(self):
        r = requests.post(self.url, data = self.credentials)
        flights = flight_points_from_json(r.json())
        self.add_landed_flights(flights)
        self._log_flights(flights)
        self.last_flights = flights
        return flights

    def add_landed_flights(self, flights):
        missing_flights = self.last_flights.missing_points(flights)

        ## array of landed flights in WC volume
        landed_flights = FlightPointCollection()
        for flight in missing_flights:
            point = (flight.latitude, flight.longitude)
            if self.u_space.is_inside_well_clear_volume(point, 0):
                flight.altitude = 0
                flight.landed = True
                landed_flights.append(flight)

        flights.extend(landed_flights)
        
    def run(self):
        rospy.init_node('flight_traffic_status')

        # Create ROS Timer to get data for every second
        rospy.Timer(rospy.Duration(1.0), self.pull_traffic_data)
        rospy.spin()

    def _log_flights(self, flights):
        rospy.loginfo_no_repeat(f'Valid flights {len(flights)}: {list(map(lambda flight: [flight.hex_or_multiple_ids(), f"sim:{flight.sim}", f"Landed:{flight.landed}"], flights))}')

def flight_points_from_json(json_data):
    flight_points = FlightPointCollection()
    for aircraft in json_data['aircraft']:
        point = FlightPoint()
        point.seen = int(aircraft.get('seen'), 0)
        point.time = datetime.datetime.fromtimestamp(int(json_data['now'])+int(point.seen))
        point.type = aircraft.get('type', '')
        point.name = aircraft.get('flight', '')
        point.latitude = float(aircraft.get('lat', 0))
        point.longitude = float(aircraft.get('lon', 0))
        point.altitude = int(aircraft.get('altitude', 0)) * 0.30480
        point.track = int(aircraft.get('track', 0))
        point.speed = float(aircraft.get('speed', 0))
        point.hex = aircraft.get('hex', '')
        point.flarm_id = aircraft.get('flarm_id', '')
        point.ais_id = aircraft.get('ais_id', '')
        point.sim = bool(distutils.util.strtobool(aircraft.get('sim', 'false')))

        if point.is_valid():
            flight_points.append(point)

    return flight_points

if __name__ == "__main__":
    node = TrafficNode()
    node.run()