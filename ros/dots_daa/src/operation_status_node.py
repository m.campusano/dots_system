#!/usr/bin/env python3
import dots_process

import rospy

from std_srvs.srv import Trigger
from std_msgs.msg import UInt8MultiArray

from dots.daa import OperationStatus

import pickle

class OperationStatusNode:

    def __init__(self):
        
        self.operation_start_service = rospy.Service("operation/start", Trigger, self.start_operation)
        self.operation_stop_service = rospy.Service("operation/stop", Trigger, self.stop_operation)
        self.operation_fly_service = rospy.Service("operation/fly", Trigger, self.fly_operation)
        self.operation_land_service = rospy.Service("operation/land", Trigger, self.land_operation)
        self.operation_status_publisher = rospy.Publisher('operation/status', UInt8MultiArray, queue_size=10) # OperationStatus

        self.operation_status = OperationStatus(True)
        self.operation_status.fly()

    def start_operation(self, req):
        self.operation_status.start()
        rospy.loginfo('Operation started')
        return [True, 'success']

    def stop_operation(self, req):
        self.operation_status.stop()
        rospy.loginfo('Operation stopped')
        return [True, 'success']

    def fly_operation(self, req):
        self.operation_status.fly()
        rospy.loginfo('Drone flying')
        return [True, 'success']

    def land_operation(self, req):
        self.operation_status.land()
        rospy.loginfo('Drone landed')
        return [True, 'success']

    def run(self):
        rospy.init_node('operation_status')
        rate = rospy.Rate(10) # 10hz
        while not rospy.is_shutdown():
            publish_data = UInt8MultiArray()
            publish_data.data = pickle.dumps(self.operation_status)
            self.operation_status_publisher.publish(publish_data)
            rate.sleep()



if __name__ == "__main__":
    OperationStatusNode().run()