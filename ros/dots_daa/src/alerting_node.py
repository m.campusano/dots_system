#!/usr/bin/env python3
import dots_process

import rospy
import smach

from std_msgs.msg import UInt8MultiArray

from dots.daa import RiskStatusCollection, Alert
from dots.daa.hospital import AlertFactory

from dots_utils import patcher
patcher.rospy_monkey_patch()

from abc import abstractmethod
import pickle
import time

class AlertingNode:

    def __init__(self):
        self.risk_data = RiskData()
        self.operation_status_subscriber = rospy.Subscriber('operation/situation', UInt8MultiArray, self.risk_statuses_callback) # RiskStatusCollection
        self.alert_publisher = rospy.Publisher('daa/alert', UInt8MultiArray, queue_size = 10) # dots.daa.Alert

    def risk_statuses_callback(self, msg):
        self.risk_data.risk_statuses = pickle.loads(msg.data)

    def run(self):
        rospy.init_node('alerting')
        
        sm = smach.StateMachine(outcomes=['end'])
        with sm:
            smach.StateMachine.add('NoAlert', NoAlertState(self.risk_data, self.alert_publisher),
                                            transitions = {'risk': 'Alert'})
            smach.StateMachine.add('Alert', AlertState(self.risk_data, self.alert_publisher),
                                            transitions = {'no_risk': 'AfterAlert'})
            smach.StateMachine.add('AfterAlert', AfterAlertState(self.risk_data, self.alert_publisher),
                                            transitions = {'stop_alerting': 'NoAlert',
                                                            'risk': 'Alert'})
        sm.execute()
        rospy.spin()

# Wrapper for risk status data
# All objects should access this object to get the same risk_status data
class RiskData:

    def __init__(self):
        self.risk_statuses = RiskStatusCollection()

    def is_risky(self):
        return self.risk_status.is_risky()

class RiskDataState(smach.State):

    def __init__(self, data, publisher, outcomes=[], input_keys=[], output_keys=[]):
        outcomes.append('end')
        super().__init__(outcomes=outcomes, input_keys=input_keys, output_keys=output_keys)
        self.data = data
        self.publisher = publisher
        self.rate = rospy.Rate(10)
        self.risk_statuses = RiskStatusCollection()

    def execute(self, userdata):
        self.process_input_data(userdata)
        start_time = time.time()
        while not rospy.is_shutdown():
            self.risk_statuses = self.data.risk_statuses
            outcome = self.condition(start_time)
            if(outcome):
                self.process_output_data(userdata)
                return outcome
            self.publish()
            self.rate.sleep()

        return 'end'

    @abstractmethod
    def condition(self, start_time):
        pass

    def publish(self):
        pass

    def process_input_data(self, userdata):
        pass

    def process_output_data(self, userdata):
        pass


class NoAlertState(RiskDataState):

    def __init__(self, data, publisher):
        super().__init__(data, publisher, outcomes=['risk'])

    def condition(self, start_time):
        if(self.risk_statuses.is_risky()):
            return 'risk'
        return None


class AlertState(RiskDataState):

    def __init__(self, data, publisher):
        super().__init__(data, publisher, outcomes=['no_risk'], output_keys=['last_riskier_status'])

    def condition(self, start_time):
        if(not self.risk_statuses.is_risky()):
            return 'no_risk'
        return None

    def publish(self):
        riskier_alert = Alert()
        for risk_status in self.risk_statuses:
            alert = AlertFactory.from_risk_status(risk_status)
            if alert.riskier_than(riskier_alert):
                riskier_alert = alert
                self.last_riskier_status = risk_status

        publish_data = UInt8MultiArray()
        publish_data.data = pickle.dumps(riskier_alert)
        self.publisher.publish(publish_data)

        rospy.loginfo_no_repeat(f'Alerting: {riskier_alert}')

    def process_output_data(self, userdata):
        userdata.last_riskier_status = self.last_riskier_status


class AfterAlertState(RiskDataState):

    def __init__(self, data, publisher):
        super().__init__(data, publisher, outcomes=['risk', 'stop_alerting'], input_keys=['last_riskier_status'])
        self.waiting_seconds = 5
        self.last_riskier_status = None

    def condition(self, start_time):
        if(self.risk_statuses.is_risky()):
            return 'risk'
        if(time.time() - start_time > self.waiting_seconds):
            return 'stop_alerting'
        return None

    def publish(self):
        alert = AlertFactory.from_ending_risk_status(self.last_riskier_status)
        publish_data = UInt8MultiArray()
        publish_data.data = pickle.dumps(alert)
        self.publisher.publish(publish_data)

        rospy.loginfo_no_repeat(f'Alerting: {alert}')

    def process_input_data(self,userdata):
        self.last_riskier_status = userdata.last_riskier_status

if __name__ == "__main__":
    AlertingNode().run()