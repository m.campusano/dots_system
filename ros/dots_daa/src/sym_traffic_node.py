#!/usr/bin/env python3
import dots_process

import rospy
import rospkg

from std_srvs.srv import Trigger
from std_msgs.msg import UInt8MultiArray

from sym_flight import SymFlight

import datetime
import pickle
import os

class SymTrafficNode:

    def __init__(self):
        
        # delay the publishing of the first data point
        self.delay = 5

        # kml file
        rospack = rospkg.RosPack()
        self.kml_file_folder = os.path.join(rospack.get_path('dots_daa'), 'resources', 'flights')
        self.sym_flights = []

        self.flight_publisher = rospy.Publisher('flight/status', UInt8MultiArray, queue_size=10) # dots.daa.FlightPoint[]

    def parse_flight_file(self):
        for root, dirs, files in os.walk(self.kml_file_folder):
            for file in files:
                rospy.loginfo("'Sym from file: %s", file)
                self.sym_flights.append(SymFlight.from_kml_file(os.path.join(root, file)))

    def publish_points(self, points):
        publish_data = UInt8MultiArray()
        publish_data.data = pickle.dumps(points)
        self.flight_publisher.publish(publish_data)
        
    def run(self):
        rospy.init_node('flight_traffic_status')
        sym_node.parse_flight_file()

        rospy.sleep(self.delay)
        
        # The next point is published depending on the time the flight was seen
        # This implementation does not take into account time drifting
        rate = rospy.Rate(1)
        for sym_flight in self.sym_flights:
            sym_flight.iterate(1)

        while not rospy.is_shutdown():
            all_finished = True
            points = []
            for sym_flight in self.sym_flights:
                if not sym_flight.finished():
                    points.append(sym_flight.next_point())
                    all_finished = False
            if all_finished:
                break
            print(points)
            self.publish_points(points)
            rate.sleep()

        while not rospy.is_shutdown():
            self.publish_points([])
            rate.sleep()



if __name__ == "__main__":
    sym_node = SymTrafficNode()
    sym_node.run()