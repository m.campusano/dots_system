#!/usr/bin/env python3
import dots_process

import rospy

from std_srvs.srv import Trigger
from std_msgs.msg import UInt8MultiArray

from dots.daa import OperationStatus

import pickle

class TrafficManagerNode:

    def __init__(self):
        
        self.operation_status_subscriber = rospy.Subscriber('operation/status', UInt8MultiArray, self.operation_status_callback) # dots.daa.OperationStatus
        
        self.traffic_pull_start_service = rospy.ServiceProxy("traffic/pull/start", Trigger)
        self.traffic_pull_stop_service = rospy.ServiceProxy("traffic/pull/stop", Trigger)

        self.last_operation_status = None

    def operation_status_callback(self, msg):
        operation_status = pickle.loads(msg.data)
        if self.last_operation_status is None:
            self.last_operation_status = OperationStatus(not operation_status.started)
        if operation_status.started != self.last_operation_status.started:
            self.toggle_traffic_pulling(operation_status)

        self.last_operation_status = operation_status

    def toggle_traffic_pulling(self, operation_status):
        if operation_status.started:
            rospy.loginfo('Traffic Manager start pulling')
            self.traffic_pull_start_service()
        else:
            rospy.loginfo('Traffic Manager stop pulling')
            self.traffic_pull_stop_service()

    def run(self):
        self.traffic_pull_start_service.wait_for_service()
        self.traffic_pull_stop_service.wait_for_service()

        rospy.init_node('traffic_manager')
        rospy.spin()


if __name__ == "__main__":
    TrafficManagerNode().run()