
from dots.daa import FlightPoint

from xml.etree import ElementTree

import datetime
import dateutil.parser
import re
import math

class SymFlight:
    def __init__(self):
        self.points = []
        self.step_seconds = 0

    def add_point(self, flight_point):
        self.points.append(flight_point)
        self.points.sort(key=lambda p: p.time)

    ## Iterable methods ##
    def iterate(self, step = 0):
        self.step_seconds = step
        self.__current_seconds = 0
        self.__index = -1

    def next_point(self):
        self.__advance_index()
        return_value = self.points[self.__index]
        return return_value

    def finished(self):
        return self.__index + 1 >= len(self.points)

    def seconds_until_next_point(self):
        if(self.finished()):
            return 0

        return (self.peek_point().time - self.current_point().time).seconds

    def current_point(self):
        return self.points[self.__index]

    def peek_point(self):
        return self.points[self.__index + 1]

    def __advance_index(self):
        # if no step seconds is defined
        if self.step_seconds == 0:
            self.__index += 1
            return

        # first iteration
        if self.__index == -1:
            self.__index += 1
            return

        self.__current_seconds += self.step_seconds
        
        if self.__current_seconds >= self.seconds_until_next_point():
            self.__index += 1
            self.__current_seconds = 0
            return

    ## private methods ##

    # Using for sym file when there is no track, we calculate it using two points
    def __calculate_track(self):
        pre_point = None
        same_points = []
        for point in self.points:
            if not pre_point:
                pre_point = point
                continue

            # correct track when there are several same points
            # TODO: this does not work with the last point (we do not care about that)
            if pre_point.same_2d_position(point):
                same_points.append(pre_point)
                pre_point = point
                continue

            dy = point.latitude - pre_point.latitude # latitude
            dx = point.longitude - pre_point.longitude # longitude
            # the dy and dx are reverted to start the angle from the north
            angle = math.degrees(math.atan2(float(dx), float(dy)))
            pre_point.track = round(angle) % 360
            if len(same_points) != 0:
                for same_point in same_points:
                    same_point.track = pre_point.track
                same_points = []

            pre_point = point

        # last point
        self.points[-1].track = self.points[-2].track


    ## Initialization methods ##
    @classmethod
    def from_kml_file(cls, file_path):
        flight = cls()

        root = ElementTree.parse(file_path).getroot()
        ns = re.match(r'\{.*\}', root.tag).group(0)
        
        placemarks = root.findall('./{0}document/{0}placemark'.format(ns))
        placemarks.pop(0) #remove first
        for placemark in placemarks:
            point = FlightPoint()

            # description attribute data
            description = placemark.find('{0}description'.format(ns)).text
            pattern = re.compile('Track id: (.*) Time: (.*) Altitude: (.*) m Speed: (.*) m/s')
            elements = pattern.findall(description)[0]
            point.time = dateutil.parser.parse(elements[1])
            point.speed = float(elements[3])
            point.point_id = elements[0]

            # coordinate data
            coordinates = placemark.find('{0}point/{0}coordinates'.format(ns)).text.split(',')
            point.longitude = float(coordinates[0])
            point.latitude = float(coordinates[1])
            point.altitude = float(coordinates[2])

            flight.add_point(point)

        flight.__calculate_track()

        return flight