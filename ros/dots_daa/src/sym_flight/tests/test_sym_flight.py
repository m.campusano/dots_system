from sym_flight import SymFlight
from dots.daa import FlightPoint

import datetime
import dateutil.parser
import time
import inspect
from pathlib import Path

class TestSymFlight:

    def testSymInit(self):
        flight = SymFlight()

        assert len(flight.points) == 0

    def testSymPoints(self):
        flight = SymFlight()
        flight.add_point(FlightPoint())
        flight.add_point(FlightPoint())

        assert len(flight.points) == 2

    def testOrderPoints(self):
        now = datetime.datetime.now().astimezone()

        flight = SymFlight()
        pointA = FlightPoint()
        pointA.time = now
        pointB = FlightPoint()
        pointB.time = now + datetime.timedelta(seconds = 2)
        flight.add_point(pointB)
        flight.add_point(pointA)

        assert flight.points[0] == pointA
        assert flight.points[1] == pointB

    def testPointIterator(self):
        now = datetime.datetime.now().astimezone()

        flight = SymFlight()
        pointA = FlightPoint()
        pointA.time = now
        pointB = FlightPoint()
        pointB.time = now + datetime.timedelta(seconds = 2)
        flight.add_point(pointB)
        flight.add_point(pointA)

        flight.iterate()
        assert not flight.finished()
        assert flight.next_point() == pointA
        assert flight.next_point() == pointB
        assert flight.finished()

    def testSecondsUntilNextPoint(self):
        now = datetime.datetime.now().astimezone()

        flight = SymFlight()
        pointA = FlightPoint()
        pointA.time = now
        pointB = FlightPoint()
        pointB.time = now + datetime.timedelta(seconds = 2)
        flight.add_point(pointB)
        flight.add_point(pointA)

        flight.iterate()
        flight.next_point()
        assert flight.seconds_until_next_point() == 2
        flight.next_point()
        assert flight.seconds_until_next_point() == 0

    def testFromKMLFile(self):
        file_path = Path(inspect.getfile(self.__class__))
        kml_path = file_path.parent / 'test.kml'
        flight = SymFlight.from_kml_file(kml_path)
        assert len(flight.points) == 5
        
        flight.iterate()
        point = flight.next_point()
        assert point.time == dateutil.parser.parse('2020-12-25 14:59:50')
        assert point.latitude == 1.0
        assert point.longitude == 1.0
        assert point.track == 90
        assert point.speed == 10.0
        assert point.altitude == 20

        point = flight.next_point()
        assert point.time == dateutil.parser.parse('2020-12-25 15:00:00')
        assert point.latitude == 1.0
        assert point.longitude == 1.0
        assert point.track == 90
        assert point.speed == 10.0
        assert point.altitude == 20

        point = flight.next_point()
        assert point.time == dateutil.parser.parse('2020-12-25 15:00:01')
        assert point.latitude == 1.0
        assert point.longitude == 2.0
        assert point.track == 0
        assert point.speed == 11.0
        assert point.altitude == 25

        point = flight.next_point()
        assert point.time == dateutil.parser.parse('2020-12-25 15:00:02')
        assert point.latitude == 2.0
        assert point.longitude == 2.0
        assert point.track == 45
        assert point.speed == 12.0
        assert point.altitude == 30

        point = flight.next_point()
        assert point.time == dateutil.parser.parse('2020-12-25 15:00:03')
        assert point.latitude == 3.0
        assert point.longitude == 3.0
        assert point.track == 45
        assert point.speed == 0.0
        assert point.altitude == 0

    def testNextPointWithStep(self):
        now = datetime.datetime.now().astimezone()

        flight = SymFlight()
        pointA = FlightPoint()
        pointA.time = now
        pointA.point_id = 1
        pointB = FlightPoint()
        pointB.time = now + datetime.timedelta(seconds = 1)
        pointB.point_id = 2
        pointC = FlightPoint()
        pointC.time = now + datetime.timedelta(seconds = 3)
        pointC.point_id = 3
        pointD = FlightPoint()
        pointD.time = now + datetime.timedelta(seconds = 6)
        pointD.point_id = 4
        flight.add_point(pointB)
        flight.add_point(pointA)
        flight.add_point(pointD)
        flight.add_point(pointC)

        flight.iterate(1)
        assert flight.next_point() == pointA
        assert flight.next_point() == pointB
        assert flight.next_point() == pointB
        assert flight.next_point() == pointC
        assert flight.next_point() == pointC
        assert flight.next_point() == pointC
        assert flight.next_point() == pointD
