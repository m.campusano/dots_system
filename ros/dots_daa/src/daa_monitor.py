#!/usr/bin/env python3
import dots_process

import rospy

from std_msgs.msg import UInt8MultiArray
from dots_daa.msg import DAAStatus

from dots.daa import OperationStatus

import pickle

class DAAMonitorNode:

    def __init__(self):
        
        self.operation_status_subscriber = rospy.Subscriber('operation/status', UInt8MultiArray, self.operation_status_callback) # dots.daa.OperationStatus
        self.daa_status_publisher = rospy.Publisher('daa/status', DAAStatus, queue_size=10)
        self.last_operation_status = OperationStatus()

    def status(self):
        status = DAAStatus()
        if self.last_operation_status.started:
            status.status = DAAStatus.ON
        else:
            status.status = DAAStatus.OFF

        return status

    def operation_status_callback(self, msg):
        self.last_operation_status = pickle.loads(msg.data)

    def run(self):
        rospy.init_node('daa_monitor')
        rate = rospy.Rate(1) # 1 per second
        while not rospy.is_shutdown():
            self.daa_status_publisher.publish(self.status())
            rate.sleep()


if __name__ == "__main__":
    DAAMonitorNode().run()