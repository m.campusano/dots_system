#!/usr/bin/env python3
import eventlet
eventlet.monkey_patch()
import dots_process

from flask import Flask, request, current_app
from flask_jsonpify import jsonify
from flask_restful import Resource, Api
from flask_socketio import SocketIO, Namespace

import rospy
from rospy.service import ServiceException
from std_srvs.srv import Trigger
from std_msgs.msg import UInt8MultiArray

from dots_shared.srv import SaveData, LoadData, \
                                ParseTemplate, PlanProgram
from dots_daa.msg import DAAStatus
from dots_shared.msg import DronePosition, MissionStatus, WorldState

from dots.core.program import Program
from dots.core.type import Area

from dots_utils import patcher
patcher.rospy_monkey_patch()
patcher.web_patch()

import os
import pickle

### Urls ###

#/template
#/template  <- new template (POST)
#/template -> list of all templates
#/template/<id> -> data about template (for now, use id=name)
#/template/<id> <- new program (POST)
#/drone/<id>/plan -> plan for drone (only first available plan for now)

## DAA ##

#/operation/start
#/operation/stop
#/operation/fly
#/operation/land

## Drone Position ##

#/monitor/drone/<id> -> drone position

############


class Server:

    def __init__(self):

        self.set_up()

        ## database ##
        self.get_templates_service = rospy.ServiceProxy("database/template/all", LoadData)
        self.get_operation_service = rospy.ServiceProxy("database/template/get", LoadData)
        self.save_program_service = rospy.ServiceProxy("database/program/save", SaveData)
        self.get_drone_plan_service = rospy.ServiceProxy("database/drone/plan", LoadData)
        self.save_no_fly_zone_service = rospy.ServiceProxy('database/no_fly_zone/save', SaveData)
        ##############

        ## parser ##
        self.parse_template_service = rospy.ServiceProxy("parser/parse", ParseTemplate)
        ############

        ## planner ##
        self.plan_service = rospy.ServiceProxy("planner/plan", PlanProgram)
        #############

        ## daa ##
        self.operation_start_service = rospy.ServiceProxy("operation/start", Trigger)
        self.operation_stop_service = rospy.ServiceProxy("operation/stop", Trigger)
        self.operation_fly_service = rospy.ServiceProxy("operation/fly", Trigger)
        self.operation_land_service = rospy.ServiceProxy("operation/land", Trigger)
        
        self.alerting_subscriber = rospy.Subscriber('daa/alert', UInt8MultiArray, self.alerting_callback) # dots.daa.Alert
        self.daa_status_subscriber = rospy.Subscriber('daa/status', DAAStatus, self.daa_status_callback)
        self.last_daa_status = None
        #########

        ## monitor ##
        self.drone_publisher = rospy.Publisher('monitor/drone/position', DronePosition, queue_size = 10) 
        self.mission_monitor = rospy.Subscriber('monitor/mission/status', MissionStatus, self.mission_status_callback)
        self.world_state = rospy.Subscriber('database/world/state', WorldState, self.world_state_callback)
        #############
        
        self.app = Flask(__name__)
        self.app.secret_key = self.options['secret']
        self.api = Api(self.app)
        socket_options = { 
            'logger' : False,
            'cors_allowed_origins' : '*'
        }
        self.socketio = SocketIO(self.app, **socket_options)

        self.set_resources()
       
    def set_resources(self):
        ## restful resources
        self.api.add_resource(DotsTemplate, "/template", 
            resource_class_kwargs={
                "parse_template": self.parse_template_service,
                "get_templates": self.get_templates_service
            })
        self.api.add_resource(DotsTemplateOperation, "/template/<operation_name>",
            resource_class_kwargs={
                "get_operation": self.get_operation_service,
                "plan_program": self.plan_service,
                "save_program": self.save_program_service
            })
        self.api.add_resource(DotsDronePlan, "/drone/<drone_id>/plan",
            resource_class_kwargs={
                "get_drone_plan": self.get_drone_plan_service,
            })
        self.api.add_resource(DotsNoFlyZone, "/no-fly-zone",
            resource_class_kwargs={
                "save_no_fly_zone": self.save_no_fly_zone_service
            })

        ## other resources
        OperationResource(self.app, "/operation",
            self.operation_start_service,
            self.operation_stop_service,
            self.operation_fly_service,
            self.operation_land_service
            ).add_resources()

        ## websocket resources
        self.daa_websocket = DaaResource()
        self.monitor_websocket = MonitorResource(self.drone_publisher)
        self.world_state_websocket = WorldStateResource()
        self.socketio.on_namespace(self.monitor_websocket)
        self.socketio.on_namespace(self.world_state_websocket)

    def alerting_callback(self, msg):
        alert = pickle.loads(msg.data)
        # rospy.loginfo(f'Alerting received: {alert}')
        self.daa_websocket.emit_alert(alert.as_web_dictionary())

    def daa_status_callback(self, daa_status):
        # if self.last_daa_status is not daa_status: # this does not work
        #     rospy.loginfo(f'DAA Status changed: {daa_status}')
        self.daa_websocket.emit_status(daa_status.as_web_dictionary())

        self.last_daa_status = daa_status

    def mission_status_callback(self, mission_status):
        self.monitor_websocket.emit_mission(mission_status.as_web_dictionary())

    def world_state_callback(self, world_state):
        self.world_state_websocket.emit_state(world_state.as_web_dictionary())

    def set_up(self):
        self._defaults = {
            'host' : '0.0.0.0',
            'port' : 8080,
            'env' : 'Development',
            'secret' : 'dev_secret'
        }
        self.options = {
            'host' : os.environ['FLASK_HOST'] if 'FLASK_HOST' in os.environ else self._defaults['host'],
            'port' : os.environ['FLASK_PORT'] if 'FLASK_PORT' in os.environ else self._defaults['port'],
            'env' : os.environ['DOTS_ENV'] if 'DOTS_ENV' in os.environ else self._defaults['env'],
            'secret' : os.environ['FLASK_SECRET'] if 'FLASK_SECRET' in os.environ else self._defaults['secret']
        }

    def is_development(self):
        return self.options['env'] == 'Development'

    def run(self):
        self.socketio.server.start_background_task(self.__run_node)
        self.socketio.run(self.app, host=self.options['host'], port=self.options['port'], use_reloader=False, log_output=True, debug= True)

    def __run_node(self):
        rospy.init_node('web_server', disable_signals=True)

        

class DotsTemplate(Resource):

    def __init__(self, **kwargs):
        self.parse_template = kwargs["parse_template"]
        self.get_templates = kwargs["get_templates"]

    def post(self):
        text = request.get_data(as_text=True)
        parser_response = self.parse_template(text)
        operation = pickle.loads(parser_response.operation)
        return jsonify({
            'status' : 'success',
            'operation' : operation.name
        })

    def get(self):
        response = self.get_templates()
        operations = pickle.loads(response.data)
        return jsonify({
            'status': 'success',
            'operations': list(map(lambda operation: operation.name, operations))
        })


class DotsTemplateOperation(Resource):

    def __init__(self, **kwargs):
        self.get_operation = kwargs["get_operation"]
        self.plan_program = kwargs["plan_program"]
        self.save_program = kwargs["save_program"]

    def get(self, operation_name):
        data = {'operation': operation_name}
        response = self.get_operation(pickle.dumps(data))
        operation = pickle.loads(response.data)

        return jsonify({
            'status': 'success',
            'parameters': operation.get_parameters_as_dictionary()
        })

    def post(self, operation_name):

        # create program
        data = {'operation': operation_name}
        response = self.get_operation(pickle.dumps(data))
        operation = pickle.loads(response.data)
        arguments = request.get_json()
        program = Program.create_with_arguments(operation, arguments)
        self.save_program(pickle.dumps(program))

        # plan program
        response = self.plan_program(pickle.dumps(program))

        return jsonify({
            'status': 'success'
        })

class DotsDronePlan(Resource):

    def __init__(self, **kwargs):
        self.get_drone_plan = kwargs["get_drone_plan"]

    def get(self, drone_id):
        data = {'drone_id' : drone_id}
        response = self.get_drone_plan(pickle.dumps(data))
        plan = pickle.loads(response.data)
        return jsonify(plan.as_web_dictionary())

class DotsNoFlyZone(Resource):
    def __init__(self, **kwargs):
        self.save_no_fly_zone = kwargs["save_no_fly_zone"]

    def post(self):
        area = Area.from_json(request.get_json())
        response = self.save_no_fly_zone(pickle.dumps(area))

        return jsonify({
            'status': 'success'
        })

class OperationResource:
    def __init__(self, app, base_url, start_service, stop_service, fly_service, land_service):
        self.app = app
        self.base_url = base_url
        self.start_service = start_service
        self.stop_service = stop_service
        self.fly_service = fly_service
        self.land_service = land_service

    def add_resources(self):
        self.app.add_url_rule(self.base_url + '/start', view_func=self.start)
        self.app.add_url_rule(self.base_url + '/stop', view_func=self.stop)
        self.app.add_url_rule(self.base_url + '/fly', view_func=self.fly)
        self.app.add_url_rule(self.base_url + '/land', view_func=self.land)

    def _ros_service_request(self, ros_service, success_log, error_log):
        success = True
        message = ''
        status_message = ''
        code = 200
        try:
            response = ros_service()
            success = response.success
            message = response.message

            status_message = 'success' if success else 'error'
            if success:
                rospy.loginfo(success_log)
            else:
                rospy.logerr('%s: %s', error_log, response.message)
                code = 503
                
        except ServiceException as e:
            rospy.logerr('%s: %s', error_log, e)
            status_message = 'error'
            message = 'Service Unavailable'
            code = 503

        return jsonify({
            'status' : status_message,
            'message' : message
        }), code

    def start(self):
        return self._ros_service_request(self.start_service, 'Operation started', 'Operation could not start')

    def stop(self):
        return self._ros_service_request(self.stop_service, 'Operation stopped', 'Operation could not stop')

    def fly(self):
        return self._ros_service_request(self.fly_service, 'Drone flying', 'Drone could not fly')
        
    def land(self):
        return self._ros_service_request(self.land_service, 'Drone landed', 'Drone could not land')

class DaaResource(Namespace):

    def __init__(self):
        super().__init__('/daa')

    def on_connect(self):
        rospy.loginfo(f'daa client connected: {request.remote_addr}')

    def on_disconnect(self):
        rospy.loginfo(f'daa client disconnected: {request.remote_addr}')

    def emit_alert(self, dict_alert):
        self.emit('alert', dict_alert)
        # rospy.loginfo(f'Alerting emitted: {dict_alert}')

    def emit_status(self, daa_status):
        self.emit('status', daa_status)
        # rospy.loginfo(f'Status emitted: {daa_status}')


class MonitorResource(Namespace):

    def __init__(self, drone_publisher):
        super().__init__('/monitor')
        self.drone_publisher = drone_publisher

    def emit_mission(self, status):
        self.emit('mission', status)

    def on_drone(self, drone_id, position):
        if rospy.core.is_initialized():
            self.drone_publisher.publish(drone_id, position['latitude'], position['longitude'])

class WorldStateResource(Namespace):

    def __init__(self):
        super().__init__('/world')

    def emit_state(self, state):
        self.emit('state', state)


if __name__ == "__main__":
    server = Server()
    server.run()
