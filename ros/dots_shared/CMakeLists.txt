cmake_minimum_required(VERSION 3.0.2)
project(dots_shared)

find_package(catkin REQUIRED COMPONENTS
  std_msgs
  message_generation
)

## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
catkin_python_setup()

## Generate services in the 'srv' folder
add_service_files(
  FILES

  planner/PlanProgram.srv
  planner/RePlanPrograms.srv

  parser/ParseTemplate.srv

  database/SaveData.srv
  database/LoadData.srv

  dynamic_replanning/SliceProgram.srv
)

add_message_files(DIRECTORY msg 
  FILES
  DronePosition.msg
  MissionStatus.msg
  WorldState.msg
)

generate_messages(
  DEPENDENCIES
  std_msgs
)