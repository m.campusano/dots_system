from dots.core.type import Area, NavigationPoint 
from dots.core.plan import DronePlan, OperationPlan
from dots.daa import Alert

from dots_daa.msg import DAAStatus
from dots_shared.msg import MissionStatus, WorldState

import pickle

def web_patch():
    as_dictionary_patch()
    from_json_patch()

def as_dictionary_patch():
    setattr(DronePlan, 'as_web_dictionary', drone_plan_as_dictionary)
    setattr(OperationPlan, 'as_web_dictionary', operation_plan_as_dictionary)
    setattr(Alert, 'as_web_dictionary', alert_as_dictionary)
    setattr(DAAStatus, 'as_web_dictionary', daa_as_dictionary)
    setattr(MissionStatus, 'as_web_dictionary', mission_status_as_dictionary)
    setattr(WorldState, 'as_web_dictionary', world_state_as_dictionary)
    setattr(Area, 'as_web_dictionary', area_as_dictionary)
    setattr(NavigationPoint, 'as_web_dictionary', point_as_dictionary)
    
def from_json_patch():
    setattr(Area, 'from_json', classmethod(area_from_json))


def drone_plan_as_dictionary(self):
    points = []
    for waypoint in self.path:
        point = {
            'latitude' : waypoint.latitude,
            'longitude' : waypoint.longitude,
            'time_to_depart' : waypoint.depart_to_here_time.get_serialized_string()
        }
        points.append(point)
    return points

def alert_as_dictionary(self):
    return {
        'timestamp' : self.created_at.strftime('%H:%M:%S'),
        'default_message' : self.default_message,
        'type' : self.type.value,
        'risk_code' : self.risk_code
    }

def daa_as_dictionary(self):
    return {
        'status' : self.status
    }

def operation_plan_as_dictionary(self):
    plans = []
    for drone_plan in self.drone_plans:
        drone_plan_dict = {
            'uuid' : drone_plan.drone.uuid,
            'path' : drone_plan.as_web_dictionary()
        }
        plans.append(drone_plan_dict)
    return plans

def mission_status_as_dictionary(self):

    drones = []
    for drone in self.drones:
        drones.append({
            'id' : drone.uuid,
            'latitude' : drone.latitude,
            'longitude' : drone.longitude
        })
    status = {
        'id' : self.uuid,
        'drones' : drones,
        'replanned' : self.replanned,
        'original_plan' : pickle.loads(self.original_plan).as_web_dictionary(),
        'plan' : pickle.loads(self.plan).as_web_dictionary(),
        'no_fly_zones': list(map(lambda area: area.as_web_dictionary(), pickle.loads(self.no_fly_zones)))
    }
    return status

def world_state_as_dictionary(self):
    return {
        'no_fly_zones' : list(map(lambda area: area.as_web_dictionary(), pickle.loads(self.no_fly_zones)))
    }

def area_as_dictionary(self):
    return list(map(lambda point: point.as_web_dictionary(), self.boundingBox))

def point_as_dictionary(self):
    return {'latitude' : self.latitude, 'longitude' : self.longitude}


def area_from_json(cls, json_area):
    area = cls()
    for json_point in json_area:
        area.add_point(NavigationPoint(json_point['latitude'], json_point['longitude']))
    return area