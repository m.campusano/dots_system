import rospy
import inspect
from hashlib import md5

def rospy_monkey_patch():
    setattr(rospy, 'loginfo_no_repeat', loginfo_no_repeat)
    setattr(rospy, 'logdebug_no_repeat', logdebug_no_repeat)
    setattr(rospy, 'logwarn_no_repeat', logwarn_no_repeat)
    setattr(rospy, 'logerr_no_repeat', logerr_no_repeat)
    setattr(rospy, 'logfatal_no_repeat', logfatal_no_repeat)

def loginfo_no_repeat(msg, *args, **kwargs):
    _base_no_repeat(msg, args, kwargs, level='info')

def logdebug_no_repeat(msg, *args, **kwargs):
    _base_no_repeat(msg, args, kwargs, level='debug')

def logwarn_no_repeat(msg, *args, **kwargs):
    _base_no_repeat(msg, args, kwargs, level='warn')

def logerr_no_repeat(msg, *args, **kwargs):
    _base_no_repeat(msg, args, kwargs, level='error')

def logfatal_no_repeat(msg, *args, **kwargs):
    _base_no_repeat(msg, args, kwargs, level='critical')

def _base_no_repeat(msg, args, kwargs, level=None):
    caller_id = rospy.core._frame_to_caller_id(inspect.currentframe().f_back.f_back)
    if _logging_identical(caller_id, msg):
        rospy.core._base_logger(msg, args, kwargs, level=level)

class LoggingIdentical(object):

    last_logging_msg = ''
    last_caller_id = ''

    def __call__(self, caller_id, msg):
        
        msg_hash = md5(msg.encode()).hexdigest()

        if caller_id != self.last_caller_id or msg_hash != self.last_logging_msg:
            self.last_logging_msg = msg_hash
            self.last_caller_id = caller_id
            return True
        return False

_logging_identical = LoggingIdentical()