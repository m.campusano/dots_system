# __init__.py

from .sentry import setup
from .sentry import process_exception
from .sentry import process_message

setup()