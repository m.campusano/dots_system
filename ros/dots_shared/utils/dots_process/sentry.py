import importlib
import logging

def has_sentry():
    sentry_pkg = importlib.util.find_spec('sentry_sdk')
    return not (sentry_pkg is None)

def setup():
    if not has_sentry():
        logging.warning('Sentry not found: The system will not notify errors')
        return

    import sentry_sdk
    import os
    sentry_sdk.init(
        traces_sample_rate=1.0,
        environment = os.environ['DOTS_ENV'] if 'DOTS_ENV' in os.environ else 'Development'
    )

def process_exception(exception, extra_scope=None):
    if not should_process():
        raise exception
        return

    import sentry_sdk
    with sentry_sdk.push_scope() as scope:
        for key, value in extra_scope.items():
            scope.set_extra(key, value)
        sentry_sdk.capture_exception(exception)

def process_message(message):
    if not should_process():
        logging.warning(message)

    import sentry_sdk
    sentry_sdk.capture_message(message)

def should_process():
    if has_sentry():
        from sentry_sdk import Hub
        if Hub.current.client.options['environment'] != 'Development':
            return True
    return False
