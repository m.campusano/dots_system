from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

setup_args = generate_distutils_setup(
    packages=['dots_utils', 'dots_process'],
    package_dir={'': 'utils'}
)

setup(**setup_args)