#!/usr/bin/env python3

import rospy
from std_msgs.msg import UInt8MultiArray
from dots_shared.msg import WorldState
from dots_shared.srv import SaveData, LoadData, LoadDataResponse
from data_model import Template, TemplateManager

import datetime
import pickle

class DatabaseNode:

    def __init__(self):
        
        # templates and operations
        self.save_template_service = rospy.Service("database/template/save", SaveData, self.save_template)
        self.get_templates_service = rospy.Service("database/template/all", LoadData, self.get_templates)
        self.get_template_service = rospy.Service("database/template/get", LoadData, self.get_template)

        # programs
        self.save_program_service = rospy.Service("database/program/save", SaveData, self.save_program)
        self.save_planned_program_service = rospy.Service("database/program/planned/save", SaveData, self.save_planned_programs)
        self.get_non_started_program_service = rospy.Service("database/program/planned/non_started", LoadData, self.get_non_started_program)
        self.new_planned_program_publisher = rospy.Publisher('database/callback/program/planned/new', UInt8MultiArray, queue_size=10) # dots.core.program.Program
        self.get_program_from_drone = rospy.Service("database/drone/program", LoadData, self.get_program_from_drone)
        self.update_program_service = rospy.Service("database/program/update", SaveData, self.update_program) # dots.core.program.Program
        self.program_update_callback = rospy.Publisher('database/callback/program/updated', UInt8MultiArray, queue_size=10) # dots.core.program.Program

        # no fly zones
        self.get_no_fly_zones_service = rospy.Service("database/no_fly_zone/all", LoadData, self.get_no_fly_zones)
        self.save_no_fly_zone_service = rospy.Service('database/no_fly_zone/save', SaveData, self.save_no_fly_zone)
        self.new_no_fly_zone_publisher = rospy.Publisher('database/callback/no_fly_zone/new', UInt8MultiArray, queue_size=10) # dots.core.type.Area
        self.all_no_fly_zone_publisher = rospy.Publisher('database/callback/no_fly_zone/new/all', UInt8MultiArray, queue_size=10) # dots.core.type.Area[]

        # plans
        self.get_drone_plan_service = rospy.Service("database/drone/plan", LoadData, self.get_drone_plan)

        #hardcoded
        self.publish_world_state = rospy.Publisher('database/world/state', WorldState, queue_size = 10)

        ## DB ##
        self.template_manager = TemplateManager()
        self.no_fly_zones = []
        ########
    
    def save_template(self, req):
        operation = pickle.loads(req.data)
        template = Template(operation)
        self.template_manager.add_template(template)
        return [False]

    def get_templates(self, req):
        response = LoadDataResponse()
        response.data = pickle.dumps(self.template_manager.get_operations())
        return response

    def get_template(self, req):
        data = pickle.loads(req.data)
        template = self.template_manager.get_template(data['operation'])
        response = LoadDataResponse()
        response.data = pickle.dumps(template.operation)
        return response

    def save_program(self, req):
        program = pickle.loads(req.data)
        self.template_manager.add_program(program)
        return [False]

    def save_planned_programs(self, req):
        programs = pickle.loads(req.data)
        self.template_manager.replace_programs(programs)
        for program in programs:
            publish_data = UInt8MultiArray()
            publish_data.data = pickle.dumps(program)
            self.new_planned_program_publisher.publish(publish_data)
        return [False]

    def get_non_started_program(self, req):
        response = LoadDataResponse()
        response.data = pickle.dumps(self.template_manager.not_started_programs())
        return response

    def get_no_fly_zones(self, req):
        response = LoadDataResponse()
        response.data = pickle.dumps(self.no_fly_zones)
        return response

    def save_no_fly_zone(self, req):
        area = pickle.loads(req.data)
        self.no_fly_zones.append(area)

        publish_data = UInt8MultiArray()
        publish_data.data = pickle.dumps(area)
        self.new_no_fly_zone_publisher.publish(publish_data)

        publish_data = UInt8MultiArray()
        publish_data.data = pickle.dumps(self.no_fly_zones)
        self.all_no_fly_zone_publisher.publish(publish_data)
        return [False]

    def get_drone_plan(self, req):
        data = pickle.loads(req.data)
        response = LoadDataResponse()
        response.data = pickle.dumps(self.template_manager.get_drone_plan_from_id(data['drone_id']))
        return response

    def get_program_from_drone(self, req):
        data = pickle.loads(req.data)
        response = LoadDataResponse()
        response.data = pickle.dumps(self.template_manager.get_program_from_drone_id(data['drone_id']))
        return response

    def update_program(self, req):
        program = pickle.loads(req.data)
        self.template_manager.replace_program(program)

        publish_data = UInt8MultiArray()
        publish_data.data = pickle.dumps(program)
        self.program_update_callback.publish(publish_data)
        return [False]

    def push_world_state(self, event):
        state = WorldState()
        state.no_fly_zones = pickle.dumps(self.no_fly_zones)
        self.publish_world_state.publish(state)

    def run(self):
        rospy.init_node('dots_database')
        rospy.Timer(rospy.Duration(1/10), self.push_world_state)
        rospy.spin()


if __name__ == "__main__":
    DatabaseNode().run()