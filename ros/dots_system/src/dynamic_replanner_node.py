#!/usr/bin/env python3

import rospy
import pickle
import time

from dots_shared.srv import SliceProgram, PlanProgram

from dots.slicer import Slicer


class DynamicReplannerNode:

    def __init__(self):
        self.slice_program = rospy.Service("dynamic_replanner/replan", SliceProgram, self.dynamic_replan)
        self.plan_service = rospy.ServiceProxy("planner/plan", PlanProgram)

    def dynamic_replan(self, req):
        pre = time.time()
        new_program = Slicer().slice(pickle.loads(req.program))
        print(time.time()-pre)
        response = self.plan_service(pickle.dumps(new_program))
        return [False, response.program]

    def run(self):
        rospy.init_node('dynamic_replanner')
        rospy.spin()


if __name__ == "__main__":
    DynamicReplannerNode().run()