

class Template():

    def __init__(self, operation):
        self.operation = operation
        self.programs = []

    def non_canceled_programs(self):
        return list(filter(lambda program: program.non_canceled(), self.programs))

    def add_program(self, program):
        self.programs.append(program)

    def replace_program(self, program):
        self.programs.remove(program)
        self.programs.append(program)

class TemplateManager:

    def __init__(self):
        self.templates = []

    def add_template(self, template):
        self.templates.append(template)

    def get_template(self, operation_name):
        try:
            return next(filter(lambda template: template.operation.name == operation_name, self.templates))
        except StopIteration:
            pass

        return None

    def get_drone_plan_from_id(self, drone_id):
        for template in self.templates:
            for program in template.non_canceled_programs():
                for drone_plan in program.operation_plan.drone_plans:
                    if drone_plan.drone.uuid == drone_id:
                        return drone_plan
        return None

    def add_program(self, program):
        template = self.get_template(program.operation.name)

        if template is None:
            template = self.get_template(program.original_operation.name)
        if template is None:
            template = self.create_template_from_program(program)

        template.add_program(program)

    def create_template_from_program(self, program):
        template = Template(program.operation)
        template.add_program(program)
        self.templates.append(template)
        return template

    def replace_programs(self, programs):
        for program in programs:
            self.replace_program(program)

    def replace_program(self, program):
        template = self.get_template(program.operation.name)
        if template is None:
            template = self.get_template(program.original_operation.name)
        if template is not None:
            template.replace_program(program)
        else:
            self.add_program(program)

    def not_started_programs(self):
        programs = []
        for template in self.templates:
            for program in template.non_canceled_programs():
                if program.has_plan() and not program.has_started():
                    programs.append(program)
        return programs

    def get_program_from_drone_id(self, drone_id):
        for template in self.templates:
            for program in template.programs:
                for drone_plan in program.operation_plan.drone_plans:
                    if drone_plan.drone.uuid == drone_id:
                        return program
        return None

    def get_operations(self):
        return list(map(lambda template: template.operation, self.templates))
