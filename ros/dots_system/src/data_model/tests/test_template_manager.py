from dots.core.program import Program
from dots.ast import Operation
from data_model import TemplateManager, Template

class TestTemplateManager:

    def testNewTemplate(self):
        manager = TemplateManager()
        assert not manager.templates


    def testAddTemplate(self):
        manager = TemplateManager()
        manager.add_template(Template(Operation("test")))
        assert len(manager.templates) == 1


    def testAddProgram(self):
        manager = TemplateManager()
        operation = Operation('operation')
        program = Program(operation, {})
        template = Template(operation)
        template.add_program(program)
        manager.add_template(template)

        assert len(template.programs) == 1