#!/usr/bin/env python3

import rospy
import pickle

from std_msgs.msg import UInt8MultiArray
from dots_shared.msg import DronePosition, MissionStatus
from dots_shared.srv import LoadData, SaveData, SliceProgram

from dots.core.plan import OperationPlan
from dots.utils import ConflictDetector

class MissionMonitorNode:

    def __init__(self):
        rospy.Subscriber("monitor/drone/position", DronePosition, self.drone_position)
        rospy.Subscriber("database/callback/no_fly_zone/new/all", UInt8MultiArray, self.conflict_detector_callback) # dots.core.type.Area[]
        self.publish_mission = rospy.Publisher("monitor/mission/status", MissionStatus, queue_size = 10)
        self.get_program = rospy.ServiceProxy("database/drone/program", LoadData) # dots.core.program.Program
        self.update_program = rospy.ServiceProxy("database/program/update", SaveData) # dots.core.program.Program
        self.save = rospy.ServiceProxy("database/program/save", SaveData) # dots.core.program.Program
        self.slice_program = rospy.ServiceProxy("dynamic_replanner/replan", SliceProgram)
        self.running_program = None
        self.conflict_detector = ConflictDetector()

        ## hardcoded for demo
        self.original_plan = OperationPlan()
        self.replanned = False

    def drone_position(self, drone_position):
        program = self.program_from_drone_id(drone_position.uuid)
        program.drone_position(drone_position.uuid, drone_position.latitude, drone_position.longitude)

    def conflict_detector_callback(self, data):
        no_fly_zones = pickle.loads(data.data)
        if self.conflict_detector.plan_in_no_fly_zones(self.running_program.operation_plan, no_fly_zones):
            self.mission_in_conflict(self.running_program)

    def mission_in_conflict(self, conflicted_program):
        self.original_plan = self.running_program.operation_plan
        self.running_program.cancel()
        self.update_program(pickle.dumps(self.running_program))

        response = self.slice_program(pickle.dumps(self.running_program))
        self.running_program = pickle.loads(response.sliced_program)
        self.replanned = True
        self.update_program(pickle.dumps(self.running_program))
        
    def program_from_drone_id(self, drone_id):
        if self.running_program is None:
            data = {'drone_id' : drone_id}
            response = self.get_program(pickle.dumps(data))
            self.running_program = pickle.loads(response.data)

        return self.running_program
        
    def publish_missions(self, event):
        if self.running_program is None or self.running_program.finished():
            return

        status = MissionStatus()
        status.uuid = str(self.running_program.get_id())
        status.drones = []
        status.original_plan = pickle.dumps(self.original_plan)
        status.replanned = self.replanned
        status.plan = pickle.dumps(self.running_program.operation_plan)
        status.no_fly_zones = pickle.dumps(self.running_program.all_no_fly_zones())
        for drone_id, position in self.running_program.runtime.drones_position.items():
            drone = DronePosition()
            drone.uuid = drone_id
            drone.latitude = position[0]
            drone.longitude = position[1]
            status.drones.append(drone)
        self.publish_mission.publish(status)

        self.replanned = False

    def run(self):
        rospy.init_node('mission_monitor')
        rospy.Timer(rospy.Duration(1.0/10.0), self.publish_missions)
        rospy.spin()


if __name__ == "__main__":
    MissionMonitorNode().run()