#!/usr/bin/env python3

import rospy

from dots_shared.srv import ParseTemplate, SaveData

from dots.ast_parser import DotsXML

import pickle

class ParseNode:

    def __init__(self):
        
        self.parser_template_service = rospy.Service("parser/parse", ParseTemplate, self.parse_template)
        self.database_template_service = rospy.ServiceProxy("database/template/save", SaveData)
        self.dots_parser = DotsXML()

    def parse_template(self, req):
        #TODO: check errors parser and database call
        operation = self.dots_parser.parse(req.template)
        operation_binary = pickle.dumps(operation)
        database_response = self.database_template_service(operation_binary)
        return [False, operation_binary]

    def run(self):
        rospy.init_node('xml_parser')
        rospy.spin()


if __name__ == "__main__":
    ParseNode().run()