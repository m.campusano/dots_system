#!/usr/bin/env python3

import rospy
import rospkg
from std_msgs.msg import UInt8MultiArray

import os

from dots.utm import UtmManager
from dots.utm.unifly import Operation
from UniflyLib import UniflyLib

import json
import pickle

class UtmPublisherNode:

    def __init__(self):
        rospy.init_node('external_data_publisher')

        self.plan_program_subscriber = rospy.Subscriber("database/callback/program/planned/new", UInt8MultiArray, self.publish_plan) # dots.core.program.Program 
        self.update_program_subscriber = rospy.Subscriber("database/callback/program/updated", UInt8MultiArray, self.update_plan) # dots.core.program.Program 

        # get params, if params is empty, use default path
        param_path = rospy.get_param("~utm_creds", "")
        if param_path == "":
            # get path to this package for reading the creds json file
            rospack = rospkg.RosPack()
            # get the file path for rospy_tutorials
            path = rospack.get_path('hd_utm_link')
            self.creds_path = path + "/src/utm_link/creds.json"
        else:
            self.creds_path = os.path.relpath(os.path.expanduser(param_path))

        self.unifly = UniflyLib(self.creds_path)
        self.utm_manager = UtmManager()

    def update_plan(self, data):
        program = pickle.loads(data.data)
        if program.canceled():
            for operation in self.utm_manager.get_operations_of_program(program):
                self.unifly.cancelOperation(operation.operation_id)
        for operation in self.utm_manager.get_operations_of_program(program):
            operation.program = program

       
    def publish_plan(self, data):
        program = pickle.loads(data.data)

        # remove previous operations of same program
        for operation in self.utm_manager.get_operations_of_program(program):
           self.unifly.removeOperation(operation.operation_id)

        # create operation in Unifly
        for drone_plan in program.operation_plan.drone_plans:
            response = self.unifly.createOperation(
                drone_plan.drone.uuid,
                drone_plan.path,
                drone_plan.get_start_time().get_serialized_string(),
                drone_plan.get_end_time().get_serialized_string()
            )
            content = json.loads(response.content)
            self.utm_manager.add_operation(Operation(program, content['uniqueIdentifier']))


    def run(self):
        rospy.spin()

if __name__ == "__main__":
    UtmPublisherNode().run()