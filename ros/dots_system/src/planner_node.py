#!/usr/bin/env python3

import rospy
from std_msgs.msg import UInt8MultiArray
from dots_shared.srv import SaveData, LoadData, \
                                PlanProgram, PlanProgramResponse, \
                                RePlanPrograms, RePlanProgramsResponse
from dots.planner import DefaultPlanner

import time
import pickle

class PlannerNode:

    def __init__(self):
        # plan program
        self.plan_program_service = rospy.Service("planner/plan", PlanProgram, self.plan_dots_program)

        # re plan programs when new no-fly-zones are added
        self.new_no_fly_zone_subscriber = rospy.Subscriber("database/callback/no_fly_zone/new", UInt8MultiArray, self.re_plan_programs) # dots.core.type.Area

        # database
        self.get_non_started_programs_service = rospy.ServiceProxy("database/program/planned/non_started", LoadData)
        self.get_no_fly_zones_service = rospy.ServiceProxy("database/no_fly_zone/all", LoadData)
        self.save_planned_program_service = rospy.ServiceProxy("database/program/planned/save", SaveData)
       
    def plan_dots_program(self, req):

        # get program
        program = pickle.loads(req.program)

        # non started programs
        response_programs = self.get_non_started_programs_service()
        non_started_programs = pickle.loads(response_programs.data)

        # no fly zones
        response_no_fly_zones = self.get_no_fly_zones_service()
        no_fly_zones = pickle.loads(response_no_fly_zones.data)

        pre = time.time()
        plan = DefaultPlanner().plan(program, no_fly_zones, non_started_programs)
        print(time.time()-pre)

        program.add_new_plan(plan)
        self.save_planned_program_service(pickle.dumps([program]))

        response = PlanProgramResponse()
        response.error = False
        response.program = pickle.dumps(program)
        return response

    def re_plan_programs(self, data):

        # non started programs
        response_programs = self.get_non_started_programs_service()
        non_started_programs = pickle.loads(response_programs.data)

        # no fly zones
        response_no_fly_zones = self.get_no_fly_zones_service()
        no_fly_zones = pickle.loads(response_no_fly_zones.data)

        re_planned_programs = DefaultPlanner().re_plan(non_started_programs, no_fly_zones)
        self.save_planned_program_service(pickle.dumps(re_planned_programs))

    def run(self):
        rospy.init_node('dots_planner')
        rospy.spin()



if __name__ == "__main__":
    PlannerNode().run()